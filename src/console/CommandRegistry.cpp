#include "CommandRegistry.h"

namespace JadeGit::Console
{
	CommandRegistry& CommandRegistry::get()
	{
		static CommandRegistry f;
		return f;
	}

	CommandRegistry::Registration::Registration()
	{
		get().registry.push_back(this);
	}

	void CommandRegistry::setup(CLI::App& app, Session& session, bool shell) const
	{
		for (auto& registration : registry)
			registration->setup(app, session, shell);
	}

	CommandRegistry::CommandRegistry() {}
}