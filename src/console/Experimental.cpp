#include "Command.h"
#include "CommandRegistration.h"
#include <registry/Root.h>
#include <registry/Storage.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Registry;

namespace JadeGit::Console
{
	class ExperimentalCommand : public Command
	{
	public:
		static constexpr const char* description = "Experimental mode";

		ExperimentalCommand(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			cmd.require_option(1, 1);
			cmd.add_flag("--enable", this->enable, "Enable experimental features");
			cmd.add_flag("--disable", "Disable experimental features");
		}

		void execute() final
		{
			// Open registry
			DatabaseStorage storage;
			Root registry(storage);

			// Check if experimental mode needs updating
			if (registry.experimental == enable)
			{
				cout << "Experimental mode already " << (registry.experimental ? "enabled" : "disabled") << endl;
				return;
			}

			// Update experimental mode
			registry.experimental = enable;
			registry.save(storage);

			// Confirm change made
			cout << "Experimental mode " << (registry.experimental ? "enabled" : "disabled") << endl;
		}

	protected:
		bool enable = false;
	};
	static CommandRegistration<ExperimentalCommand, true> registration("experimental");
}