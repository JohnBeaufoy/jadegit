#pragma once
#include "CommandRegistry.h"
#include <string>

namespace JadeGit::Console
{
	template <class TCommand, bool independent = false>
	class CommandRegistration : CommandRegistry::Registration
	{
	public:
		CommandRegistration(const char* name) : name(name) {}

	protected:
		void setup(CLI::App& app, Session& session, bool shell) const final
		{
			// Suppress setting up independent commands which aren't intended for use within shell
			if (independent && shell)
				return;

			// Setup command
			auto cmd = app.add_subcommand(name, TCommand::description);
			auto opt = std::make_shared<TCommand>(*cmd, session);
			cmd->callback([opt]() { opt->execute(); });
		}

	private:
		const char* name;
	};
}