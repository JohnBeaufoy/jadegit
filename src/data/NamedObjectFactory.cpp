#include <jadegit/data/NamedObjectFactory.h>

using namespace std;

namespace JadeGit::Data
{
	NamedObjectFactory& NamedObjectFactory::Get()
	{
		static NamedObjectFactory f; return f;
	}

	const NamedObjectFactory::Registration* NamedObjectFactory::Lookup(const string& key, const Component* origin, Class*& dataClass, bool required) const
	{
		return static_cast<const Registration*>(ObjectFactory::Lookup(key, origin, dataClass, required));
	}

	const NamedObjectFactory::Registration* NamedObjectFactory::Lookup(const type_info& type) const
	{
		return static_cast<const Registration*>(ObjectFactory::Lookup(type));
	}
}