#include <jadegit/data/JadeDynamicPropertyCluster.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeDynamicPropertyClusterMeta.h>
#include "NamedObjectRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(JadeDynamicPropertyCluster)

	class JadeDynamicPropertyClusterRegistration : protected NamedObjectRegistration<JadeDynamicPropertyCluster>
	{
	public:
		JadeDynamicPropertyClusterRegistration() : NamedObjectRegistration("JadeDynamicPropertyCluster", &Class::dynamicPropertyClusters) {}

	protected:
		JadeDynamicPropertyCluster* Resolve(Class* type, const std::string& name, bool shallow, bool inherit) const override
		{
			// Attempt standard load
			if (JadeDynamicPropertyCluster* cluster = NamedObjectRegistration::Resolve(type, name, shallow, inherit))
				return cluster;

			// Infer clusters needed while loading dynamic properties
			return inherit ? new JadeDynamicPropertyCluster(type, nullptr, name.c_str()) : nullptr;
		}
	};
	static JadeDynamicPropertyClusterRegistration registrar;

	template ObjectValue<Type*, &JadeDynamicPropertyClusterMeta::schemaType>;

	JadeDynamicPropertyCluster::JadeDynamicPropertyCluster(Class* parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeDynamicPropertyCluster), name),
		schemaType(parent)
	{
		// Flag clusters as inferred, meaning they don't have to be explicitly saved
		inferred();
	}

	JadeDynamicPropertyClusterMeta::JadeDynamicPropertyClusterMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeDynamicPropertyCluster", superclass),
		name(NewString("name", 101)),
		properties(NewReference<ExplicitInverseRef>("properties", NewType<CollClass>("PropertyNDict"))),
		schemaType(NewReference<ExplicitInverseRef>("schemaType", NewType<Class>("Type")))
	{
		name->unwritten().bind(&JadeDynamicPropertyCluster::name);
		schemaType->manual().child().bind(&JadeDynamicPropertyCluster::schemaType);
	}
}