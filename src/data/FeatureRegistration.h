#include "EntityRegistration.h"

namespace JadeGit::Data
{
	template<class TDerived>
	class FeatureRegistration : protected EntityRegistration<TDerived>
	{
	public:
		using EntityRegistration<TDerived>::EntityRegistration;
		using EntityRegistration<TDerived>::Resolve;

		TDerived* Resolve(EntityRegistration<TDerived>::Parent* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Iterate through superschema type hierarchy
			auto type = parent;
			while (type)
			{
				if (TDerived* feature = EntityRegistration<TDerived>::Resolve(type, name, shallow, inherit))
					return feature;

				if (!inherit)
					break;

				// Infer static class features (possibly invalid, but cheaper than having to explicitly instantiate all features upfront)
				if constexpr (std::is_constructible_v<TDerived, typename EntityRegistration<TDerived>::Parent*, const Class*, const char*>)
				{
					if (!type->superschemaType && type->isStatic())
						return new TDerived(type, nullptr, name.c_str());
				}

				// Get next superschema type to check
				type = static_cast<EntityRegistration<TDerived>::Parent*>(static_cast<Type*>(type->superschemaType));
			}

			// Feature doesn't exist in superschema type hierarchy (may exist in superclass hierarchy, which we don't currently check)
			return nullptr;
		}
	};

	template<class TDerived>
	class PropertyRegistration : protected FeatureRegistration<TDerived>
	{
		static_assert(std::is_base_of<Class, FeatureRegistration<TDerived>::Parent>(), "Parent is not a class");

	public:
		PropertyRegistration(const char* key) : FeatureRegistration<TDerived>(key, "property", &Class::properties) {}
	};

	template<class TDerived>
	class MethodRegistration : protected FeatureRegistration<TDerived>
	{
	public:
		MethodRegistration(const char* key) : FeatureRegistration<TDerived>(key, "method", &Type::methods) {}
	};
}