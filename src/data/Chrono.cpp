#include <jadegit/data/Chrono.h>

using namespace std;
using namespace std::chrono;

namespace JadeGit::Data
{
	template<>
	string Value<Date>::ToString() const
	{
		// Invalid
		if (!value.has_value())
			return "invalid";

		auto& d = value.value();

		// Null
		if (d == Date().value())
			return "";

		return format("{:%F}", d);
	}

	template<>
	string Value<Time>::ToString() const
	{
		// Invalid
		if (!value.has_value())
			return "invalid";

		auto& t = value.value();

		// Without milliseconds
		if (t.subseconds() == Time::value_type::precision::zero())
			return format("{:%T}", floor<seconds>(t.to_duration()));

		// With milliseconds
		return format("{:%T}", t);
	}

	template<>
	string Value<TimeStamp>::ToString() const
	{
		// Invalid
		if (!value.has_value())
			return "invalid";

		auto& ts = value.value();

		// Null
		if (ts == TimeStamp().value())
			return "";

		// Without milliseconds
		if (auto ts_without_ms = time_point_cast<seconds>(ts); ts == ts_without_ms)
			return format("{:%F %T}", ts_without_ms);

		// With milliseconds
		return format("{:%F %T}", ts);
	}

	template<>
	string Value<TimeStamp>::ToBasicString() const
	{
		// Invalid
		if (!value.has_value())
			return "*invalid*";

		auto& ts = value.value();

		// Null
		if (ts == TimeStamp().value())
			return "";

		return format("{0:%Y:%m:%d:%T}", ts);
	}
}