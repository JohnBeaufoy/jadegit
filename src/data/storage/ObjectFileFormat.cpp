#pragma once
#include "ObjectXMLFormat.h"

namespace JadeGit::Data
{
	const ObjectFileFormat* ObjectFileFormat::get()
	{
		return ObjectXMLFormat::Instance();
	}
}