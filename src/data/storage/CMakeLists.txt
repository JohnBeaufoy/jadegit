target_link_libraries(jadegit PUBLIC tinyxml2)

target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/ObjectFileFormat.cpp
	${CMAKE_CURRENT_LIST_DIR}/ObjectFileStorage.cpp
	${CMAKE_CURRENT_LIST_DIR}/ObjectLoader.cpp
)