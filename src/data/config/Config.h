#pragma once
#include <toml++/toml.h>
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Data
{
	class Config : public toml::table
	{
	public:
		Config(const FileSystem& fs, std::filesystem::path path);

		template <class T>
		T get(toml::path path, T defaultValue = T())
		{
			if constexpr (toml::impl::is_native<T>)
			{
				auto node = at_path(path);
				if (node.is_value())
				{
					return node.value<T>().value();
				}
				else
				{
					return defaultValue;
				}
			}
			else
			{
				auto value = get<std::string>(path);
				return value.empty() ? defaultValue : static_cast<T>(value);
			}
		}

		template <class T>
		void set(toml::path path, T value)
		{
			if constexpr (toml::is_container<T> || toml::is_value<T>)
			{
				if (path.size() > 1)
				{
					if (auto existing = at_path(path.parent()).as_table())
					{
						existing->insert_or_assign(path.leaf().str(), value);
					}
					else
					{
						toml::table table;
						table.insert_or_assign(path.leaf().str(), value);
						set(path.parent(), table);
					}
				}
				else
				{
					insert_or_assign(path.leaf().str(), value);
				}
			}
			else
			{
				set(path, static_cast<std::string>(value));
			}
		}

		void save();

	private:
		const FileSystem& fs;
		const std::filesystem::path path;
	};
}