#define TOML_IMPLEMENTATION
#include "Config.h"
#include <jadegit/vfs/File.h>

using namespace std;

namespace JadeGit::Data
{
	toml::table parse(const FileSystem& fs, const filesystem::path& path)
	{
		auto file = fs.open(path);
		if (file.exists())
		{
			auto input = file.createInputStream();

			try
			{
				return toml::parse(*input);
			}
			catch (...)
			{
				throw_with_nested(runtime_error(format("Failed to parse configuration file [{}]", path.generic_string())));
			}
		}

		return toml::table();
	}

	Config::Config(const FileSystem& fs, filesystem::path path) : toml::table(parse(fs, path)), fs(fs), path(move(path))
	{
	}

	void Config::save()
	{
		auto file = this->fs.open(this->path);

		if (empty())
		{
			if (file.exists())
				file.remove();

			return;
		}

		auto output = file.createOutputStream();
		*output << *this << std::endl;
	}
}