#include <jadegit/data/RelationalTable.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/RelationalTableClassMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMethMeta.h>
#include <jadegit/data/RootSchema/RelationalTableRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalTableXRelationshipMeta.h>
#include <jadegit/data/CollClass.h>
#include "NamedObjectRegistration.h"

namespace JadeGit::Data
{
	static NamedObjectRegistration<RelationalTable> relationalTable("RelationalTable", &ExternalRPSClassMap::tables, &RelationalView::tables);

	template NamedObjectDict<RelationalAttribute, &RelationalTableMeta::attributes>;
	template ObjectValue<Array<Class*>, &RelationalTableMeta::classes>;
	template ObjectValue<ExternalRPSClassMap* const, &RelationalTableMeta::rpsClassMap>;
	template ObjectValue<JadeOdbcView* const, &RelationalTableMeta::rView>;

	RelationalTable::RelationalTable(JadeOdbcView& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalTable), name),
		classMap(nullptr),
		view(parent)
	{
	}

	RelationalTable::RelationalTable(ExternalRPSClassMap& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalTable), name),
		classMap(parent),
		view(nullptr)
	{
	}

	RelationalTableMeta::RelationalTableMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalTable", superclass),
		attributes(NewReference<ExplicitInverseRef>("attributes", NewType<CollClass>("RelationalAttributeDict"))),
		classes(NewReference<ExplicitInverseRef>("classes", NewType<CollClass>("ClassSet"))),
		firstClassName(NewString("firstClassName", 101)),
		name(NewString("name", 101)),
		rView(NewReference<ExplicitInverseRef>("rView", NewType<Class>("RelationalView"))),
		rpsClassMap(NewReference<ExplicitInverseRef>("rpsClassMap", NewType<Class>("ExternalRPSClassMap"))),
		rpsExcluded(NewBoolean("rpsExcluded")),
		rpsExtra(NewBoolean("rpsExtra")),
		rpsIncludeInCallback(NewBoolean("rpsIncludeInCallback")),
		rpsShowMethods(NewBoolean("rpsShowMethods")),
		rpsShowObjectFeatures(NewBoolean("rpsShowObjectFeatures")),
		rpsShowVirtualProperties(NewBoolean("rpsShowVirtualProperties")),
		rpsTableKind(NewInteger("rpsTableKind")),
		uuid(NewBinary("uuid", 16))
	{
		attributes->automatic().parent().bind(&RelationalTable::attributes);
		classes->manual().bind(&RelationalTable::classes);
		name->unwritten().bind(&RelationalTable::name);
		rView->manual().child().bind(&RelationalTable::view);
		rpsClassMap->manual().child().bind(&RelationalTable::classMap);
	}

	RelationalTableClassMeta::RelationalTableClassMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableClass", superclass),
		bCallIFAllInstances(NewBoolean("bCallIFAllInstances")),
		rootCollectionName(NewString("rootCollectionName", 101)) {}

	RelationalTableCollectionMeta::RelationalTableCollectionMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableCollection", superclass),
		collectionName(NewString("collectionName", 101)),
		collectionType(NewInteger("collectionType")) {}

	RelationalTableCollectionMethMeta::RelationalTableCollectionMethMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableCollectionMeth", superclass),
		collectionType(NewInteger("collectionType")),
		methodName(NewString("methodName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}

	RelationalTableRelationshipMeta::RelationalTableRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableRelationship", superclass),
		firstPropertyName(NewString("firstPropertyName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}

	RelationalTableXRelationshipMeta::RelationalTableXRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableXRelationship", superclass),
		firstPropertyName(NewString("firstPropertyName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}
}