#pragma once
#include <jadegit/data/File.h>
#include <jadegit/xml.h>

namespace JadeGit::Data
{
	FileElement::FileElement(const File& file, const tinyxml2::XMLElement* source) : file(file), source(source) {}

	const FileSignature* FileElement::getSignature() const
	{
		if (auto lastLine = getLastLineNum(source))
			return file.getSignature(source->GetLineNum(), lastLine);
		else
			return file.getSignature(source->GetLineNum());
	}

	const char* FileElement::type() const
	{
		return source->Name();
	}

	const char* FileElement::header(const char* name) const
	{
		return source->Attribute(name);
	}

	int FileElement::getLastLineNum(const tinyxml2::XMLNode* node) const
	{
		if (auto next = node->NextSibling())
			return next->GetLineNum() - 1;

		if (auto parent = node->Parent())
			if (auto line = getLastLineNum(parent))
				return line - 1;

		// Don't know, will need to assume EOF
		// TODO: https://github.com/leethomason/tinyxml2/issues/800
		return 0;
	}
}