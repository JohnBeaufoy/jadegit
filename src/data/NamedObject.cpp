#include <jadegit/data/NamedObject.h>
#include <jadegit/data/File.h>

using namespace std;

namespace JadeGit::Data
{
	const char* NamedObject::getName() const
	{
		return name.c_str();
	}

	const NamedObject* NamedObject::getQualifiedParent() const
	{
		return getQualifiedParent(false);	
	}

	const NamedObject* NamedObject::getQualifiedParent(bool shorthand) const
	{
		if (shorthand)
			return getQualifiedParent();

		return dynamic_cast<const NamedObject*>(getParentObject());
	}

	string NamedObject::getQualifiedName(const NamedObject* context, bool shorthand) const
	{
		const NamedObject* parent = getQualifiedParent(shorthand);
		if (parent && (!context || !context->IsDescendent(parent)))
			return parent->getQualifiedName(context, shorthand) + "::" + name;
		else
			return name;
	}

	void NamedObject::LoadHeader(const FileElement& source)
	{
		// Load name attribute
		if (auto name = source.header("name"))
		{
			if (this->name.empty())
				this->name = name;

			// Verify name matches what was used on construction
			else if (this->name != name)
				throw std::runtime_error(std::format("Name [{}] doesn't match filename", name));
		}
	}

	void NamedObject::WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const
	{
		// Use qualified name for references
		if (reference)
		{
			// Resolve parent object
			const Object* parent = origin;
			const NamedObject* context = nullptr;
			while (parent && !context)
			{
				context = dynamic_cast<const NamedObject*>(parent);
				parent = parent->getParentObject();
			}

			element->SetAttribute("name", getQualifiedName(context).c_str());
		}
		else
		{
			if (!name.empty())
				element->SetAttribute("name", name.c_str());
		}
	}
}