#include <jadegit/data/Library.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/LibraryMeta.h>
#include "SchemaEntityRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(Library)

	static SchemaEntityRegistration<Library> registrar("Library", &Schema::libraries);

	template ObjectValue<Schema* const, &LibraryMeta::schema>;
	template ObjectValue<Set<Routine*>, &LibraryMeta::entrypoints>;

	Library::Library(Schema* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::library), name),
		schema(parent)
	{
	}

	void Library::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	LibraryMeta::LibraryMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Library", superclass),
		entrypoints(NewReference<ExplicitInverseRef>("entrypoints", NewType<CollClass>("EntryPointDict"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		// Group libraries after global constants
		subject->bracket(2);

		entrypoints->automatic().bind(&Library::entrypoints);
		schema->manual().child().bind(&Library::schema);
	}
}