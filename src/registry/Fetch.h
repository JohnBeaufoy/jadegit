#pragma once
#include <git2.h>
#include <registry/Data_generated.h>

namespace JadeGit::Registry
{
	void fetch(const git_repository& repo, const git_strarray* refspecs);
	void fetch(const git_repository& repo, const git_oid& commit);
	void fetch(const git_repository& repo, const CommitT& commit);
	void fetch(const git_repository& repo, const std::vector<CommitT>& commits);
}