#include "Visitor.h"

namespace JadeGit::Registry
{
	bool Visitor::visit(const DataT& registry)
	{
		if (visitEnter(registry))
		{
			for (auto& repo : registry.repos)
				if (!visit(repo))
					break;
		}
		return visitLeave(registry);
	}

	bool Visitor::visit(const RepositoryT& repository)
	{
		if (visitEnter(repository))
		{
			bool first = true;
			for (auto& commit : repository.latest)
			{
				if (!visit(commit, first, true))
					break;
				first = false;
			}

			first = true;
			for (auto& commit : repository.previous)
			{
				if (!visit(commit, first, false))
					break;
				first = false;
			}

			first = true;
			for (auto& schema : repository.schemas)
			{
				if (!visit(schema, first))
					break;
				first = false;
			}	
		}
		return visitLeave(repository);
	}
}