#pragma once
#include <jadegit/build.h>
#include <registry/Data_generated.h>
#include <iosfwd>

namespace JadeGit::Registry
{
	std::ostream& operator<<(std::ostream& os, const RepositoryT& repo);

	bool operator==(const RepositoryT& lhs, const RepositoryT& rhs);

	bool repo_match(const RepositoryT& lhs, const RepositoryT& rhs);
	bool repo_unknown(const RepositoryT& repo);
	void repo_update(RepositoryT& lhs, const RepositoryT& rhs, bool force);

#if USE_GIT2
	std::string repo_name(const std::string& origin);
	std::string repo_name(const git_repository& repo);
	std::string repo_origin(const git_repository& repo);
	bool repo_match(const RepositoryT& lhs, const git_repository& rhs);
#endif

#if USE_JADE && USE_GIT2
	RepositoryT repo_init(const std::string& name, const std::filesystem::path& repo);
#endif
}