#pragma once
#include "Storage.h"

namespace JadeGit::Registry
{
	class DatabaseStorage : public Storage
	{
	protected:
		void load(Root& registry) const final;
		void save(const uint8_t* buffer, size_t length) const final;
	};
}