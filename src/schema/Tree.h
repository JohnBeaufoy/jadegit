#pragma once
#include "Repository.h"

namespace JadeGit::Schema
{
	class Tree : public GitObject<git_tree>
	{
	public:
		static Tree lookup(const Repository& repo, const git_oid& oid);

		using GitObject::GitObject;

	protected:
		Tree(const Repository& repo, std::unique_ptr<git_tree> ptr);
	};
}