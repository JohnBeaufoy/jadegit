#pragma once
#include "Repository.h"

namespace JadeGit::Schema
{
	class LocalBranch;
	class Reference;
	class Tree;

	class Commit : public GitObject<git_commit>
	{
	public:
		static Commit lookup(const Repository& repo, const git_oid& oid);
		static Commit lookup(const Repository& repo, std::unique_ptr<git_commit> ptr);
		static Commit peel(const Reference& ref);
		Commit amend(const std::string& message);

		using GitObject::GitObject;

		LocalBranch branch(std::string name) const;
		void parents(DskObjectArray& array) const;
		Repository repo() const;
		bool reset(bool hard, IProgress* progress) const;
		bool switch_(IProgress* progress) const;
		Tree tree() const;

	protected:
		Commit(const Repository& repo, std::unique_ptr<git_commit> ptr);

		static Commit get(const Repository& repo, const git_oid& oid);
	};
}