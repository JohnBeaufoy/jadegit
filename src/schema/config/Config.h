#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class ConfigEntry;
	class Repository;

	class Config : public GitObject<git_config>
	{
	public:
		static Config default_();
		
		using GitObject::GitObject;
		Config(const Repository& parent, std::unique_ptr<git_config> ptr);

		std::string default_branch() const;

		ConfigEntry get_entry(const std::string& name) const;

		void set_string(const std::string& name, const std::string& value) const;

		Config snapshot() const;

	private:
		Config(std::unique_ptr<git_config> ptr);
	};
}