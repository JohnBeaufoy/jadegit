#include "Exception.h"
#include "data/Root.h"
#include <registry/Root.h>
#include <registry/Storage.h>

namespace JadeGit::Schema
{
	class Application : public Object
	{
	public:
		using Object::Object;

		void initialize()
		{
			// Ensure Root exists
			Root::get();

			// Retrieve current experimental setting
			Registry::DatabaseStorage storage;
			Registry::Root registry(storage);
			setProperty(TEXT("experimental"), registry.experimental);
		}
	};

	int JOMAPI jadegit_application_initialize(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Application app(&pBuffer->oid);

				// Handle base initialize
				JADE_RETURN(app.inheritMethod(pParams, pReturn));

				// Handle local initialize
				app.initialize();
				return J_OK;
			});
	}
}