#pragma once
#include "Data.h"
#include <jade/Collection.h>

namespace JadeGit::Schema
{
	class ReferenceNodeDict : public Jade::Collection<DskMemberKeyDictionary>
	{
	public:
		using Collection::Collection;

		ReferenceData lookup(const std::string& path) const;
		void remove(const std::string& path) const;
		void swap(std::string, std::string) const;
		bool write(const git_reference* reference, bool force) const;

		template <class Node>
		Node getAtKey(const std::string& name) const
		{
			using namespace Jade;

			DskParamCString pName(name);
			DskParamBool pDirectory(std::is_same_v<Node, ReferenceDirectory>);

			DskParam keys;
			jade_throw(paramSetParamList(keys, &pName, &pDirectory));

			Node node;
			jade_throw(DskMemberKeyDictionary::getAtKey(&keys, node));
			return node;
		}

		template <class T>
		std::unique_ptr<T> head(git_repository* repo) const
		{
			return target<T>("HEAD", repo);
		}

		template <class T>
		std::unique_ptr<T> target(const std::string& name, git_repository* repo) const
		{
			auto ref = lookup(name);
			if (ref.isNull())
				return nullptr;

			return ref.target<T>(repo);
		}

		template <class T>
		void update(const std::string& name, const T& target) const
		{
			init(name).update(target);
		}

	private:
		void cleanup() const;
		ReferenceData init(const std::string& path) const;
	};
}