#include "Data.h"
#include <git2/sys/refs.h>
#include <schema/data/Repository.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{	
	static GitObjectRegistration<ReferenceData> registration(TEXT("ReferenceData"));

	ReferenceData::ReferenceData() : ReferenceNode(registration) {}

	void ReferenceData::read(git_reference** out) const
	{
		string target = getProperty<string>(TEXT("target"));

		git_reference_t type;
		getProperty(TEXT("type"), (int*)&type);

		if (type == GIT_REFERENCE_DIRECT)
		{
			git_oid oid;
			git_throw(git_oid_fromstr(&oid, target.c_str()));
			*out = git_reference__alloc(path().c_str(), &oid, nullptr);
		}
		else
		{
			*out = git_reference__alloc_symbolic(path().c_str(), target.c_str());
		}
	}

/*	bool GitReference::rename(const string& path, bool force)
	{
		auto pos = path.rfind("/");
		auto name = (pos == path.npos) ? path : path.substr(pos + 1);

		GitRepository repository;
		GetRepository(repository);

		GitReferenceRoot references(repository);
		GitReferenceDirectory new_directory;
		references.create((pos == path.npos) ? string() : path.substr(0, pos), new_directory);

		GitReference existing;
		if (new_directory.children().getAtKey(name, false, existing))
		{
			if (!force)
				return false;

			jade_throw(existing.deleteObject());
		}

		GitReferenceDirectory old_directory;
		getProperty(TEXT("parent"), old_directory);

		if (old_directory.oid == new_directory.oid)
		{
			setProperty(TEXT("name"), name);
		}
		else
		{
			jade_throw(setProperty(TEXT("parent"), &NullDskObjectId));
			setProperty(TEXT("name"), name);
			jade_throw(setProperty(TEXT("parent"), new_directory));

			old_directory.cleanup();
		}
		
		return true;
	}
*/

	void ReferenceData::swap(const ReferenceData& other) const
	{
		auto target = other.getProperty<string>(TEXT("target"));
		auto type = other.getProperty<int>(TEXT("type"));

		other.setProperty(TEXT("target"), getProperty<string>(TEXT("target")));
		other.setProperty(TEXT("type"), getProperty<int>(TEXT("type")));
		
		setProperty(TEXT("target"), target);
		setProperty(TEXT("type"), type);
	}

	template <>
	unique_ptr<git_commit> ReferenceData::target<git_commit>(git_repository* repo) const
	{
		unique_ptr<git_commit> commit;

		switch (type())
		{
		case GIT_REFERENCE_DIRECT:
		{
			auto oid = getProperty<git_oid>(TEXT("target"));
			git_throw(git_commit_lookup(git_ptr(commit), repo, &oid));
			return commit;
		}
		case GIT_REFERENCE_SYMBOLIC:
		{
			unique_ptr<git_reference> ref;
			git_throw(git_reference_lookup(git_ptr(ref), repo, getProperty<string>(TEXT("target")).c_str()));
			git_throw(git_reference_resolve(git_ptr(ref), ref.get()));
			git_throw(git_commit_lookup(git_ptr(commit), repo, git_reference_target(ref.get())));
			return commit;
		}
		default:
			throw logic_error("Invalid reference type");
		}
	}

	template <>
	unique_ptr<git_reference> ReferenceData::target<git_reference>(git_repository* repo) const
	{
		switch (type())
		{
		case GIT_REFERENCE_DIRECT:
			return nullptr;
		case GIT_REFERENCE_SYMBOLIC:
		{
			unique_ptr<git_reference> ref;
			git_throw(git_reference_lookup(git_ptr(ref), repo, getProperty<string>(TEXT("target")).c_str()));
			return ref;
		}
		default:
			throw logic_error("Invalid reference type");
		}
	}

	git_reference_t ReferenceData::type() const
	{
		return static_cast<git_reference_t>(getProperty<int>(TEXT("type")));
	}

	void ReferenceData::update(const git_oid& commit_id) const
	{
		setProperty(TEXT("target"), &commit_id);
		jade_throw(setProperty(TEXT("type"), GIT_REFERENCE_DIRECT));
	}

	void ReferenceData::update(const unique_ptr<git_commit>& commit) const
	{
		assert(commit);
		update(*git_commit_id(commit.get()));
	}

	void ReferenceData::update(const unique_ptr<git_reference>& ref) const
	{
		assert(ref);
		setProperty(TEXT("target"), string(git_reference_name(ref.get())));
		jade_throw(setProperty(TEXT("type"), GIT_REFERENCE_SYMBOLIC));
	}

	bool ReferenceData::valid() const
	{
		return type() != GIT_REFERENCE_INVALID;
	}

	void ReferenceData::write(const git_reference* reference) const
	{
		if (const git_oid* target = git_reference_target(reference))
		{
			update(*target);
		}
		else
		{
			setProperty(TEXT("target"), string(git_reference_symbolic_target(reference)));
			jade_throw(setProperty(TEXT("type"), GIT_REFERENCE_SYMBOLIC));
		}
	}
}