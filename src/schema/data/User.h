#pragma once
#include <schema/Object.h>

namespace JadeGit::Schema
{
	class User : public Object
	{
	public:
		using Object::Object;
		User();

		std::string name() const;
	};
}