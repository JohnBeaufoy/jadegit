#pragma once
#include <git2/sys/odb_backend.h>
#include <schema/data/Repository.h>

namespace JadeGit::Schema::Backend
{
	class jadegit_odb_backend : public git_odb_backend
	{
	public:
		jadegit_odb_backend(const RepositoryData& repo);

		RepositoryData repo;
	};
}