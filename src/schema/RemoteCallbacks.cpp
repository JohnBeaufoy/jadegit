#include "RemoteCallbacks.h"
#include <git2.h>
#include <git2/sys/errors.h>
#include <jade/StringUtf8.h>
#include <jade/Transient.h>
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	class GitCredential : public Object
	{
	public:
		GitCredential();
		GitCredential(const DskObjectId& pOid) : Object(pOid) {}

		int fill(const string& url, string& username, string& password, string& errors);
		int fill(const u8string& url, u8string& username, u8string& password, u8string& errors);

		int approve(const string& url, const string& username, const string& password, string& errors);
		int approve(const u8string& url, const u8string& username, const u8string& password, u8string& errors);

		int reject(const string& url, string& errors);
		int reject(const u8string& url, u8string& errors);
	};
	static GitObjectRegistration<GitCredential> registration(TEXT("GitCredential"));

	GitCredential::GitCredential() : Object(registration) {}

	int GitCredential::fill(const string& url, string& username, string& password, string& errors)
	{
		u8string pUrl = u8string(url.begin(), url.end());
		u8string pUsername = u8string(username.begin(), username.end());
		u8string pPassword = u8string(password.begin(), password.end());
		u8string pErrors = u8string(errors.begin(), errors.end());

		int result = fill(pUrl, pUsername, pPassword, pErrors);

		username = string(pUsername.begin(), pUsername.end());
		password = string(pPassword.begin(), pPassword.end());
		errors = string(pErrors.begin(), pErrors.end());
		return result;
	}

	int GitCredential::fill(const u8string& url, u8string& username, u8string& password, u8string& errors)
	{
		DskParamStringUtf8 pUrl(url);
		DskParamStringUtf8 pUserName(username);
		DskParamStringUtf8 pPassword(password);
		DskParamStringUtf8 pErrors(errors);

		DskParam params;
		paramSetParamList(params);
		paramSetParameter(params, 1, &pUrl);
		paramSetParameter(params, 2, &pUserName);
		paramSetParameter(params, 3, &pPassword);
		paramSetParameter(params, 4, &pErrors);

		DskParam pResult;
		jade_throw(paramSetInteger(pResult));

		jade_throw(sendMsg(TEXT("fill"), &params, &pResult));

		pUserName.get(username);
		pPassword.get(password);
		pErrors.get(errors);

		int result = 0;
		paramGetInteger(pResult, result);
		return result;
	}

	int GitCredential::approve(const string& url, const string& username, const string& password, string& errors)
	{
		u8string pUrl = u8string(url.begin(), url.end());
		u8string pUsername = u8string(username.begin(), username.end());
		u8string pPassword = u8string(password.begin(), password.end());
		u8string pErrors = u8string(errors.begin(), errors.end());

		int result = approve(pUrl, pUsername, pPassword, pErrors);

		errors = string(pErrors.begin(), pErrors.end());
		return result;
	}

	int GitCredential::approve(const u8string& url, const u8string& username, const u8string& password, u8string& errors)
	{
		DskParamStringUtf8 pUrl(url);
		DskParamStringUtf8 pUserName(username);
		DskParamStringUtf8 pPassword(password);
		DskParamStringUtf8 pErrors(errors);

		DskParam params;
		paramSetParamList(params);
		paramSetParameter(params, 1, &pUrl);
		paramSetParameter(params, 2, &pUserName);
		paramSetParameter(params, 3, &pPassword);
		paramSetParameter(params, 4, &pErrors);

		DskParam pResult;
		jade_throw(paramSetInteger(pResult));

		jade_throw(sendMsg(TEXT("approve"), &params, &pResult));

		pErrors.get(errors);

		int result = 0;
		paramGetInteger(pResult, result);
		return result;
	}

	int GitCredential::reject(const string& url, string& errors)
	{
		u8string pUrl = u8string(url.begin(), url.end());
		u8string pErrors = u8string(errors.begin(), errors.end());

		int result = reject(pUrl, pErrors);

		errors = string(pErrors.begin(), pErrors.end());
		return result;
	}

	int GitCredential::reject(const u8string& url, u8string& errors)
	{
		DskParamStringUtf8 pUrl(url);
		DskParamStringUtf8 pErrors(errors);

		DskParam params;
		paramSetParamList(params);
		paramSetParameter(params, 1, &pUrl);
		paramSetParameter(params, 2, &pErrors);

		DskParam pResult;
		jade_throw(paramSetInteger(pResult));

		jade_throw(sendMsg(TEXT("reject"), &params, &pResult));

		pErrors.get(errors);

		int result = 0;
		paramGetInteger(pResult, result);
		return result;
	}

	int RemoteCallbacks::credentials(git_credential** cred, const char* url, const char* username_from_url, unsigned int allowed_types)
	{
		if (allowed_types & git_credential_t::GIT_CREDENTIAL_USERPASS_PLAINTEXT)
		{
			Transient<GitCredential> credentials;

			string username;
			string password;
			string errors;

			if (username_from_url)
				username = username_from_url;

			// Reject prior credentials on subsequent attempt
			if (authenticating)
			{
				if (credentials.reject(url, errors) != 0)
				{
					git_error_set_str(GIT_ERROR_NONE, errors.c_str());
					return GIT_ERROR;
				}
			}

			// Retrieve credentials from git
			if (credentials.fill(url, username, password, errors) == 0)
			{
				// Handle user cancellation
				if (username.empty() && password.empty())
					return GIT_EUSER;

				// Approve credentials upfront (rejected above on failure)
				if (credentials.approve(url, username, password, errors) != 0)
				{
					git_error_set_str(GIT_ERROR_NONE, errors.c_str());
					return GIT_ERROR;
				}

				// Return credentials
				authenticating = true;
				return git_credential_userpass_plaintext_new(cred, username.c_str(), password.c_str());
			}
			// Handle errors
			else
			{
				git_error_set_str(GIT_ERROR_NONE, errors.c_str());
				return GIT_ERROR;
			}
		}

		return GIT_PASSTHROUGH;
	}

	int RemoteCallbacks::push_transfer_progress(unsigned int current, unsigned int total, size_t bytes)
	{
		if (!progress)
			return GIT_OK;

		// Ignore if already finished
		if (!transferring && current == total)
			return GIT_OK;

		// Handle start of object download
		if (!transferring)
		{
			if (!progress->start(total))
				return GIT_EUSER;

			transferring = true;
		}

		// Handle progress update
		if (!progress->update(current))
			return GIT_EUSER;

		// Handle finish
		if (current == total)
		{
			transferring = false;

			if (!progress->finish())
				return GIT_EUSER;
		}

		return GIT_OK;
	}

	int RemoteCallbacks::sideband_progress(const char* str, int len)
	{
		if (!progress)
			return GIT_OK;

		progress->message(string(str, len));
		return progress->wasCancelled() ? GIT_EUSER : GIT_OK;
	}

	int RemoteCallbacks::transfer_progress(const git_indexer_progress* stats)
	{
		if (!progress)
			return GIT_OK;

		// Ignore if already finished
		if (!transferring && stats->indexed_objects == stats->total_objects)
			return GIT_OK;

		// Handle start of object download
		if (!transferring)
		{
			if (!progress->start(stats->total_objects, "Downloading"))
				return GIT_EUSER;

			transferring = true;
		}
		
		// Handle transition to delta resolution
		if (stats->indexed_deltas == 1)
		{
			if (!progress->update(stats->indexed_objects, "Resolving"))
				return GIT_EUSER;
		}
		// Handle progress update
		else if (!progress->update(stats->indexed_objects))
			return GIT_EUSER;

		// Handle finish
		if (stats->indexed_objects == stats->total_objects)
		{
			transferring = false;

			if (!progress->finish())
				return GIT_EUSER;
		}

		return GIT_OK;
	}
}