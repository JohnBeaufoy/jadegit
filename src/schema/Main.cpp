#include "Main.h"
#include <git2/global.h>
#include <git2/trace.h>
#include <jomcalls.h>
#include <Log.h>
#include <sstream>

void jadegit_trace_callback(git_trace_level_t level, const char* msg)
{
	// TODO: Pass selector onto logging macros
	LOG(msg);
}

namespace JadeGit::Schema
{
	DskHandle node = { 0 };

	class Main
	{
	public:
		Main()
		{
			jomGetNodeContextHandle(nullptr, &node);

			git_libgit2_init();

			/* Set log directory */
			// TODO: Need to get/use logs folder based on INI settings
			// JadeGit::Util::SetLogDirectory ...

			/* Setup trace logging */
			git_trace_set(git_trace_level_t::GIT_TRACE_TRACE, &jadegit_trace_callback);
		}

		~Main()
		{
			git_libgit2_shutdown();
		}
	};
	static Main main;
}