#pragma once
#include "Repository.h"
#include <vector>

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Schema
{
	class Remote : public GitObject<git_remote>
	{
	public:
		static Remote add(const Repository& repo, const std::string& name, const std::string& url);
		static Remote lookup(const Repository& repo, const std::string& name);

		using GitObject::GitObject;

		bool fetch(bool prune, bool allTags, IProgress* progress) const;
		bool pull(IProgress* progress) const;
		bool push(bool force, IProgress* progress) const;
		bool push(git_reference* branch, bool force, IProgress* progress) const;
		bool push(const std::vector<std::string>& refspecs, IProgress* progress) const;
		bool sync(IProgress* progress) const;

		std::unique_ptr<git_reference> upstream(const git_reference* branch, git_direction direction = git_direction::GIT_DIRECTION_FETCH) const;
		std::string upstream_name(const git_reference* branch, git_direction direction = git_direction::GIT_DIRECTION_FETCH) const;

	protected:
		Remote(const Repository& repo, std::unique_ptr<git_remote> ptr);

		bool pull(const Repository& repo, const git_reference* branch, IProgress* progress) const;
		Repository repo() const;
		std::string upstream_name(const char* refname, git_direction direction = git_direction::GIT_DIRECTION_FETCH) const;
	};
}