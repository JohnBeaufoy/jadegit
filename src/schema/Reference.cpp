#include "Reference.h"
#include "Branch.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Reference> registration(TEXT("Reference_"));

	Reference Reference::dwim(const Repository& repo, const string& shorthand)
	{
		// Lookup reference
		unique_ptr<git_reference> ptr;
		auto error = git_reference_dwim(git_ptr(ptr), repo, shorthand.c_str());
		if (error == GIT_ENOTFOUND)
			return Reference();
		git_throw(error);

		// Return new
		return Reference::make(repo, move(ptr));
	}

	Reference Reference::lookup(const Repository& repo, const string& name)
	{
		// Lookup reference
		unique_ptr<git_reference> ptr;
		auto error = git_reference_lookup(git_ptr(ptr), repo, name.c_str());
		if (error == GIT_ENOTFOUND)
			return Reference();
		git_throw(error);

		// Return new
		return Reference::make(repo, move(ptr));
	}

	Reference Reference::make(const Repository& repo, unique_ptr<git_reference> ptr)
	{
		if (!ptr)
			return Reference();

		if (git_reference_is_branch(ptr.get()))
			return LocalBranch::make(repo, move(ptr));
		
		if (git_reference_is_remote(ptr.get()))
			return RemoteBranch::make(repo, move(ptr));

		return Reference(repo, move(ptr));
	}

	Reference::Reference(ClassNumber classNo, const Repository& repo, std::unique_ptr<git_reference> ptr) : GitObject(classNo, move(ptr))
	{
		jade_throw(setProperty(TEXT("parent"), repo));
		jade_throw(setProperty(TEXT("symbolic"), git_reference_type(*this) == git_reference_t::GIT_REFERENCE_SYMBOLIC));
	}

	Reference::Reference(const Repository& repo, unique_ptr<git_reference> ptr) : Reference(registration, repo, move(ptr))
	{	
	}

	Reference Reference::dup() const
	{
		// Duplicate reference
		unique_ptr<git_reference> ptr;
		git_throw(git_reference_dup(git_ptr(ptr), *this));
		return Reference::make(repo(), move(ptr));
	}

	string Reference::path() const
	{
		return git_reference_name(*this);
	}

	Repository Reference::repo() const
	{
		return getProperty<Repository>(TEXT("parent"));
	}

	Reference Reference::resolve() const
	{
		// Resolve reference
		unique_ptr<git_reference> ptr;
		git_throw(git_reference_resolve(git_ptr(ptr), *this));
		return Reference::make(repo(), move(ptr));
	}

	string Reference::shorthand() const
	{
		return git_reference_shorthand(*this);
	}

	int JOMAPI jadegit_reference_dup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Reference(pBuffer).dup().oid);
			});
	}

	int JOMAPI jadegit_reference_dwim(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pShorthand = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pShorthand));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string shorthand;
				JADE_RETURN(paramGetString(*pShorthand, shorthand));

				auto reference = Reference::dwim(repo, shorthand);
				return paramSetOid(pReturn, reference.oid);
			});
	}

	int JOMAPI jadegit_reference_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pName = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pName));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string name;
				JADE_RETURN(paramGetString(*pName, name));

				auto reference = Reference::lookup(repo, name);
				return paramSetOid(pReturn, reference.oid);
			});
	}

	int JOMAPI jadegit_reference_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Reference>(pBuffer, pParams, git_reference_name);
	}

	int JOMAPI jadegit_reference_resolve(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Reference(pBuffer).resolve().oid);
			});
	}

	int JOMAPI jadegit_reference_shorthand(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Reference>(pBuffer, pParams, git_reference_shorthand);
	}

	int JOMAPI jadegit_reference_symbolic_target(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, git_reference_symbolic_target(Reference(pBuffer)));
			});
	}

	int JOMAPI jadegit_reference_target(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, git_oid_tostr_s(git_reference_target(Reference(pBuffer))));
			});
	}
}