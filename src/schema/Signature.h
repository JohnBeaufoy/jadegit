#pragma once
#include "Commit.h"

namespace JadeGit::Schema
{
	class Signature : public GitObject<const git_signature*>
	{
	public:
		using GitObject::GitObject;
		Signature(const Commit& parent, const Character* prop, const git_signature* ptr);

	};
}