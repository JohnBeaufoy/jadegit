#pragma once
#include <deploy/build/DirectoryBuilder.h>
#include <vfs/TempFileSystem.h>
#include <vector>

namespace JadeGit::Schema::Install
{
	class DeploymentCommand;

	class Deployment : public Deploy::DirectoryBuilder
	{
	public:
		Deployment();
		~Deployment();

		void execute();

	private:
		TempFileSystem fs;
		std::vector<std::unique_ptr<DeploymentCommand>> commands;
		Registry::Root registry;

		void start() final;
		void start(const Registry::Root& registry) final;
		void finish(const Registry::Root& registry) final;
		void finish() final;

		void addCommandFile(const std::filesystem::path& jcf, bool latestVersion) final;
		void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, bool latestVersion) final;
		void addReorg() final;
		void addScript(const Build::Script& script) final;
	};
}