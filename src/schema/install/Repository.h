#pragma once
#include <jadegit/git2.h>

namespace JadeGit::Schema::Install
{
	void fetch(std::unique_ptr<git_repository>& repo, std::unique_ptr<git_commit>& commit);
}