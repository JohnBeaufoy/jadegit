#include "Deployment.h"
#include <jade/Loader.h>
#include <jadegit/vfs/File.h>
#include <Exception.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Install
{
	class DeploymentCommand
	{
	public:
		virtual void execute(Loader& loader) = 0;
	};

	class LoadFile : public DeploymentCommand
	{
	public:
		LoadFile(filesystem::path path, bool latest) : path(path), latest(latest) {}

	protected:
		filesystem::path path;
		bool latest = false;
	};

	class LoadCommandFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadCommandFile(path, latest);
		}
	};

	class LoadSchemaFile : public LoadFile
	{
	public:
		LoadSchemaFile(filesystem::path scm, filesystem::path ddb, bool latest) : LoadFile(scm, latest), ddb(ddb) {}

	private:
		filesystem::path ddb;

		void execute(Loader& loader) final
		{
			// NOTE: Reorg is not suppressed here to cater for controls being versioned, albeit at the risk of reorg failing.
			// Further work is need to cater for possibility of update partially succeeding (could backfill prior to update).
			loader.loadSchemaFile(path, ddb, latest, false);
		}
	};

	class ReorgCommand : public DeploymentCommand
	{
	private:
		void execute(Loader& loader) final
		{
			loader.reorg(TEXT("JadeGitSchema"));
		}
	};

	Deployment::Deployment() : DirectoryBuilder(fs), fs(filesystem::path("install") / "deployment")
	{
	}

	Deployment::~Deployment() {}

	void Deployment::execute()
	{
		// Setup loader
		Loader loader;

		// Validate registry changes
		Registry::Root::validate(registry, false);

		// Execute deployment commands
		for (auto& command : commands)
			command->execute(loader);

		// Update registry
		Registry::Root::load(registry, false);
	}

	void Deployment::start()
	{
		fs.purge();
	}

	void Deployment::start(const Registry::Root& registry)
	{
		// Can only support single stage currently
		assert(this->registry.repos.empty());

		// Registry copied during finish for validation/update during deploy
	}

	void Deployment::finish(const Registry::Root& registry)
	{
		// Save copy of registry for validation/update during deploy
		this->registry = registry;
	}

	void Deployment::finish()
	{
	}

	void Deployment::addCommandFile(const filesystem::path& jcf, bool latestVersion)
	{
		commands.push_back(make_unique<LoadCommandFile>(fs.path() / jcf, latestVersion));
	}

	void Deployment::addSchemaFiles(const filesystem::path& scm, const filesystem::path& ddb, bool latestVersion)
	{
		commands.push_back(make_unique<LoadSchemaFile>(scm.empty() ? scm : fs.path() / scm, ddb.empty() ? ddb : fs.path() / ddb, latestVersion));
	}

	void Deployment::addReorg()
	{
		commands.push_back(make_unique<ReorgCommand>());
	}

	void Deployment::addScript(const Build::Script& script)
	{
		throw unimplemented_feature("Deploying scripts during install");
	}
}