#include "Object.h"
#include "ObjectFactory.h"
#include "Exception.h"
#include <jadegit/git2.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	LockType Object::getLockStatus() const
	{
		LockType type;
		LockDuration duration;
		DskObjectId lockedBy = NullDskObjectId;
		jade_throw(jomGetLockStatus(nullptr, &oid, &type, &duration, &lockedBy, __LINE__));
		return type;
	}
	
	template <>
	git_oid Object::getProperty(const Character* name) const
	{
		git_oid oid = { 0 };
		string value = getProperty<string>(name);
		git_throw(git_oid_fromstrn(&oid, value.c_str(), value.length()));
		return oid;
	}

	Object* Object::getThis(const DskBuffer* buffer)
	{
		return getThis(buffer->oid);
	}

	Object* Object::getThis(const DskObjectId& oid)
	{
		DskParam address;
		jade_throw(paramSetMemoryAddress(address));

		DskParam prop;
		jade_throw(paramSetCString(prop, TEXT("this")));

		jade_throw(jomGetProperty(nullptr, &oid, &prop, &address, __LINE__));

		void* result;
		jade_throw(paramGetMemoryAddress(address, result, jomGetMemorySpaceId()));
		return static_cast<Object*>(result);
	}

	void Object::setProperty(const Character* name, const git_oid* oid) const
	{
		setProperty(name, string(git_oid_tostr_s(oid)));
	}

	void Object::setThis() const
	{
		DskParam address;
		jade_throw(paramSetMemoryAddress(address, const_cast<Object*>(this), jomGetMemorySpaceId()));
		jade_throw(setProperty(TEXT("this"), &address));
	}

	int JOMAPI jadegit_object_delete(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				GitObjectFactory::get().dispose(pBuffer->oid);
				return J_OK;
			});
	}
}