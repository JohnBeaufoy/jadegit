#pragma once
#include "DeploymentCommand.h"

namespace JadeGit::Schema
{
	/*
		Represents an internal deployment which is created persistently to support restarts
	*/
	class GitDeployment : public GitDeploymentCommand
	{
	public:
		using GitDeploymentCommand::GitDeploymentCommand;
		GitDeployment();

		bool abort(IProgress* progress);
		bool execute(IProgress* progress);

	protected:
		using GitDeploymentCommand::abort;
		using GitDeploymentCommand::execute;

		bool abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done, IProgress* progress) const final;
		bool execute(Jade::Transaction& transaction, Jade::Loader& loader, IProgress* progress) const final;
		void executeExit() const override;

		void GetRepository(RepositoryData& repo) const override;
		void GetUser(User& user) const override;

	private:
		void abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done) const final {};
		void execute(Jade::Transaction& transaction, Jade::Loader& loader) const final {}
	};
}