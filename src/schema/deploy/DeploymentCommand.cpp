#include "DeploymentCommand.h"
#include "Deployment.h"
#include <jade/Iterator.h>
#include <jadegit/Progress.h>
#include <schema/data/Repository.h>
#include <schema/ObjectFactory.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	GitDeploymentCommand::GitDeploymentCommand(const GitDeploymentCommand& parent, ClassNumber classNo) : Object(classNo)
	{
		jade_throw(createObject());
		jade_throw(setProperty(TEXT("parent"), parent));
	}

	GitDeploymentCommand::State GitDeploymentCommand::getState() const
	{
		Byte state = 0;
		getProperty(TEXT("state"), state);
		return (GitDeploymentCommand::State)state;
	}

	void GitDeploymentCommand::setState(State value) const
	{
		jade_throw(setProperty(TEXT("state"), (Byte)value));
	}

	bool GitDeploymentCommand::abort(Transaction& transaction, Loader& loader, bool& done, IProgress* progress) const
	{
		// Skip those which have already been aborted
		if (isAborted() && !isExecuting())
			return (!progress || progress->skip());

		// TODO: Disable cancellation before point of no-return
		if (progress && progress->wasCancelled())
			return false;

		// Start execution (first time)
		if (!isAborted())
			abortEnter();

		assert(isAborted());
		if (isAborting())
		{
			// Attempt execution (should throw exception on error)
			abort(transaction, loader, done);

			// Finish execution
			abortExit();
		}
		assert(isAborted() && !isAborting());
		
		return (!progress || progress->step());
	}

	void GitDeploymentCommand::abortEnter() const
	{
		if (isCompleted())
			setState(getState() | State::Aborted | State::Executing);
		else
			setState(getState() | State::Aborted);
	}

	void GitDeploymentCommand::abortExit() const
	{
		setState(getState() & ~State::Executing);
	}

	bool GitDeploymentCommand::execute(Transaction& transaction, Loader& loader, IProgress* progress) const
	{
		// Skip those which have already been completed previously
		assert(!isAborted());
		if (isCompleted())
			return (!progress || progress->skip());

		// TODO: Disable cancellation before point of no-return
		if (progress && progress->wasCancelled())
			return false;

		// Start execution (first attempt only)
		if (!isExecuting())
			executeEnter();

		// Attempt execution (should throw exception on error)
		execute(transaction, loader);

		// Finish execution
		executeExit();
		assert(isCompleted() && !isExecuting());

		return (!progress || progress->step());
	}

	void GitDeploymentCommand::executeEnter() const
	{
		assert(!isAborted() && !isCompleted());
		setState(State::Executing);
	}

	void GitDeploymentCommand::executeExit() const
	{
		if (!isCompleted())
			setState(State::Completed);
	}

	void GitDeploymentCommand::GetRepository(RepositoryData& repo) const
	{
		DskObjectId oid;
		getProperty(TEXT("parent"), oid);

		unique_ptr<GitDeploymentCommand> parent = GitObjectFactory::get().create<GitDeploymentCommand>(oid);
		parent->GetRepository(repo);
	}

	void GitDeploymentCommand::GetUser(User& user) const
	{
		DskObjectId oid;
		getProperty(TEXT("parent"), oid);

		unique_ptr<GitDeploymentCommand> parent = GitObjectFactory::get().create<GitDeploymentCommand>(oid);
		parent->GetUser(user);
	}

	void GitDeploymentCommand::markAsCompleted() const
	{
		setState(State::Completed);
	}

	void GitDeploymentCommand::markRepositoryAsUnstable() const
	{
		// Put repository into unstable state (reset when deployment is completed successfully)
		RepositoryData repo;
		GetRepository(repo);
		repo.SetState(repo.GetState() | RepositoryData::Unstable);
	}
}