#pragma once
#include "DeploymentCommand.h"
#include <memory>

namespace JadeGit::Schema
{
	class GitDeploymentFile : public GitDeploymentCommand
	{
	public:
		using GitDeploymentCommand::GitDeploymentCommand;

		std::unique_ptr<std::ostream> CreateOutputStream();

	protected:
		std::unique_ptr<std::istream> CreateInputStream() const;

		void ExtractToFile(const std::string& path) const;
	};
}