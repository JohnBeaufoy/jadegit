#pragma once
#include "DeploymentCommand.h"

namespace JadeGit::Schema
{
	class GitDeploymentReorg : public GitDeploymentCommand
	{
	public:
		using GitDeploymentCommand::GitDeploymentCommand;
		GitDeploymentReorg(const GitDeploymentCommand& parent);

	protected:
		void abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done) const override;
		void execute(Jade::Transaction& transaction, Jade::Loader& loader) const override;
	};
}