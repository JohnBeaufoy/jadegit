#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class DiffBinary : public GitObject<const git_diff_binary*>
	{
	public:
		DiffBinary(const git_diff_binary* ptr);

	};
}