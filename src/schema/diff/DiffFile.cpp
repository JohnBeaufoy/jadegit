#include "DiffFile.h"
#include <schema/ObjectRegistration.h>

using namespace std;

namespace JadeGit::Schema
{
	static GitObjectRegistration<DiffFile> registration(TEXT("DiffFile"));

	DiffFile::DiffFile(const DiffDelta& parent, const Character* prop, const git_diff_file* file) : GitObject(registration, file, parent, prop)
	{
		if (file)
		{
			setProperty(TEXT("id"), &file->id);
			jade_throw(setProperty(TEXT("size"), file->size));
			jade_throw(setProperty(TEXT("flags"), file->flags));
			jade_throw(setProperty(TEXT("mode"), file->mode));
			jade_throw(setProperty(TEXT("id_abbrev"), file->id_abbrev));
		}
	}

	int JOMAPI jadegit_diff_file_path(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<DiffFile>(pBuffer, pParams, &git_diff_file::path);
	}
}