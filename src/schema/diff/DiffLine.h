#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class DiffLine : public GitObject<const git_diff_line*>
	{
	public:
		using GitObject::GitObject;
		DiffLine(const git_diff_line* ptr);

	};
}