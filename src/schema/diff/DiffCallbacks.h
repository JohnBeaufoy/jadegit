#pragma once
#include <schema/Object.h>
#include <git2.h>

namespace JadeGit::Schema
{
	class DiffCallbacks : public Object
	{
	public:
		using Object::Object;

		operator git_diff_binary_cb() const;
		operator git_diff_file_cb() const;
		operator git_diff_hunk_cb() const;
		operator git_diff_line_cb() const;

		int binary_cb(const git_diff_delta* delta, const git_diff_binary* binary) const;
		int file_cb(const git_diff_delta* delta, float progress) const;
		int hunk_cb(const git_diff_delta* delta, const git_diff_hunk* hunk) const;
		int line_cb(const git_diff_delta* delta, const git_diff_hunk* hunk, const git_diff_line* line) const;
	};
}