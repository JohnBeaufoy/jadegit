target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Diff.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffBinary.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffCallbacks.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffDelta.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffHunk.cpp
	${CMAKE_CURRENT_LIST_DIR}/DiffLine.cpp
)