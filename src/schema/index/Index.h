#pragma once
#include <schema/Repository.h>

namespace Jade
{
	class Transaction;
}

namespace JadeGit::Schema
{
	class IndexData;

	class Index : public GitObject<git_index>
	{
	public:
		static Index open(const Repository& repo);

		using GitObject::GitObject;

		void add_from_buffer(const std::string& path, const std::string& blob) const;
		bool has_conflicts() const;
		const Index& read(bool force = false) const;
		void reset(const git_commit* commit) const;
		void reset(const git_tree* tree) const;
		void write() const;

	private:
		Index(const Repository& repo, const WorktreeData& worktree);
		Index(const Repository& repo, std::unique_ptr<git_index> ptr);

		WorktreeData worktree() const;

		bool edition_check(IndexData& data) const;
		void edition_store(IndexData& data) const;
	};
}