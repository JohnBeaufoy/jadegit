target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Index.cpp
	${CMAKE_CURRENT_LIST_DIR}/IndexConflictIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/IndexEntry.cpp
)