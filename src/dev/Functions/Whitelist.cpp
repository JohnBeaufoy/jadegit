#include "Function.h"
#include <Exception.h>
#include <Log.h>
#include <Platform.h>

namespace JadeGit::Development
{
	// Allows whitelisted functions (no special handling required)
	class WhitelistFunction : public Function
	{
	public:
		WhitelistFunction(const AnnotatedVersion& requiredVersion, std::initializer_list<const char*> taskNames) : Function(taskNames), requiredVersion(requiredVersion) {}
		WhitelistFunction(std::initializer_list<const char*> taskNames) : WhitelistFunction(nullVersion, taskNames) {}

	private:
		const AnnotatedVersion& requiredVersion;

		bool execute(const std::string& entityName) const final
		{
			if (jadeVersion < requiredVersion)
				throw unsupported_feature(requiredVersion);

			LOG_TRACE("whitelisted");
			return true;
		}
	};

	static WhitelistFunction whitelist(
		{
			Function::AccessClasses,
			Function::AccessToAdminFns,			// Administration
			Function::AccessToClassFns,
			Function::AccessToConstantFns,
			Function::AccessToHistoryFns,
			Function::AccessToInterfaceFns,
			Function::AccessToMethodFns,
			Function::AccessToPropertyFns,
			Function::AddApplication,
			Function::AddClass,
			Function::AddCondition,
			Function::AddConstant,
			Function::AddConsumer,
			Function::AddCurrencyFormat,
			Function::AddExposureList,
			Function::AddExternalDb,
			Function::AddExternalMethod,
			Function::AddExtFunction,
			Function::AddExtFunctionLibrary,
			Function::AddForm,
			Function::AddGblConstsCategory,
			Function::AddGlobalConstant,
			Function::AddInterface,
			Function::AddJadeMethod,
			Function::AddLibrary,
			Function::AddLongDateFormat,
			Function::AddMapFile,
			Function::AddMethodView,
			Function::AddNumericFormat,
			Function::AddPackage,
			Function::AddProperty,
			Function::AddRPSView,
			Function::AddRelationalView,
			Function::AddSchemaView,
			Function::AddShortDateFormat,
			Function::AddSubclass,
			Function::AddTimeFormat,
			Function::BroadcastMessage,
			Function::BrowseActiveXLibrary,
			Function::BrowseApplications,
			Function::BrowseBreakpoints,
			Function::BrowseChangedMethods,
			Function::BrowseChkOutMethods,
			Function::BrowseClassesInUse,
			Function::BrowseConstants,
			Function::BrowseConsumer,
			Function::BrowseExportPackages,
			Function::BrowseExposedLists,
			Function::BrowseExposureList,
			Function::BrowseExternalDb,
			Function::BrowseExtFunctions,
			Function::BrowseFormats,
			Function::BrowseHTML,
			Function::BrowseImportPackages,
			Function::BrowseInterfaces,
			Function::BrowseLibraries,
			Function::BrowseLocales,
			Function::BrowseMapFiles,
			Function::BrowseMethods,
			Function::BrowseMethodStatusList,
			Function::BrowseMthdStatusList,
			Function::BrowsePackageClasses,
			Function::BrowsePackageUsages,
			Function::BrowsePrimTypes,
			Function::BrowseRPS,
			Function::BrowseRelViews,
			Function::BrowseSchemaViews,
			Function::BrowseTranslateForms,
			Function::BrowseTranslateStrings,
			Function::BrowseUnreferMethods,
			Function::ChangeClassCluster,
			Function::ChangeFunction,
			Function::ChangeMethod,
			Function::ChangeMethodView,
			Function::ChangeSchemaView,			// Schema views are currently ignored (incomplete/inconsistent patch control & schema load behaviour)
			Function::CloseSchemaWindows,
			Function::CompareMethods,
//			Function::CompileClass,				// duplicate of CompileMethod
//			Function::CompileExtFunction,		// duplicate of CompileMethod
			Function::CompileMethod,
			Function::CompileType,
			Function::Component_ActiveX,
			Function::Component_DotNet,
			Function::ClearHistory,
			Function::CopyMethod,
			Function::CopyMethodView,
			Function::CreateMappingMethod,
			Function::DebugJadeScript,
			Function::DisplayVersionInfo,
			Function::EditCopy,
			Function::EditCut,
			Function::EditPaste,
			Function::EditRedo,
			Function::EditUndo,
			Function::EnableInspectToolbar,
			Function::ExecuteJadeScript,
			Function::ExpandSchemaHierarchy,
			Function::ExtractActiveX,
			Function::ExtractAll,
//			Function::ExtractAllMethods,		// duplicate of ExtractAll
			Function::ExtractConsumer,
			Function::ExtractDotNetLib,
			Function::ExtractExposureList,
			Function::ExtractExternalDb,
			Function::ExtractFunction,
			Function::ExtractHTMLDocument,
			Function::ExtractInterface,
			Function::ExtractMethod,
			Function::ExtractMethod1,
			Function::ExtractMethods,
			Function::ExtractMethodView,
			Function::ExtractPackage,
//			Function::ExtractPrimType,			// duplicate of ExtractType
			Function::ExtractRPSView,
			Function::ExtractRelationalView,
			Function::ExtractRemapTable,
			Function::ExtractSchema,
			Function::ExtractSchemaView,
			Function::ExtractType,
			Function::FindBookmark,
			Function::FindClass,
			Function::FindCodePosition,
			Function::FindCollClsRefs,
			Function::FindConstantRefs,
//			Function::FindExtFunctionRefs,		// duplicate of FindMethodRefs
			Function::FindFormatRefs,
			Function::FindGblConstant,
			Function::FindGblConstsRefs,
			Function::FindInterface,
			Function::FindMethodImpls,
			Function::FindMethodLocalImpls,
			Function::FindMethodLocalImplsRefs,
			Function::FindMethodLocalRefs,
//			Function::FindMethodReferences,		// duplicate of FindMethodRefs
			Function::FindMethodRefs,
//			Function::FindPrimType,				// duplicate of FindClass
//			Function::FindPrimTypeRefs,			// duplicate of FindTypeRefs
			Function::FindPropertyReadRefs,
			Function::FindPropertyRefs,
			Function::FindPropertyUpdateRefs,
			Function::FindSchema,
			Function::FindTransientLeaks,
			Function::FindTypeRefs,
			Function::FindUnusedClassEntities,
			Function::FindUnusedLocalVars,
			Function::GenerateJavaScript,
			Function::GenerateTestCase,
			Function::InspectAllPersntInsts,
			Function::InspectMethod,
			Function::InspectPersistentInsts,
			Function::InspectSchemaTransients,
			Function::InspectTransientInsts,
			Function::InspectorSearch,
			Function::InterfaceMapper,
			Function::LoadExternalDb,
			Function::LoadRPSView,
			Function::LoadRelationalView,
			Function::LoadSchema,
			Function::Messages,
			Function::MethodsViewMenu,
			Function::MoveClass,
			Function::MoveMethod,
			Function::NewWorkspace,
			Function::OpenSchemaView,
			Function::OpenWorkspace,
			Function::PackageAddClass,
			Function::PackageAddConstant,
			Function::PackageAddInterface,
			Function::PackageAddMethod,
			Function::PackageAddProperty,
			Function::PackageChangeProperty,
			Function::PackageChangeType,
			Function::PackageFindType,
			Function::PackageRemoveConstant,
			Function::PackageRemoveMethod,
			Function::PackageRemoveProperty,
			Function::PackageRemoveType,
			Function::PrintExternalDb,
			Function::PrintMethod,
			Function::PrintRelationalView,
			Function::PrintRPSView,
			Function::PrintSchema,
			Function::PrintSelected,
			Function::PrintSetup,
			Function::RegisterActiveXServer,
			Function::RemoveApplication,
			Function::RemoveBreakpoint,
			Function::RemoveConstant,
			Function::RemoveExposureList,
			Function::RemoveExternalDb,
			Function::RemoveExtFunction,
			Function::RemoveFormat,
			Function::RemoveGblConstant,
			Function::RemoveGblConstsCategory,
			Function::RemoveInterface,
			Function::RemoveLibrary,
			Function::RemoveMapFile,
			Function::RemoveMethod,
			Function::RemoveMethodFromList,
			Function::RemoveMethodView,
			Function::RemovePackage,
			Function::RemoveProperty,
			Function::RemoveRPSView,
			Function::RemoveRelationalView,
			Function::RemoveSchemaView,
			Function::RemoveUnusedLocalVariables,
			Function::ResetUserPreferences,		// Administration
			Function::RpsCreateJcf,
			Function::RunApplication,
			Function::Save,
			Function::SaveHTML,
			Function::SaveMethod,
			Function::SaveMethodAs,
			Function::SchemaEntityBrowse,		// Methods viewer
			Function::SchemaEntityRecent,
			Function::SchemaEntityView,
			Function::SchemaEntityViewNew,
			Function::SearchInstances,
			Function::SearchListedMethods,
			Function::SetApplication,
			Function::SetBookmark,
			Function::SetBreakpoint,
			Function::SetConstantsSortOrder,
			Function::SetConstantText,
			Function::SetExtFunctionText,
//			Function::SetGblConstsSortOrder,	// duplicate of SetConstantsSortOrder
			Function::SetGlobalConstantText,
			Function::SetHTMLText,
			Function::SetMethodText,
			Function::SetPropertyText,
			Function::SetSchemaView,
			Function::SetSchemaViewText,
			Function::SetTypesSortOrder,
			Function::SetTypeText,
			Function::SetText,
			Function::SetUserPreferences,		// Administration
			Function::ShowAllMethodView,
			Function::ShowBackupDb,				// Administration
			Function::ShowCompositeVersion,
			Function::ShowCurrentVersion,
			Function::ShowEditForm,
			Function::ShowEditForm2,
			Function::ShowExternalDb,
			Function::ShowFormWizard,
			Function::ShowGlobalFindReplace,
			Function::ShowInterfaceMappings,
			Function::ShowLatestVersion,
			Function::ShowLocalFindReplace,
			Function::ShowMonitor,
			Function::ShowPromoteConstant,
			Function::ShowProtected,
			Function::ShowPublic,
			Function::ShowQuickNavigation,
			Function::ShowReadOnly,
			Function::ShowRelationDiagram,
			Function::ShowRelatnDiagramOptions,
			Function::ShowUserPreferences,
			Function::ShowVersionInfoObject,
			Function::ShowWSDLMappings,
			Function::ShowXMIImport,
			Function::UnregisterActiveXServer,
			Function::UserPreferences_Logoff,	// Administration
			Function::ValidateSchema,
			Function::ViewBubbleHelp,
			Function::ViewClass,
			Function::ViewCompiledMethods,
			Function::ViewExpandedHTree,
			Function::ViewInherited,
			Function::ViewLibrarySuperscms,
			Function::ViewMethodSource,
			Function::ViewMethodsInError,
			Function::ViewMethodsPane,
//			Function::ViewPrimType,				// duplicate of ViewClass
			Function::ViewPropertiesPane,
			Function::ViewResetRoot,
			Function::ViewSetRoot,
			Function::ViewSuperschemas,
			Function::ViewUncompiledMethods,
			Function::WebServicesOptions,
			Function::ZoomInRelationDiagram,
			Function::ZoomOutRelationDiagram
		}
	);

	static WhitelistFunction whitelist_jade2022SP3(jade2022SP3,
		{
			Function::SetSchemaText
		}
	);

	static WhitelistFunction whitelist_par70151(par70151,
		{
			Function::RemoveExtFunctionLibrary
		}
	);
}