if(NOT WITH_SCHEMA)
	return()
endif()

# Add development functionality to common object library
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Command.cpp
	${CMAKE_CURRENT_LIST_DIR}/MsgBox.cpp
	${CMAKE_CURRENT_LIST_DIR}/Session.cpp
	${CMAKE_CURRENT_LIST_DIR}/UserInfo.cpp
)
include(${CMAKE_CURRENT_LIST_DIR}/Functions/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/PatchControl/CMakeLists.txt)

# Setup shared development library
add_library(jadegitdev SHARED ${jadegit_SOURCE_DIR}/src/jadegit.rc)
target_compile_definitions(jadegitdev PRIVATE JADEGIT_FILEDESC="jadegit development library" JADEGIT_FILENAME="jadegitdev.dll" GIT_VERSION="${GIT_VERSION}")
target_include_directories(jadegitdev PRIVATE ${jadegit_SOURCE_DIR}/src)
target_link_libraries(jadegitdev PUBLIC jadegitscm)

install(TARGETS jadegitdev RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES $<TARGET_PDB_FILE:jadegitdev> DESTINATION ${CMAKE_INSTALL_BINDIR} OPTIONAL)

target_sources(jadegitdev PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Development.cpp
	${CMAKE_CURRENT_LIST_DIR}/jadegitdev.def
)