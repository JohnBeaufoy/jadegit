#include "TranslatableStringControl.h"
#include <jade/Transaction.h>
#include <Exception.h>
#include <Platform.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	bool TranslatableStringControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		if (operation.front() == 'D' && entityType == "TranslatableString")
		{
			// Deletion unsupported when it cannot be tracked atomically in same transaction (PAR #68418)
			if (!Transaction::InTransactionState() && jadeVersion < jade2022SP3)
				throw unsupported_feature("Deleting translatable strings", jade2022SP3);
		}

		return PatchControl::execute(session, entityType, entityName, operation);
	}
}