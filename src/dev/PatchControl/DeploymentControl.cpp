#include "DeploymentControl.h"
#include <jade/Loader.h>

namespace JadeGit::Development
{
	bool DeploymentControl::execute(const std::string& entityType, const QualifiedName& entityName, const std::string& operation)
	{
		// Currently, we just ignore calls during automated load
		// This needs to be expanded to prevent conflicts between multiple branches
		if (Jade::Loader::isLoader())
			return true;
		
		return PatchControl::execute(entityType, entityName, operation);
	}
}