#pragma once
#include "Feature.h"

namespace JadeGit::Extract
{
	class Property : public Feature
	{
	public:
		using Feature::Feature;

		void dependents(std::set<DskObjectId>& dependents) const override;
		void GetNamesakes(std::set<DskObjectId>& namesakes) const override;

	protected:		
		bool lookup(const Object* ancestor, const QualifiedName& path) final;

		void GetNamesakes(std::set<DskObjectId>& namesakes, const DskClass* schemaType, const Jade::String& name) const;
	};
}