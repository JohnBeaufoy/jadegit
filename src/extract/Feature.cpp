#include "Feature.h"

namespace JadeGit::Extract
{
	void Feature::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		// Handle indirect dependencies via exposed classes
		std::set<DskObjectId> exposedClasses;
		getProperty(PRP_Feature_exposedClassRefs, exposedClasses);
		for (auto& oid : exposedClasses)
		{
			DskObject exposedClass(oid);
			DskObjectId exposedList = NullDskObjectId;
			jade_throw(exposedClass.getProperty(PRP_JadeExposedClass_exposedList, &exposedList));
			if (!exposedList.isNull())
				dependents.insert(exposedList);
		}

		// Handle indirect dependencies via exposed features
		std::set<DskObjectId> exposedFeatures;
		getProperty(PRP_Feature_exposedFeatureRefs, exposedFeatures);
		for (auto& oid : exposedFeatures)
		{
			DskObject exposedFeature(oid);
			DskObject exposedClass;
			jade_throw(exposedFeature.getProperty(PRP_JadeExposedFeature_exposedClass, &exposedClass.oid));
			if (!exposedClass.isNull())
			{
				DskObjectId exposedList = NullDskObjectId;
				jade_throw(exposedClass.getProperty(PRP_JadeExposedClass_exposedList, &exposedList));
				if (!exposedList.isNull())
					dependents.insert(exposedList);
			}
		}
	}
}