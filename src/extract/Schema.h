#pragma once
#include "Entity.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Extract
{
	class Schema : public Entity
	{
	public:
		static Schema get(const QualifiedName& name);
		static bool isSupplied(const std::string& name);

		using Entity::Entity;
		using Entity::resolve;

		std::string getName() const override;

		bool IsDescendent(const DskObjectId& ancestor) const override;

		void children(std::set<DskObjectId>& children) const override;
		void dependents(std::set<DskObjectId>& dependents) const override;

		SchemaStatus getStatus() const;

	protected:
		DskObjectId getParentId() const final { return NullDskObjectId; }

		bool lookup(const Object* ancestor, const QualifiedName& path) final;
		Data::Schema* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;
	};
}