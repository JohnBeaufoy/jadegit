#pragma once
#include "Schema.h"
#include "Type.h"
#include "EntityRegistration.h"
#include "DataMapper.h"
#include <jadegit/data/RootSchema/ExternalRPSClassMapMeta.h>
#include <jadegit/data/RootSchema/RARelationIndexMeta.h>
#include <jadegit/data/RootSchema/RARelationKeyMeta.h>
#include <jadegit/data/RootSchema/RARelationOidMeta.h>
#include <jadegit/data/RootSchema/RARpsPropertyMeta.h>
#include <jadegit/data/RootSchema/RelationalTableClassMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMethMeta.h>
#include <jadegit/data/RootSchema/RelationalTableRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalTableXRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<RelationalViewMeta> relationalViewMapper(DSKRELATIONALVIEW, &RootSchema::relationalView, {
		{PRP_RelationalView_creator, new DataProperty(&RelationalViewMeta::creator)},
		{PRP_RelationalView_defaultVisibility, new DataProperty(&RelationalViewMeta::defaultVisibility)},
		{PRP_RelationalView_includeSystemClasses, new DataProperty(&RelationalViewMeta::includeSystemClasses)},
		{PRP_RelationalView_includedObjectFeatures, new DataProperty(&RelationalViewMeta::includedObjectFeatures)},
		{PRP_RelationalView_resyncOnExecute, new DataProperty(&RelationalViewMeta::resyncOnExecute)},
		{PRP_RelationalView_rootClass, new DataProperty(&RelationalViewMeta::rootClass)},
		{PRP_RelationalView_rpsClassMaps, new DataProperty<RelationalViewMeta>(nullptr)},
		{PRP_RelationalView_rpsDatabaseName, new DataProperty(&RelationalViewMeta::rpsDatabaseName)},
		{PRP_RelationalView_rpsDatabaseType, new DataProperty(&RelationalViewMeta::rpsDatabaseType)},
		{PRP_RelationalView_rpsDefaultConnectionString, new DataProperty(&RelationalViewMeta::rpsDefaultConnectionString)},
		{PRP_RelationalView_rpsDefaultPassword, new DataProperty(&RelationalViewMeta::rpsDefaultPassword)},
		{PRP_RelationalView_rpsDefaultUserName, new DataProperty(&RelationalViewMeta::rpsDefaultUserName)},
		{PRP_RelationalView_rpsExceptionCreate, new DataProperty(&RelationalViewMeta::rpsExceptionCreate)},
		{PRP_RelationalView_rpsExceptionDelete, new DataProperty(&RelationalViewMeta::rpsExceptionDelete)},
		{PRP_RelationalView_rpsExceptionUpdate, new DataProperty(&RelationalViewMeta::rpsExceptionUpdate)},
		{PRP_RelationalView_rpsLoggingOptions, new DataProperty(&RelationalViewMeta::rpsLoggingOptions)},
		{PRP_RelationalView_rpsShowMethods, new DataProperty(&RelationalViewMeta::rpsShowMethods)},
		{PRP_RelationalView_rpsShowObjectFeatures, new DataProperty(&RelationalViewMeta::rpsShowObjectFeatures)},
		{PRP_RelationalView_rpsShowVirtualProperties, new DataProperty(&RelationalViewMeta::rpsShowVirtualProperties)},
		{PRP_RelationalView_rpsTopSchemaName, new DataProperty(&RelationalViewMeta::rpsTopSchemaName)},
		{PRP_RelationalView_rpsUseOidClassInstMap, new DataProperty(&RelationalViewMeta::rpsUseOidClassInstMap)},
		{PRP_RelationalView_securityMethod, new DataProperty(&RelationalViewMeta::securityMethod)},
		{PRP_RelationalView_tables, new DataProperty<RelationalViewMeta>(nullptr)},
		{PRP_RelationalView_timeCreated, new DataProperty(&RelationalViewMeta::timeCreated)},
		{PRP_RelationalView_uuid, new DataProperty(&RelationalViewMeta::uuid)}
		});

	static DataMapper<ExternalRPSClassMapMeta> externalRPSClassMapMapper(DSKEXTERNALRPSCLASSMAP, &RootSchema::externalRPSClassMap, {
		{PRP_ExternalRPSClassMap_constraint, new DataProperty(&ExternalRPSClassMapMeta::constraint)},
		{PRP_ExternalRPSClassMap_excluded, new DataProperty(&ExternalRPSClassMapMeta::excluded)},
		{PRP_ExternalRPSClassMap_guid, new DataProperty(&ExternalRPSClassMapMeta::guid)},
		{PRP_ExternalRPSClassMap_includeInCallback, new DataProperty(&ExternalRPSClassMapMeta::includeInCallback)},
		{PRP_ExternalRPSClassMap_noDeletes, new DataProperty(&ExternalRPSClassMapMeta::noDeletes)},
		{PRP_ExternalRPSClassMap_rpsClass, new DataProperty(&ExternalRPSClassMapMeta::rpsClass)},
		{PRP_ExternalRPSClassMap_tableMode, new DataProperty(&ExternalRPSClassMapMeta::tableMode)},
		{PRP_ExternalRPSClassMap_tables, new DataProperty<ExternalRPSClassMapMeta>(nullptr)},
		{PRP_ExternalRPSClassMap_typeMapValue, new DataProperty(&ExternalRPSClassMapMeta::typeMapValue)}
		});

	static DataMapper<RelationalTableMeta> relationalTableMapper(DSKRELATIONALTABLE, &RootSchema::relationalTable, {
		{PRP_RelationalTable__sourceParcel, nullptr},
		{PRP_RelationalTable_attributes, new DataProperty<RelationalTableMeta>(nullptr)},
		{PRP_RelationalTable_classes, new DataProperty(&RelationalTableMeta::classes)},
		{PRP_RelationalTable_firstClassName, new DataProperty(&RelationalTableMeta::firstClassName)},
		{PRP_RelationalTable_rpsExcluded, new DataProperty(&RelationalTableMeta::rpsExcluded)},
		{PRP_RelationalTable_rpsExtra, new DataProperty(&RelationalTableMeta::rpsExtra)},
		{PRP_RelationalTable_rpsIncludeInCallback, new DataProperty(&RelationalTableMeta::rpsIncludeInCallback)},
		{PRP_RelationalTable_rpsShowMethods, new DataProperty(&RelationalTableMeta::rpsShowMethods)},
		{PRP_RelationalTable_rpsShowObjectFeatures, new DataProperty(&RelationalTableMeta::rpsShowObjectFeatures)},
		{PRP_RelationalTable_rpsShowVirtualProperties, new DataProperty(&RelationalTableMeta::rpsShowVirtualProperties)},
		{PRP_RelationalTable_rpsTableKind, new DataProperty(&RelationalTableMeta::rpsTableKind)}
		});

	static DataMapper<RelationalTableClassMeta> relationalTableClassMapper(DSKRELATIONALTABLECLASS, &RootSchema::relationalTableClass, {
		{PRP_RelationalTableClass_bCallIFAllInstances, new DataProperty(&RelationalTableClassMeta::bCallIFAllInstances)},
		{PRP_RelationalTableClass_rootCollectionName, new DataProperty(&RelationalTableClassMeta::rootCollectionName)}
		});

	static DataMapper<RelationalTableCollectionMeta> relationalTableCollectionMapper(DSKRELATIONALTABLECOLLECTION, &RootSchema::relationalTableCollection, {
		{PRP_RelationalTableCollection_collectionName, new DataProperty(&RelationalTableCollectionMeta::collectionName)},
		{PRP_RelationalTableCollection_collectionType, new DataProperty(&RelationalTableCollectionMeta::collectionType)}
		});

	static DataMapper<RelationalTableCollectionMethMeta> relationalTableCollectionMethMapper(DSKRELATIONALTABLECOLLECTIONMETH, &RootSchema::relationalTableCollectionMeth, {
		{PRP_RelationalTableCollectionMeth_collectionType, new DataProperty(&RelationalTableCollectionMethMeta::collectionType)},
		{PRP_RelationalTableCollectionMeth_methodName, new DataProperty(&RelationalTableCollectionMethMeta::methodName)},
		{PRP_RelationalTableCollectionMeth_secondClassName, new DataProperty(&RelationalTableCollectionMethMeta::secondClassName)}
		});

	static DataMapper<RelationalTableRelationshipMeta> relationalTableRelationshipMapper(DSKRELATIONALTABLERELATIONSHIP, &RootSchema::relationalTableRelationship, {
		{PRP_RelationalTableRelationship_firstPropertyName, new DataProperty(&RelationalTableRelationshipMeta::firstPropertyName)},
		{PRP_RelationalTableRelationship_secondClassName, new DataProperty(&RelationalTableRelationshipMeta::secondClassName)}
		});

	static DataMapper<RelationalTableXRelationshipMeta> relationalTableXRelationshipMapper(DSKRELATIONALTABLEXRELATIONSHIP, &RootSchema::relationalTableXRelationship, {
		{PRP_RelationalTableXRelationship_firstPropertyName, new DataProperty(&RelationalTableXRelationshipMeta::firstPropertyName)},
		{PRP_RelationalTableXRelationship_secondClassName, new DataProperty(&RelationalTableXRelationshipMeta::secondClassName)}
		});

	static DataMapper<RelationalAttributeMeta> relationalAttributeMapper(DSKRELATIONALATTRIBUTE, &RootSchema::relationalAttribute, {
		{PRP_RelationalAttribute_attrName, new DataProperty(&RelationalAttributeMeta::attrName)},
		{PRP_RelationalAttribute_decimals, new DataProperty(&RelationalAttributeMeta::decimals)},
		{PRP_RelationalAttribute_feature, new DataProperty(&RelationalAttributeMeta::feature)},
		{PRP_RelationalAttribute_length, new DataProperty(&RelationalAttributeMeta::length)},
		{PRP_RelationalAttribute_name, new DataProperty(&RelationalAttributeMeta::name)},
		{PRP_RelationalAttribute_referencedClassName, new DataProperty(&RelationalAttributeMeta::referencedClassName)},
		{PRP_RelationalAttribute_rpsColType, new DataProperty(&RelationalAttributeMeta::rpsColType)},
		{PRP_RelationalAttribute_rpsExcluded, new DataProperty(&RelationalAttributeMeta::rpsExcluded)},
		{PRP_RelationalAttribute_rpsKeyKind, new DataProperty(&RelationalAttributeMeta::rpsKeyKind)},
		{PRP_RelationalAttribute_rpsMapKind, new DataProperty(&RelationalAttributeMeta::rpsMapKind)},
		{PRP_RelationalAttribute_sqlType, new DataProperty(&RelationalAttributeMeta::sqlType)}
		});

	static DataMapper<RARelationIndexMeta> raRelationIndexMapper(DSKRARELATIONINDEX, &RootSchema::raRelationIndex, {
		{PRP_RARelationIndex_fromFirstClass, new DataProperty(&RARelationIndexMeta::fromFirstClass)},
		{PRP_RARelationIndex_propertyName, new DataProperty(&RARelationIndexMeta::propertyName)},
		});

	static DataMapper<RARelationKeyMeta> raRelationKeyMapper(DSKRARELATIONKEY, &RootSchema::raRelationKey, {
		{PRP_RARelationKey_keyIndex, new DataProperty(&RARelationKeyMeta::keyIndex)}
		});

	static DataMapper<RARelationOidMeta> raRelationOidMapper(DSKRARELATIONOID, &RootSchema::raRelationOid, {
		{PRP_RARelationOid_fromFirstClass, new DataProperty(&RARelationOidMeta::fromFirstClass)}
		});

	static DataMapper<RARpsPropertyMeta> raRpsPropertyMapper(DSKRARPSPROPERTY, &RootSchema::raRpsProperty, {
		{PRP_RARpsProperty_attrDesc, nullptr},
		{PRP_RARpsProperty_excluded, new DataProperty(&RARpsPropertyMeta::excluded)},
		{PRP_RARpsProperty_guid, new DataProperty(&RARpsPropertyMeta::guid)}
		});

	class RelationalView : public Entity
	{
	public:
		using Entity::Entity;

		void extract(Assembly& assembly, bool deep) const override
		{
			Entity::extract(assembly, true);
		}

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalView_name);
		}

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema_relationalViews, PRP_Schema_rpsDatabases);
		}

		DskObjectId getParentId() const final
		{
			return getProperty<DskObjectId>(PRP_RelationalView_schema);
		}

		string GetTypeName() const final
		{
			if (getProperty<int>(PRP_RelationalView_rpsDatabaseType))
				return "JadeRpsMapping";
			else
				return "JadeOdbcView";
		}
	};
	static EntityRegistration<RelationalView> relationalView(DSKRELATIONALVIEW);

	class ExternalRPSClassMap : public Object
	{
	public:
		using Object::Object;

	};
	static ObjectRegistration<ExternalRPSClassMap> externalRPSClassMap(DSKEXTERNALRPSCLASSMAP);

	class RelationalTable : public NamedObject
	{
	public:
		using NamedObject::NamedObject;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalTable_name);
		}

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final
		{
			return NamedObject::lookup<RelationalView>(ancestor, path, PRP_RelationalView_tables);
		}

		DskObjectId getParentId() const final
		{
			return getProperty<DskObjectId>(PRP_RelationalTable_rView);
		}
	};
	static NamedObjectRegistration<RelationalTable> relationalTable(DSKRELATIONALTABLE);

	class RelationalAttribute : public NamedObject
	{
	public:
		using NamedObject::NamedObject;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalAttribute_name);
		}

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final
		{
			return NamedObject::lookup<RelationalTable>(ancestor, path, PRP_RelationalTable_attributes);
		}

		DskObjectId getParentId() const final
		{
			return getProperty<DskObjectId>(PRP_RelationalAttribute_table);
		}
	};
	static NamedObjectRegistration<RelationalAttribute> relationalAttribute(DSKRELATIONALATTRIBUTE);
}