#include "Type.h"
#include "DataMapper.h"
#include "NamedObjectRegistration.h"

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	class JadeDynamicPropertyCluster : public NamedObject
	{
	public:
		using NamedObject::NamedObject;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeDynamicPropertyCluster_name);
		}

		bool isShallow() const final
		{
			return true;
		}

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final
		{
			return NamedObject::lookup<Class>(ancestor, path, PRP_Class_dynamicPropertyClusters);
		}

		DskObjectId getParentId() const final
		{
			return getProperty<DskObjectId>(PRP_JadeDynamicPropertyCluster_schemaType);
		}
	};
	static NamedObjectRegistration<JadeDynamicPropertyCluster> dynamicPropertyCluster(DSKJADEDYNAMICPROPERTYCLUSTER);
}