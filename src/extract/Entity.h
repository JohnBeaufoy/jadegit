#pragma once
#include "NamedObject.h"
#include <jadegit/data/Entity.h>

namespace JadeGit::Extract
{
	// Entities are objects which can be resolved by name
	class Entity : public NamedObject
	{
	public:
		static Data::Entity* resolve(Data::Assembly& assembly, const DskObjectId& oid, bool shallow = true);

		using NamedObject::NamedObject;

		virtual ClassNumber GetKind() const;

		std::string GetDisplay() const override;

		// Extract from source to destination assembly
		virtual void extract(Assembly& assembly, bool deep) const;

		// Populates set with all associate entities which are deleted implicitly with receiver 
		virtual void getAssociates(std::set<DskObjectId>& associates) const {};

		// Populates set with all child entities
		virtual void children(std::set<DskObjectId>& children) const {};

		// Populates set with dependent entities which refer to receiver
		virtual void dependents(std::set<DskObjectId>& dependents) const {};

		// Populates set with all dependent entities which are impacted by rename
		void dependentsAll(std::set<DskObjectId>& dependents, const DskObjectId& ancestor = NullDskObjectId) const;

		// Populates set with all entities which are renamed implicitly with receiver
		virtual void GetNamesakes(std::set<DskObjectId>& namesakes) const;

		// Retrieve parent entity
		std::unique_ptr<Entity> getParent() const;

		// Derives qualified name
		std::unique_ptr<QualifiedName> GetQualifiedName(const Entity* context = nullptr) const;

		// Determines if entity is descendent of ancestor
		virtual bool IsDescendent(const DskObjectId& ancestor) const;

		// Resolves object in assembly, instantiating if it doesn't exist already
		Data::Entity& resolve(Data::Assembly& assembly, bool shallow = true) const;

	protected:
		// Returns basic type name where entities may need to mutate (i.e. ExplicitInverseRef <=> ImplicitInverseRef)
		virtual std::string GetBasicTypeName() const { return GetTypeName(); }

		// Resolves child object in the context of parent supplied
		Data::Entity* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;
	};
}