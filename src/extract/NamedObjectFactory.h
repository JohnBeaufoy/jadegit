#pragma once
#include "NamedObject.h"
#include "ObjectFactory.h"

namespace JadeGit::Extract
{
	class NamedObjectFactory : public ObjectFactory
	{
	public:
		static NamedObjectFactory& Get();

		class Registration : public ObjectFactory::Registration
		{
		public:
			NamedObject* Create(const DskObjectId& oid) const override = 0;
		};

		void Register(const ClassNumber& key, const Registration* registrar);

		std::unique_ptr<NamedObject> Create(const DskObjectId& oid) const;

	protected:
		NamedObjectFactory() {}
	};
}