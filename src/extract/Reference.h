#pragma once
#include "Property.h"

namespace JadeGit::Extract
{
	class Reference : public Property
	{
	public:
		using Property::Property;

	protected:
		std::string GetBasicTypeName() const override;
	};

	class ExplicitInverseRef : public Reference
	{
	public:
		using Reference::Reference;

		void dependents(std::set<DskObjectId>& dependents) const override;
	};

	class ImplicitInverseRef : public Reference
	{
	public:
		using Reference::Reference;
	};
}