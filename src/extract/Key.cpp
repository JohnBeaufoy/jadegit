#include "Key.h"
#include "Type.h"
#include "DataMapper.h"
#include "ObjectRegistration.h"
#include <jadegit/data/RootSchema/ExternalKeyMeta.h>
#include <jadegit/data/RootSchema/MemberKeyMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static ObjectRegistration<ExternalKey> externalKey(DSKEXTERNALKEY);
	static ObjectRegistration<MemberKey> memberKey(DSKMEMBERKEY);

	static DataMapper<KeyMeta> keyMapper(DSKKEY, &RootSchema::key, {
		{PRP_Key__systemBasic, nullptr},
		{PRP_Key_caseInsensitive, new DataProperty(&KeyMeta::caseInsensitive)},
		{PRP_Key_descending, new DataProperty(&KeyMeta::descending)},
		{PRP_Key_sortOrder, new DataProperty(&KeyMeta::sortOrder)}
		});

	// Returns true if type usage has a specific length which isn't predefined by type
	bool hasSpecificLength(const DskObject& key)
	{
		DskType type;
		jade_throw(key.getProperty(PRP_ExternalKey_type, type));
		return !type.isNull() && !hasPredefinedLength(type);
	}

	static DataMapper<ExternalKeyMeta> externalKeyMapper(DSKEXTERNALKEY, &RootSchema::externalKey, {
		{PRP_ExternalKey_length, new DataProperty(&ExternalKeyMeta::length, [](const DskObject& object) { return hasSpecificLength(DskObject(&object.oid)); })},
		{PRP_ExternalKey_name, new DataProperty(&ExternalKeyMeta::name)},
		{PRP_ExternalKey_precision, new DataProperty(&ExternalKeyMeta::precision)},
		{PRP_ExternalKey_scaleFactor, new DataProperty(&ExternalKeyMeta::scaleFactor)},
		{PRP_ExternalKey_type, new DataProperty(&ExternalKeyMeta::type)}
		});

	static DataMapper<MemberKeyMeta> memberKeyMapper(DSKMEMBERKEY, &RootSchema::memberKey, {
		{PRP_MemberKey__propertyClasses, nullptr},
		{PRP_MemberKey_keyPath, new DataProperty(&MemberKeyMeta::keyPath)},
		{PRP_MemberKey_path, nullptr},
		{PRP_MemberKey_property, new DataProperty(&MemberKeyMeta::property)},
		});
}