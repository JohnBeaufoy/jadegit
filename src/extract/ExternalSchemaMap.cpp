#include "Type.h"
#include "Property.h"
#include "DataMapper.h"
#include "NamedObjectRegistration.h"
#include <jadegit/data/RootSchema/ExternalClassMapMeta.h>
#include <Exception.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ExternalSchemaMapMeta> externalSchemaMapMapper(DSKEXTERNALSCHEMAMAP, &RootSchema::externalSchemaMap, {
		{PRP_ExternalSchemaMap_schemaEntity, new DataProperty(&ExternalSchemaMapMeta::schemaEntity)}
		});

	static DataMapper<ExternalClassMapMeta> externalClassMapMapper(DSKEXTERNALCLASSMAP, &RootSchema::externalClassMap, {
		{PRP_ExternalClassMap_attributeMaps, new DataProperty<ExternalClassMapMeta>(nullptr)},
		{PRP_ExternalClassMap_database, nullptr},
		{PRP_ExternalClassMap_referenceMaps, new DataProperty<ExternalClassMapMeta>(nullptr)},
		{PRP_ExternalClassMap_tables, new DataProperty(&ExternalClassMapMeta::tables)},
		});

	static DataMapper<ExternalAttributeMapMeta> externalAttributeMapMapper(DSKEXTERNALATTRIBUTEMAP, &RootSchema::externalAttributeMap, {
		{PRP_ExternalAttributeMap_classMap, nullptr},
		{PRP_ExternalAttributeMap_column, new DataProperty(&ExternalAttributeMapMeta::column)}
		});

	static DataMapper<ExternalReferenceMapMeta> externalReferenceMapMapper(DSKEXTERNALREFERENCEMAP, &RootSchema::externalReferenceMap, {
		{PRP_ExternalReferenceMap_classMap, nullptr},
		{PRP_ExternalReferenceMap_dirnLeftRight, new DataProperty(&ExternalReferenceMapMeta::dirnLeftRight)},
		{PRP_ExternalReferenceMap_leftColumns, new DataProperty(&ExternalReferenceMapMeta::leftColumns)},
		{PRP_ExternalReferenceMap_otherRefMap, new DataProperty(&ExternalReferenceMapMeta::otherRefMap)},
		{PRP_ExternalReferenceMap_relType, new DataProperty(&ExternalReferenceMapMeta::relType)},
		{PRP_ExternalReferenceMap_rightColumns, new DataProperty(&ExternalReferenceMapMeta::rightColumns)},
		{PRP_ExternalReferenceMap_wherePredicate, new DataProperty(&ExternalReferenceMapMeta::wherePredicate)}
		});

	template <class TOriginal, auto parent>
	class ExternalSchemaMap : public NamedObject
	{
	public:
		using NamedObject::NamedObject;

		string getName() const final
		{
			return getProperty<TOriginal>(PRP_ExternalSchemaMap_schemaEntity).getName();
		}

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final
		{
			throw unimplemented_feature();
		}

		DskObjectId getParentId() const final
		{
			return getProperty<DskObjectId>(*parent);
		}
	};

	using ExternalClassMap = ExternalSchemaMap<Class, &PRP_ExternalClassMap_database>;
	using ExternalCollClassMap = ExternalSchemaMap<Class, &PRP_ExternalCollClassMap_database>;
	using ExternalAttributeMap = ExternalSchemaMap<Property, &PRP_ExternalAttributeMap_classMap>;
	using ExternalReferenceMap = ExternalSchemaMap<Property, &PRP_ExternalReferenceMap_classMap>;

	static NamedObjectRegistration<ExternalClassMap> externalClassMap(DSKEXTERNALCLASSMAP);
	static NamedObjectRegistration<ExternalCollClassMap> externalCollClassMap(DSKEXTERNALCOLLCLASSMAP);
	static NamedObjectRegistration<ExternalAttributeMap> externalAttributeMap(DSKEXTERNALATTRIBUTEMAP);
	static NamedObjectRegistration<ExternalReferenceMap> externalReferenceMap(DSKEXTERNALREFERENCEMAP);
}