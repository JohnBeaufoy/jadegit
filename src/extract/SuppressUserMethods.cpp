#include "SuppressUserMethods.h"
#include <jade/AppContext.h>
#include <jade/Object.h>

using namespace Jade;

namespace JadeGit::Extract
{
	SuppressUserMethods::SuppressUserMethods()
	{
		restore = invokeUserMethods(false);
	}

	SuppressUserMethods::~SuppressUserMethods()
	{
		invokeUserMethods(restore);
	}

	bool SuppressUserMethods::invokeUserMethods(bool invoke) const
	{
		Object process(AppContext::process());

		DskParamBool pInvoke(invoke);
		DskParamBool pResult;
		jade_throw(process.sendMsg(TEXT("_invokeUserMethods"), &pInvoke, &pResult));

		bool result = false;
		jade_throw(paramGetBoolean(pResult, result));
		return result;
	}
}