#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/Application.h>
#include <jadegit/data/JadeWebServiceManager.h>

namespace JadeGit::Build
{
	class ApplicationDelta : public SchemaComponentDelta<Application>
	{
	public:
		ApplicationDelta(Graph& graph) : SchemaComponentDelta(graph, "Application") {}

	protected:
		bool AnalyzeEnter() final
		{
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Handle deleting before exposures
				if (previous->webServiceManager)
				{
					auto deletion = this->GetDeletion();
					for (JadeExposedList* exposure : previous->webServiceManager->exposures)
						this->addDeletionDependency(exposure, deletion);
				}

				return false;
			}

			auto definition = this->GetDefinition();

			if (definition && latest->webServiceManager)
			{
				// Application definition depends on exposures being created first
				for (JadeExposedList* exposure : latest->webServiceManager->exposures)
					this->addCreationDependency(exposure, definition);

				// Deleting exposures used previously depends on application being updated first
				if (previous && previous->webServiceManager)
				{
					for (JadeExposedList* exposure : previous->webServiceManager->exposures)
						this->addDeletionDependency(exposure, definition);
				}
			}

			return true;
		}
	};
}