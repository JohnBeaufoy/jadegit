#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class FileTask : public Task
	{
	public:
		enum LoadStyle : unsigned char
		{
			Current = 1,
			Latest = 2,
			Any = 3
		};

		const LoadStyle loadStyle;

		FileTask(Graph& graph, Task* parent = nullptr, LoadStyle loadStyle = LoadStyle::Latest, bool complete = false, short priority = 0) : Task(graph, parent, complete, priority), loadStyle(loadStyle) {}

		inline bool isLatest() const { return loadStyle & LoadStyle::Latest; }
		inline bool isLatest(bool latest) const { return loadStyle & (latest ? LoadStyle::Latest : LoadStyle::Current); }
	};
}