#include "Task.h"
#include "TaskCluster.h"
#include "TaskState.h"
#include <assert.h>

using namespace std;

namespace JadeGit::Build
{
	void TaskCluster::make(Task& left, Task& right)
	{
		assert(&left != &right);
		assert(!left.cluster);
		assert(!right.cluster);
		new TaskCluster(left, right);
	}

	TaskCluster::TaskCluster(Task& left, Task& right)
	{
		// Set initial state
		state = TaskState::initialState();

		// Add initial tasks
		add(left);
		add(right);
	}

	void TaskCluster::add(Task& task)
	{
		// Merge task clusters
		if (auto redundant = task.cluster)
		{
			// Update cluster for each task
			for (auto& task : redundant->tasks)
				task->cluster = this;

			// Merge tasks
			tasks.insert(tasks.end(), redundant->tasks.begin(), redundant->tasks.end());

			// Cleanup redundant cluster
			delete redundant;
		}
		// Add task
		else
		{
			task.cluster = this;
			tasks.push_back(&task);
		}
	}

	void TaskCluster::remove(Task& task)
	{
		assert(task.cluster == this);

		// Reset task cluster
		task.cluster = nullptr;

		// Remove task from list
		if (auto pos = find(tasks.begin(), tasks.end(), &task); pos != tasks.end())
			tasks.erase(pos);

		// Cleanup cluster when last task is removed
		if (tasks.empty())
			delete this;
	}
}