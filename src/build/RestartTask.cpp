#include "RestartTask.h"
#include "TaskVisitor.h"

namespace JadeGit::Build
{
	RestartTask::operator std::string() const
	{
		return "Restart";
	}
	
	bool RestartTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}
}