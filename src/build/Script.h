#pragma once
#include <string>

namespace JadeGit::Build
{
	class Script
	{
	public:
		std::string schema;
		std::string app;
//		std::string executeScript;
		std::string executeSchema;
		std::string executeClass;
		std::string executeMethod;
		std::string executeParam;
		bool executeTransient = false;
		bool executeTypeMethod = false;
	};
}