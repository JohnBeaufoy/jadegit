#include "SchemaFileWriter.h"
#include "SchemaDefinition.h"
#include "TypeDeclaration.h"
#include <jadegit/data/Schema.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	SchemaFileWriter::SchemaFileWriter(Builder &builder, bool latest) : builder(builder), latest(latest) {}

	void SchemaFileWriter::Write()
	{
		root.Extract(builder, latest);
	}

	bool SchemaFileWriter::Declare(const Class& klass)
	{
		return (NodeFactory<TypeDeclaration>::get().create(std::type_index(typeid(klass)), root, klass));
	}

	bool SchemaFileWriter::Define(const Data::ActiveXLibrary& library)
	{
		return (NodeFactory<DataNode>::get().create(std::type_index(typeid(library)), root, library));
	}

	bool SchemaFileWriter::Define(const Application& application)
	{
		return (NodeFactory<DataNode>::get().create(std::type_index(typeid(application)), root, application));
	}

	bool SchemaFileWriter::Define(const Data::ExternalDatabase& database)
	{
		return (NodeFactory<DataNode>::get().create(std::type_index(typeid(database)), root, database));
	}

	bool SchemaFileWriter::Define(const JadeExposedList& list)
	{
		if (NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(list)), root, list))
		{
			for (auto& exposedClass : list.exposedClasses)
			{
				NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(*exposedClass.second)), root, *exposedClass.second);

				for (auto& exposedFeature : exposedClass.second->exposedFeatures)
					NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(*exposedFeature.second)), root, *exposedFeature.second);
			}

			return true;
		}

		return false;
	}

	bool SchemaFileWriter::Define(const JadeHTMLDocument& document, const char* oldName)
	{
		return (NodeFactory<DataNode, const char*>::get().create(std::type_index(typeid(document)), root, document, oldName));
	}

	bool SchemaFileWriter::Define(const JadeInterfaceMapping& mapping)
	{
		return (NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(mapping)), root, mapping));
	}

	bool SchemaFileWriter::Define(const Form& form)
	{
		return (NodeFactory<DataNode>::get().create(std::type_index(typeid(form)), root, form));
	}

	bool SchemaFileWriter::Define(const RelationalView& view)
	{
		return (NodeFactory<DataNode>::get().create(std::type_index(typeid(view)), root, view));
	}

	bool SchemaFileWriter::Define(const Schema& schema, bool complete, bool modified, bool had_text)
	{
		return (NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(schema)), root, schema, complete, modified, had_text));
	}

	bool SchemaFileWriter::Define(const SchemaEntity& entity, bool complete, bool modified, bool had_text)
	{
		return (NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(entity)), root, entity, complete, modified, had_text));
	}
}