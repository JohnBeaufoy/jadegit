#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/RelationalView.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/RelationalAttributeMeta.h>
#include <jadegit/data/RootSchema/RelationalTableMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	template <class TNode, class TContainer>
	void PrintChildren(XMLPrinter& printer, const char* element, const TContainer& children)
	{
		printer.OpenElement(element);
		for (auto& child : children)
			TNode(*child.second).Print(printer);
		printer.CloseElement();
	}

	class RelationalAttributeData : public DataNode
	{
	public:
		RelationalAttributeData(const RelationalAttribute& attribute) : DataNode(attribute) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const RelationalAttribute&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}
	};

	class RelationalTableData : public DataNode
	{
	public:
		RelationalTableData(const RelationalTable& table) : DataNode(table) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const RelationalTable&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const RelationalTable&>(this->source);

			printer.OpenElement("rpsClassMap");
			if (source.classMap)
				printer.PushAttribute("name", source.classMap->rpsClass->name.c_str());
			printer.CloseElement();

			DataNode::PrintData(printer);

			PrintChildren<RelationalAttributeData>(printer, "attributes", source.attributes);

			printer.OpenElement("classes");
			DataNode::PrintCollection(printer, source.classes, "Class", false);
			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			if (property == rootSchema.relationalTable->classes ||
				property == rootSchema.relationalTable->rpsClassMap)
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class RAMethodData : public RelationalAttributeData
	{
	public:
		RAMethodData(const RAMethod& method) : RelationalAttributeData(method) {}

		using RelationalAttributeData::RelationalAttributeData;

	protected:
		void PrintData(XMLPrinter& printer) const override
		{
			RelationalAttributeData::PrintData(printer);

			auto& source = static_cast<const RAMethod&>(this->source);

			printer.OpenElement("rpsClassMap");
			if (source.classMap)
				printer.PushAttribute("name", source.classMap->rpsClass->name.c_str());
			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			return property == rootSchema.relationalAttribute->name;
		}
	};
	
	class ExternalRPSClassMapData : public DataNode
	{
	public:
		ExternalRPSClassMapData(const ExternalRPSClassMap& source) : DataNode(source) {};

	protected:
		void PrintAttributes(XMLPrinter& printer) const final {}

		void PrintData(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const ExternalRPSClassMap&>(this->source);

			// Handle constraint explicitly
			printer.OpenElement("constraint");
			if (source.constraint)
				RAMethodData(*source.constraint).Print(printer);
			printer.CloseElement();

			// ExternalRPSClassMap isn't treated as an entity currently, but it does have patch version/modified properties
			// For now, we'll use the parent signature data to populate patch version/modified properties
			DataNode::PrintSignatureData(printer, *static_cast<RelationalView*>(source.rpsDatabase));

			DataNode::PrintData(printer);

			PrintChildren<RelationalTableData>(printer, "tables", source.tables);
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			if (property == rootSchema.externalRPSClassMap->constraint)
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class RelationalViewData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		RelationalViewData(SchemaDefinition& schema, const RelationalView& view) : DataNode(schema.data, view, "RelationalView") {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const RelationalView&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const final
		{
			DataNode::PrintData(printer);

			auto& source = static_cast<const RelationalView&>(this->source);

			printer.OpenElement("rpsClassMaps");
			for (auto& classMap : source.rpsClassMaps)
				ExternalRPSClassMapData(*classMap).Print(printer);
			printer.CloseElement();

			PrintChildren<RelationalTableData>(printer, "tables", source.tables);
		}

		void PrintValue(XMLPrinter& printer, const Data::Object* value) const final
		{
			if (auto klass = dynamic_cast<const Class*>(value->getParent()))
				printer.PushAttribute("className", klass->name.c_str());

			DataNode::PrintValue(printer, value);
		}
	};
	static NodeRegistration<RelationalViewData, RelationalView> relationalView;
	static NodeRegistration<RelationalViewData, JadeOdbcView> jadeOdbcView(true);
	static NodeRegistration<RelationalViewData, JadeRpsMapping> jadeRpsMapping(true);
}