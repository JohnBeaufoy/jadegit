#include "MethodDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	class JadeMethodDefinition : public MethodDefinition
	{
	public:
		JadeMethodDefinition(TypeDefinition& type, const Data::JadeMethod& method) : MethodDefinition(type, method)
		{
			type.jadeMethodDefinitions.push_back(this);
		}
	};
	static RoutineDefinitionRegistration<JadeMethodDefinition, Data::JadeMethod> registrar;
}