#include "PropertyDefinition.h"
#include "ClassDefinition.h"
#include <jadegit/data/Reference.h>

namespace JadeGit::Build::Classic
{
	template <class TReference>
	class ReferenceDefinition : public PropertyDefinition<TReference>
	{
	public:
		ReferenceDefinition(ClassDefinition& klass, const TReference& reference) : PropertyDefinition<TReference>(reference)
		{
			klass.referenceDefinitions.push_back(this);
		}

	protected:
		using PropertyDefinition<TReference>::source;

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			PropertyDefinition<TReference>::WriteEnter(output, indent);

			auto& constraint = source.constraint;
			if (constraint)
				output << " where " << constraint->schemaType->name << "::" << constraint->name;
		}
	};

	class ExplicitInverseRefDefinition : public ReferenceDefinition<Data::ExplicitInverseRef>
	{
	public:
		ExplicitInverseRefDefinition(SchemaDefinition& schema, ClassDefinition& klass, const Data::ExplicitInverseRef& reference) : ReferenceDefinition(klass, reference)
		{
			for (auto& inverse : reference.inverses)
				NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(*inverse)), schema, inverse->getPrimary());
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			WriteAttribute(output, "explicitEmbeddedInverse", source.embedded);
			WriteAttribute(output, "explicitInverse", !source.embedded);
			WriteAttribute(output, "transientToPersistentAllowed", source.transientToPersistentAllowed);
			WriteAttribute(output, "inverseNotRequired", source.inverseNotRequired);

			ReferenceDefinition::WriteAttributes(output);
		}
	};
	static NodeRegistration<ExplicitInverseRefDefinition, Data::ExplicitInverseRef> explicitInverseRef;
	static NodeRegistration<ExplicitInverseRefDefinition, Data::ExternalReference> externalReference(true);
	static NodeRegistration<DynamicPropertyDefinition<ExplicitInverseRefDefinition, Data::JadeDynamicExplicitInverseRef>, Data::JadeDynamicExplicitInverseRef> dynamicExplicitInverseRef(true);

	class ImplicitInverseRefDefinition : public ReferenceDefinition<Data::ImplicitInverseRef>
	{
	public:
		using ReferenceDefinition::ReferenceDefinition;

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			WriteAttribute(output, "implicitMemberInverse", source.memberTypeInverse);

			ReferenceDefinition::WriteAttributes(output);
		}
	};
	static NodeRegistration<ImplicitInverseRefDefinition, Data::ImplicitInverseRef> implicitInverseRef;
	static NodeRegistration<DynamicPropertyDefinition<ImplicitInverseRefDefinition, Data::JadeDynamicImplicitInverseRef>, Data::JadeDynamicImplicitInverseRef> dynamicImplicitInverseRef(true);
}