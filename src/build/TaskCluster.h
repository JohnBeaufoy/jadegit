#pragma once
#include <jadegit/MemoryAllocated.h>
#include <vector>

namespace JadeGit::Build
{
	class Task;

	class TaskCluster : public MemoryAllocated
	{
	public:
		static void make(Task& left, Task& right);

		void add(Task& task);
		void remove(Task& task);

	private:
		TaskCluster(Task& left, Task& right);
		TaskCluster(const TaskCluster&) = delete;
		~TaskCluster() = default;

		friend class Task;
		friend class TaskState;
		friend class ProcessingState;
		mutable const TaskState* state;

		std::vector<Task*> tasks;
	};
}
