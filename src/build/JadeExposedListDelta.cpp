#include "JadeExposedListDelta.h"
#include "ClassDelta.h"
#include "RestartTask.h"
#include <jadegit\data\RootSchema.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	JadeExposedListDelta::JadeExposedListDelta(Graph& graph) : SchemaComponentDelta(graph, "Exposure") {}

	bool JadeExposedListDelta::AnalyzeEnter()
	{
		// Basic analysis
		if (!SchemaComponentDelta<JadeExposedList>::AnalyzeEnter())
		{
			auto deletion = GetDeletion();

			// Related entities cannot be deleted before exposure
			for (auto& exposedClass : previous->exposedClasses)
			{
				addDeletionDependency(exposedClass.second->getOriginal(), deletion);

				for (auto& exposedFeature : exposedClass.second->exposedFeatures)
					addDeletionDependency(exposedFeature.second->getOriginal(), deletion);
			}

			return false;
		}

		auto definition = GetDefinition();

		// Exposure definition depends on related entities being created/renamed
		for (auto& exposedClass : latest->exposedClasses)
		{
			auto& relatedClass = exposedClass.second->getOriginal();
			if (auto delta = graph.Analyze<IClassDelta>(relatedClass))
			{
				if (const Class* previous = delta->getPrevious(); relatedClass.webService && (!previous || !previous->webService))
					definition->addPeer(delta->GetDeclaration());
				else
					definition->addPredecessor(delta->GetDeclaration());
			}

			for (auto& exposedFeature : exposedClass.second->exposedFeatures)
				addCreationDependency(exposedFeature.second->getOriginal(), definition);
		}

		// Related entities cannot be deleted before exposure is updated
		if (previous)
		{
			for (auto& exposedClass : previous->exposedClasses)
			{
				addDeletionDependency(exposedClass.second->getOriginal(), definition);

				for (auto& exposedFeature : exposedClass.second->exposedFeatures)
					addDeletionDependency(exposedFeature.second->getOriginal(), definition);
			}
		}

		return true;
	}

	Task* JadeExposedListDelta::handleDefinition(Task* parent)
	{
		auto definition = SchemaComponentDelta<JadeExposedList>::handleDefinition(parent);

		// Determine if reload is required following a class rename (workaround for PAR #70096)
		if (previous && latest->GetRootSchema().version < par70096)
		{
			for (auto& exposedClass : previous->exposedClasses)
			{
				if (auto delta = graph.Analyze<ITypeDelta>(exposedClass.second->getOriginal()); delta && delta->GetRename())
				{
					// Setup task to wait until processing cycle is complete and builder is idle/ready to start new schema file
					auto restart = new RestartTask(graph);
					restart->addPredecessor(definition);

					// Setup task to reload exposure definition in new schema file
					auto reload = new ExtractDefinitionTask<JadeExposedList>(graph, nullptr, *latest);
					reload->addPredecessor(restart);

					break;
				}
			}
		}

		return definition;
	}
}