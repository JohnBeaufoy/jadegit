#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class RestartTask : public Task
	{
	public:
		RestartTask(Graph& graph) : Task(graph)
		{
		}

		operator std::string() const final;

	protected:
		bool accept(TaskVisitor& v) const final;
	};
}