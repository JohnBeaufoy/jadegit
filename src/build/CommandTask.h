#pragma once
#include "FileTask.h"

namespace JadeGit::Build
{
	class CommandStrategy;

	class CommandTask : public FileTask
	{
	public:
		CommandTask(Graph& graph, Task* parent = nullptr, LoadStyle loadStyle = LoadStyle::Latest, bool complete = false) : FileTask(graph, parent, loadStyle, complete, -1)
		{
		}

		virtual void execute(CommandStrategy& strategy) const = 0;

	protected:
		bool accept(TaskVisitor& v) const override;
		bool required(const Task* cascade, bool repeat) const override { return !repeat; }
	};
}