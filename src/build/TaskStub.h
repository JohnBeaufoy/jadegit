#pragma once
#include "Task.h"

namespace JadeGit::Data
{
	class Entity;
}

namespace JadeGit::Build
{
	class TaskStub : public Task
	{
	public:
		static TaskStub* make(Graph& graph, Task* parent, const Data::Entity* entity);
		
		operator std::string() const final;

	private:
		TaskStub(Graph& graph, Task* parent, const Data::Entity& entity);

		const Data::Entity& entity;

		bool accept(TaskVisitor& v) const final { return true; };
	};
}