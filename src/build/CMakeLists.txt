target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/DeleteTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/Delta.cpp
	${CMAKE_CURRENT_LIST_DIR}/Director.cpp
	${CMAKE_CURRENT_LIST_DIR}/ExtractTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/FileSource.cpp
	${CMAKE_CURRENT_LIST_DIR}/Graph.cpp
	${CMAKE_CURRENT_LIST_DIR}/JadeExposedListDelta.cpp
	${CMAKE_CURRENT_LIST_DIR}/MoveClassTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/RenameTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/ReorgTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/RestartTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/SchemaDelta.cpp
	${CMAKE_CURRENT_LIST_DIR}/Task.cpp
	${CMAKE_CURRENT_LIST_DIR}/TaskCluster.cpp
	${CMAKE_CURRENT_LIST_DIR}/TaskState.cpp
	${CMAKE_CURRENT_LIST_DIR}/TaskStub.cpp
)

include(${CMAKE_CURRENT_LIST_DIR}/classic/CMakeLists.txt)