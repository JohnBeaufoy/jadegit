#include "ReorgTask.h"
#include "TaskVisitor.h"
#include <build/Builder.h>

namespace JadeGit::Build
{
	ReorgTask::operator std::string() const
	{
		return "Reorg";
	}
	
	bool ReorgTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}

	bool ReorgTask::execute(Builder& builder) const
	{
		builder.Reorg();
		return true;
	}
}