#include "TaskState.h"
#include "Task.h"
#include "TaskCluster.h"
#include <Singleton.h>
#include <Log.h>

using namespace std;

namespace JadeGit::Build
{
	bool TaskState::executeEnter(const Task& task, const Task* cascade) const
	{
		for (auto& child : task.children)
			if (!child->state->executeEnter(*child, cascade))
				return false;

		return true;
	}

	bool TaskState::executeExit(const Task& task, bool result, size_t& visited, bool& aborted, IProgress* progress) const
	{
		for (auto& child : task.children)
			if (!child->state->executeExit(*child, result, visited, aborted, progress))
				return false;

		return true;
	}
	
	void TaskState::reset(const Task& task, int depth) const
	{
		if (task.cluster)
			task.cluster->state->reset(*task.cluster);

		for (auto& child : task.children)
			child->state->reset(*child, depth + 1);
	}

	const TaskState* TaskState::get(const Task& task) const
	{
		return task.state;
	}

	const TaskState* TaskState::get(const TaskCluster& cluster) const
	{
		return cluster.state;
	}

	void TaskState::set(const Task& task, const TaskState* state) const
	{
		assert(state);
		task.state = state;
	}

	void TaskState::set(const TaskCluster& cluster, const TaskState* state) const
	{
		assert(state);
		cluster.state = state;
	}

	bool TaskState::traversable(const Task& task) const
	{
		return !task.children_processing && !task.predecessors_processing;
	}

	bool TaskState::traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const
	{
		assert(cascade);

		// Try visit all children
		if (!task.children.empty())
		{
			LOG_TRACE("build: " << string(depth, '\t') << "visit children");

			try
			{
				for (auto& child : task.children)
				{
					if (!child->state->traverse(*child, visited, visitor, aborted, progress, depth + 1, cascade, cluster) || aborted)
						return false;
				}
			}
			catch (...)
			{
				throw_with_nested(runtime_error(format("{} task failed while processing children", static_cast<string>(task))));
			}
		}

		return true;
	}

	bool TaskState::traverse(const TaskCluster& cluster, const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{
		throw logic_error("Invalid state for task cluster traversal");
	}

	class ProcessingState : public TaskState, public Singleton<ProcessingState>
	{
	public:
		bool execute(const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster = nullptr) const;
		bool execute(const TaskCluster& cluster, const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const;

	protected:
		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			assert(cascade == &task);
			return TaskState::executeEnter(task, cascade);
		}

		bool traversable(const Task& task) const final
		{
			return false;
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const final
		{
			LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << " (cyclic!)");
			throw runtime_error(format("{} task processing aborted due to cyclic dependency", static_cast<string>(task)));
		}

		bool traverse(const TaskCluster& cluster, const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << " (cyclic!)");
			throw runtime_error(format("{} task cluster processing aborted due to cyclic dependency", static_cast<string>(task)));
		}

		void updateCounts(const Task& task, short increment) const
		{
			// Update children processing count for parents
			const Task* parent = task.parent;
			while (parent && parent->state != this)
			{
				parent->children_processing += increment;
				parent = parent->parent;
			}

			// Update predecessors processing count for successors
			if (!task.successors.empty())
			{
				queue<const Task*> q;
				q.push(&task);

				while (!q.empty())
				{
					auto& task = *q.front();
					q.pop();

					for (auto& successor : task.successors)
					{
						if (successor->state == this)
							continue;

						// Update count on successor
						successor->predecessors_processing += increment;

						// Update successor parents, provided they're not a common ancestor
						const Task* parent = successor->parent;
						while (parent && parent->state != this && !task.isChildOf(parent))
						{
							parent->predecessors_processing += increment;
							parent = parent->parent;
						}

						// Queue successor to update successors of that
						q.push(successor);
					}
				}
			}
		}

	private:
		void reset(const Task& task, int depth) const final
		{
			throw logic_error("Cannot reset processing state for task");
		}

		void reset(const TaskCluster& cluster) const final 
		{
			throw logic_error("Cannot reset processing state for task cluster");
		}
	};

	bool TaskState::isProcessing() const
	{
		return this == ProcessingState::Instance();
	}

	template <bool Repeat>
	class IncompleteState : public TaskState, public Singleton<IncompleteState<Repeat>>
	{
	protected:
		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const final
		{
			// Repeat during cascade only, return true immediately otherwise for task already executed previously
			if constexpr (Repeat)
			{
				if (!cascade)
					return true;
			}

			// Determine if parent task can be executed instead of child (if not already cascading through children)
			const Task* parent = cascade ? nullptr : task.parent;
			const Task* substitute = nullptr;
			while (parent)
			{
				// Check task can be executed without creating a cycle (because it's already being partially processed somehow)
				if (!get(*parent)->traversable(*parent))
					break;

				// Substitute top-level tasks, incomplete tasks, or complete tasks where parent is incomplete
				if (!parent->parent || !parent->complete || !parent->parent->complete)
					substitute = parent;

				parent = parent->parent;
			}

			// Execute substitute determined
			if (substitute)
			{
				LOG_TRACE("build: " << string(depth, '\t') << "substituting: " << static_cast<string>(*substitute) << ", for: " << static_cast<string>(task));

				try
				{
					return ProcessingState::Instance()->execute(*substitute, Repeat, visited, visitor, aborted, progress, depth, cascade, cluster);
				}
				catch (...)
				{
					throw_with_nested(runtime_error(format("{} task failed while processing substitute", static_cast<string>(task))));
				}
			}
			
			// Execute task
			return ProcessingState::Instance()->execute(task, Repeat, visited, visitor, aborted, progress, depth, cascade, cluster);
		}

		bool traverse(const TaskCluster& cluster, const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Repeat during cascade only, return true immediately otherwise for task cluster already executed previously
			if constexpr (Repeat)
			{
				if (!cascade)
					return true;
			}

			// Execute task cluster
			return ProcessingState::Instance()->execute(cluster, task, Repeat, visited, visitor, aborted, progress, depth, cascade);
		}

		bool executeEnter(const Task& task, const Task* cascade) const final;

	private:
		void reset(const TaskCluster& cluster) const final {}
	};

	const TaskState* TaskState::initialState()
	{
		return IncompleteState<false>::Instance();
	}

	class CompletedState : public TaskState, public Singleton<CompletedState>
	{
	protected:
		void reset(const Task& task, int depth) const final
		{
			// Suppress reset for top-level tasks, as these have been fully completed without any need to repeat again
			if (!depth)
				return;

			// Reset state to incomplete state, ready for repeating if required
			set(task, IncompleteState<true>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}

		void reset(const TaskCluster& cluster) const final
		{
			// If not suppressed above, reset to incomplete state ready for repeating if required
			set(cluster, IncompleteState<true>::Instance());
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const final
		{
			// Return true immediately for tasks already completed
			return true;
		}

		bool traverse(const TaskCluster& cluster, const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Shouldn't happen, all tasks within cluster should be completed at same time if required
			throw logic_error("Task cluster processing already completed");
		}
	};

	template <bool Repeat>
	class SuppressedState : public TaskState, public Singleton<SuppressedState<Repeat>>
	{
	protected:
		void reset(const Task& task, int depth) const final
		{
			// Revert to incomplete state
			set(task, IncompleteState<Repeat>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}

		void reset(const TaskCluster& cluster) const final
		{
			// Revert to incomplete state
			set(cluster, IncompleteState<Repeat>::Instance());
		}
		
		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const final
		{
			// Process task when required by successor task (suppression applies during cascade execution only)
			if (!cascade)
			{
				// Return true immediately when task has already been completed previously
				if (Repeat)
					return true;
				
				// Execute required task
				LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << " (required)");
				return ProcessingState::Instance()->execute(task, false, visited, visitor, aborted, progress, depth, nullptr, cluster);
			}

			LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << (Repeat ? " (not required again)" : " (not required)"));
			return TaskState::traverse(task, visited, visitor, aborted, progress, depth, cascade, cluster);
		}

		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			// Revert to incomplete state when task is required
			if (task.required(cascade, Repeat))
				set(task, IncompleteState<Repeat>::Instance());

			return TaskState::executeEnter(task, cascade);
		}

		bool executeExit(const Task& task, bool result, size_t& visited, bool& aborted, IProgress* progress) const final
		{
			if (result)
			{
				// Update visited & progress if task wasn't being repeated
				if constexpr (!Repeat)
				{
					visited++;

					// Increment progress, aborting traversal if requested
					if (aborted = (progress && !progress->step()))
						return false;
				}

				// Move to completed state
				set(task, CompletedState::Instance());
			}
			else
			{
				// Revert to incomplete state
				set(task, IncompleteState<Repeat>::Instance());
			}

			return TaskState::executeExit(task, result, visited, aborted, progress);
		}
	};

	template <bool Repeat>
	bool IncompleteState<Repeat>::executeEnter(const Task& task, const Task* cascade) const
	{
		// Suppress child tasks when they're not required
		if (!task.required(cascade, Repeat))
			set(task, SuppressedState<Repeat>::Instance());

		return TaskState::executeEnter(task, cascade);
	}

	template <bool Repeat>
	class DeferredState : public TaskState, public Singleton<DeferredState<Repeat>>
	{
	protected:
		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			// Return false immediately for tasks that've been deferred until next iteration
			assert(cascade && cascade != &task);
			return false;
		}
		
		void reset(const Task& task, int depth) const final
		{
			// Reset state to indicate task can be reassessed/tried again during next iteration
			set(task, IncompleteState<Repeat>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}

		void reset(const TaskCluster& cluster) const final
		{
			// Reset state to indicate cluster can be reassessed/tried again during next iteration
			set(cluster, IncompleteState<Repeat>::Instance());
		}

		bool traversable(const Task& task) const final
		{
			// Cannot traverse tasks that've already been deferred until next iteration
			return false;
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const final
		{
			// Return true immediately for repeats (as it was done previously), false otherwise for tasks that've been deferred until next iteration
			assert(!cascade || !Repeat);
			return Repeat;
		}

		bool traverse(const TaskCluster& cluster, const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Return true immediately for repeats (as it was done previously), false otherwise for tasks that've been deferred until next iteration
			assert(!cascade || !Repeat);
			return Repeat;
		}
	};

	bool ProcessingState::execute(const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade, const TaskCluster* cluster) const
	{		
		// Redirect to cluster traversal to handle peers simultaneously if not already being processed
		assert(!cluster || task.cluster == cluster);
		if (!cluster && task.cluster)
			return task.cluster->state->traverse(*task.cluster, task, visited, visitor, aborted, progress, depth, cascade);

		assert(cascade || !repeat);

		LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << (repeat ? " (enter again)" : " (enter)"));

		// Update task state to reflect it's being processed
		set(task, this);

		// Update processing counts on related tasks
		if (!repeat)
			updateCounts(task, 1);

		// Determine whether children tasks are still required
		bool result = true;
		if (!task.isChildOf(cascade))
			result = result && executeEnter(task, &task);

		// Try visit all prerequisites
		if (result && !repeat)
		{
			auto tasks = task.prerequisites(cascade);
			if (!tasks.empty())
			{
				LOG_TRACE("build: " << string(depth, '\t') << "visit " << (cascade ? "predecessors" : "prerequisites"));

				try
				{
					// Sort tasks
					stable_sort(tasks.begin(), tasks.end(), [](const Task* a, const Task* b) { return *a < *b; });

					for (auto& task : tasks)
					{
						result = task->traverse(visited, visitor, aborted, progress, depth + 1, nullptr, cluster) && result;
						if (aborted)
							return false;
					}
				}
				catch (...)
				{
					throw_with_nested(runtime_error(format("{} task failed while processing {}", static_cast<string>(task), cascade ? "predecessors" : "prerequisites")));
				}
			}
		}

		// Try visit all children
		result = result && TaskState::traverse(task, visited, visitor, aborted, progress, depth, task.isChildOf(cascade) ? cascade : &task);
		if (aborted)
			return false;

		// Try visit self, provided all prerequisites & children have been completed
		if (result)
		{
			LOG_TRACE("build: " << string(depth, '\t') << "visit self");

			try
			{
				if ((result = task.accept(visitor)) && !repeat)
				{
					visited++;

					// Increment progress, aborting traversal if requested
					if (aborted = (progress && !progress->step()))
						return false;
				}
			}
			catch (...)
			{
				throw_with_nested(runtime_error(format("{} task failed", static_cast<string>(task))));
			}
		}

		// Update processing counts on related tasks
		if (!repeat)
			updateCounts(task, -1);

		// Update state to reflect outcome
		if (result)
			set(task, CompletedState::Instance());
		else
		{
			if (repeat)
				set(task, DeferredState<true>::Instance());
			else
				set(task, DeferredState<false>::Instance());
		}

		// Finalize tasks (during which processing may be aborted changing the result)
		result = executeExit(task, result, visited, aborted, progress) && result;

		LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << (result ? (repeat ? " (completed again)" : " (completed)") : " (deferred)"));
		return result;
	}

	bool ProcessingState::execute(const TaskCluster& cluster, const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{
		assert(cascade || !repeat);

		LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << (repeat ? " (enter cluster again)" : " (enter cluster)"));

		// Update cluster state to reflect it's being processed
		set(cluster, this);

		// Try visit all prerequisites
		bool result = true;
		if (!repeat)
		{
			// Initialize set of tasks to be excluded from prerequisites (those already within cluster)
			std::set<const Task*> excluded;
			for (auto& task : cluster.tasks)
				excluded.insert(task);

			// Collate prerequisites across all tasks within cluster
			vector<const Task*> prerequisites;
			for (auto& task : cluster.tasks)
			{
				auto tasks = task->prerequisites(task->isChildOf(cascade) ? cascade : nullptr);
				for (auto& task : tasks)
				{
					if (!excluded.contains(task))
					{
						prerequisites.push_back(task);
						excluded.insert(task);
					}
				}
			}

			// Try visit collated prerequisites (outside of cluster)
			if (!prerequisites.empty())
			{
				LOG_TRACE("build: " << string(depth, '\t') << "visit " << (cascade ? "predecessors" : "prerequisites"));

				try
				{
					stable_sort(prerequisites.begin(), prerequisites.end(), [](const Task* a, const Task* b) { return *a < *b; });

					for (auto& task : prerequisites)
					{
						result = task->traverse(visited, visitor, aborted, progress, depth + 1) && result;
						if (aborted)
							return false;
					}
				}
				catch (...)
				{
					throw_with_nested(runtime_error(format("{} task failed while processing {}", static_cast<string>(task), cascade ? "predecessors" : "prerequisites")));
				}
			}
		}

		// Try visit peer tasks in cluster, provided all prerequisites have been completed (outside of cluster)
		if (result)
		{
			LOG_TRACE("build: " << string(depth, '\t') << "visit peer tasks");

			try
			{
				// Handle task which prompted processing cluster upfront
				if (result = task.traverse(visited, visitor, aborted, progress, depth + 1, task.isChildOf(cascade) ? cascade : nullptr, &cluster))
				{
					// Handle peer tasks subject to initial task being successful
					for (auto& task : cluster.tasks)
					{
						if (!task->traverse(visited, visitor, aborted, progress, depth + 1, task->isChildOf(cascade) ? cascade : nullptr, &cluster))
						{
							if (aborted)
								return false;

							// Throw exception when peer task cannot be processed after initial task (peer tasks must be processed together)
							// NOTE: This doesn't cover scenario where initial task may fail, but a predecessor within cluster may have succeeded
							throw logic_error("Peer task failed after initial task succeeded");
						}
					}
				}
				else if (aborted)
					return false;
			}
			catch (...)
			{
				throw_with_nested(runtime_error(format("{} task failed while processing peers", static_cast<string>(task))));
			}
		}

		// Update cluster state to reflect outcome
		if (result)
			set(cluster, CompletedState::Instance());
		else
		{
			if (repeat)
				set(cluster, DeferredState<true>::Instance());
			else
				set(cluster, DeferredState<false>::Instance());
		}

		LOG_TRACE("build: " << string(depth, '\t') << static_cast<string>(task) << (result ? (repeat ? " (completed cluster again)" : " (completed cluster)") : " (deferred cluster)"));
		return result;
	}
}