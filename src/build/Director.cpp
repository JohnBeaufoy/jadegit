#include <jadegit/build/Director.h>
#include <jadegit/build/Source.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include <build/Builder.h>
#include <data/config/DeployConfig.h>
#include <data/storage/ObjectFileStorage.h>
#include <data/storage/ObjectLoader.h>
#include "Graph.h"
#include "classic/ClassicFileBuilder.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class Director::Impl
	{
	public:
		// Setup director to build from source
		Impl(const Source& source, Builder& builder, IProgress* progress) : source(source), builder(builder), progress(progress),
			previous(source.Previous(), builder.platformVersion()),
			latest(source.Latest(), builder.platformVersion()),
			graph(previous, latest)
		{
			config = make_unique<DeployConfig>(latest);
		}

		// Build deployment
		bool build()
		{
			if (progress && !progress->start(4))
				return false;

			if (!load())
				return false;

			// Analyze differences
			if (!graph.analyze(progress))
				return false;

			// TODO: Use factory for file builder?
			unique_ptr<FileBuilder> fileBuilder = make_unique<Classic::ClassicFileBuilder>(builder, latest.GetRootSchema().version);

			// Traverse graph and build
			if (!graph.traverse(*fileBuilder, progress, "Building"))
				return false;

			// Add deployment scripts
			buildScripts(builder);

			return !progress || progress->finish();
		}

	private:
		const Source& source;
		Builder& builder;
		IProgress* progress = nullptr;
		Graph graph;
		Assembly previous;
		Assembly latest;
		unique_ptr<DeployConfig> config;

		void buildScripts(Builder& builder)
		{
			if (!this->config)
				return;

			for (auto& config : this->config->scripts)
			{
				Build::Script script;

				if (config.schema)
					script.schema = config.schema->name;

				if (config.app)
					script.app = config.app->name;

				if (config.method)
				{
					if (config.method->schemaType->schema != config.schema)
						script.executeSchema = config.method->schemaType->schema->name;

					script.executeClass = config.method->schemaType->name;
					script.executeMethod = config.method->name;
					script.executeTypeMethod = config.method->isTypeMethod();
				}

				script.executeParam = config.param;
				script.executeTransient = config.transient;

				builder.addScript(script);
			}
		}

		bool load()
		{
			// Read source, queuing entities to be loaded & added to graph for analysis
			if (!source.read([&](const File& file, bool side) { (side ? latest : previous).getStorage().getLoader().queue(file, (side || !source.Latest()), bind(&Graph::Add, &graph, placeholders::_1, side)); }, progress))
				return false;

			// Load entities
			if (progress && !progress->start(previous.getStorage().getLoader().pending() + latest.getStorage().getLoader().pending(), "Loading"))
				return false;

			// Load config upfront (may resolve entities)
			if (config)
				config->load();

			if (!previous.getStorage().getLoader().load(progress))
				return false;

			if (!latest.getStorage().getLoader().load(progress))
				return false;

			return !progress || progress->finish();
		}
	};

	Director::Director(const Source& source, Builder& builder, IProgress* progress)
	{
		impl = make_unique<Impl>(source, builder, progress);
	}

	Director::~Director() = default;

	bool Director::build()
	{
		return impl->build();
	}
}