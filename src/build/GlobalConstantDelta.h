#pragma once
#include "ConstantDelta.h"
#include <jadegit/data/GlobalConstant.h>

namespace JadeGit::Build
{
	class GlobalConstantDelta : public ConstantDelta<GlobalConstant>
	{
	public:
		GlobalConstantDelta(Graph& graph) : ConstantDelta<GlobalConstant>(graph, "GlobalConstant") {}
		
	protected:
		using ConstantDelta<GlobalConstant>::graph;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!ConstantDelta::AnalyzeEnter())
			{
				// Handle deleting constants before category is deleted
				this->addDeletionDependency(previous->category, GetDeletion());

				return false;
			}

			// Analyze constant category
			if (auto delta = graph.Analyze<ISchemaComponentDelta>(latest->category))
			{
				// Make global constant definition a prerequisite to category definition
				// This is intended to force global constant categories to be extracted in correct order, duplicating if necessary to
				// satisfy cross-dependencies between global constants where a group of dependent constants may span mulitple categories
				delta->GetDefinition()->addPredecessor(GetDefinition());
			}

			return true;
		}
	};
}