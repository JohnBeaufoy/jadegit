#pragma once
#include "ClassDelta.h"
#include <jadegit/data/ActiveXLibrary.h>

namespace JadeGit::Build
{
	class ActiveXLibraryDelta : public SchemaComponentDelta<ActiveXLibrary>
	{
	public:
		ActiveXLibraryDelta(Graph& graph) : SchemaComponentDelta(graph, "ActiveXLibrary") {}

	protected:
		bool AnalyzeEnter() final
		{
			// Analyze base class
			baseClass = graph.Analyze<IClassDelta>(latest ? latest->getBaseClass() : previous->getBaseClass());

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			// Definition depends on related classes/features
			auto definition = this->GetDefinition();
			for (auto& cls : latest->classes)
			{
				this->addDefinitionDependency(cls.second->getOriginal(), definition);

				for (auto& child : cls.second->children)
				{
					if (auto feature = dynamic_cast<ActiveXFeature*>(child))
						this->addDefinitionDependency(feature->getOriginal(), definition);
				}
			}

			return true;
		}

		Task* handleDeletion(Task* parent) const final
		{
			// Deletion implied by base class deletion
			if (baseClass)
				return baseClass->GetDeletion();

			return SchemaComponentDelta::handleDeletion(parent);
		}

		Task* handleRename() final
		{
			// Rename implied by base class rename
			if (baseClass)
				return baseClass->GetRename();

			return SchemaComponentDelta::handleRename();
		}

	private:
		IClassDelta* baseClass = nullptr;

		// Override default order to analyse after related classes/features
		int ordinal() const final
		{
			return 4;
		}
	};
}