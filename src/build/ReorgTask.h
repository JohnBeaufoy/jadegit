#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class Builder;

	class ReorgTask : public Task
	{
	public:
		ReorgTask(Graph& graph) : Task(graph, false, 999)
		{
		}

		operator std::string() const final;

		bool execute(Builder& builder) const;

	protected:
		bool accept(TaskVisitor& v) const final;
	};
}