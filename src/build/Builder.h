#pragma once
#include <jadegit/Version.h>
#include <memory>
#include "Script.h"

namespace JadeGit::Build
{
	class Builder
	{
	public:
		virtual ~Builder() {};

		virtual Version platformVersion() const = 0;

		virtual void RegisterSchema(const std::string& schema) {}
		virtual void DeregisterSchema(const std::string& schema) {}

		virtual std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) = 0;
		virtual std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) = 0;
		virtual std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) = 0;
		virtual void addScript(const Script& script) = 0;

		virtual void Reorg() {};
		virtual void Flush(bool final) {}
	};
}