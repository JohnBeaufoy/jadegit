if(NOT WITH_SCHEMA)
	return()
endif()

enable_language(CSharp)

add_library(jadegitauth SHARED "")

target_compile_options(jadegitauth PRIVATE "/langversion:latest")

set_target_properties(jadegitauth PROPERTIES 
	VS_GLOBAL_ROOTNAMESPACE JadeGit.Authentication
	VS_DOTNET_TARGET_FRAMEWORK_VERSION "v4.8"
	VS_DOTNET_REFERENCES "System;System.Diagnositics.Process;System.Configuration"
)

install(TARGETS jadegitauth
		RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
		LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES $<TARGET_PDB_FILE:jadegitauth> DESTINATION ${CMAKE_INSTALL_BINDIR} OPTIONAL)

# Generate .NET Version file.
configure_file(${CMAKE_CURRENT_LIST_DIR}/VersionInfo.cs.in ${CMAKE_CURRENT_BINARY_DIR}/VersionInfo.cs)

target_sources(jadegitauth PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Authentication.cs
	${CMAKE_CURRENT_BINARY_DIR}/VersionInfo.cs
)