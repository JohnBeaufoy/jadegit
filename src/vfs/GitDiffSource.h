#pragma once
#include "GitFileSystem.h"
#include <jadegit/build/Source.h>

namespace JadeGit
{
	class GitDiffSource : public Build::Source
	{
	public:
		GitDiffSource(git_repository* repo, const GitFileSystem* previous, const GitFileSystem* latest);

		const GitFileSystem* const previous;
		const GitFileSystem* const latest;

		bool read(Source::Reader reader, IProgress* progress) const override;

		const FileSystem& Previous() const override
		{
			if (previous)
				return *previous;

			return NullFileSystem::get();
		}

		const FileSystem& Latest() const override
		{
			if (latest)
				return *latest;

			return NullFileSystem::get();
		}
	
	private:
		git_repository* const repo;

		mutable std::unique_ptr<git_diff> diff;
	};
}