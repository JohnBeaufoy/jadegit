#include <jadegit/vfs/RecursiveFileIterator.h>

namespace JadeGit
{
	RecursiveFileIterator::RecursiveFileIterator() noexcept {}

	RecursiveFileIterator::RecursiveFileIterator(const File& folder)
	{
		// Push first iterator onto stack
		stack.push(folder.begin());

		// Pop immediately if invalid
		if (stack.top() == end(stack.top()))
			stack.pop();
		// Otherwise set flag to recurse if first entry is a directory
		else
			recurse = (**this).isDirectory();
	}

	const File& RecursiveFileIterator::operator*() const
	{
		return stack.top().operator*();
	}

	const File* RecursiveFileIterator::operator->() const
	{
		return stack.top().operator->();
	}

	RecursiveFileIterator& RecursiveFileIterator::operator++()
	{
		// Push new iterator onto stack if recursion flag set
		if (recurse)
			stack.push((**this).begin());
		// Increment iterator already at top of the stack
		else if(!stack.empty())
			++stack.top();

		// Pop iterators which have ended
		if (!stack.empty())
		{
			while (stack.top() == end(stack.top()))
			{
				stack.pop();
				if (stack.empty())
					break;

				// Increment iterator we've just backtracked to
				++stack.top();
			}
		}

		// Set recursion flag for next iteration
		recurse = !stack.empty() && (**this).isDirectory();

		return *this;
	}

	bool RecursiveFileIterator::operator==(const RecursiveFileIterator& it) const
	{
		return recurse == it.recurse && stack == it.stack;
	}

	bool RecursiveFileIterator::operator!=(const RecursiveFileIterator& it) const
	{
		return !((*this) == it);
	}

	// Range based support
	RecursiveFileIterator begin(RecursiveFileIterator iter) noexcept
	{
		return iter;
	}

	RecursiveFileIterator end(const RecursiveFileIterator&) noexcept
	{
		return RecursiveFileIterator();
	}
}