#include "MemoryFile.h"

using namespace std;

namespace JadeGit
{
	MemoryFileNode::MemoryFileNode() {}
	MemoryFileNode::MemoryFileNode(MemoryFileDirectory* parent, const std::string& name) : parent(parent), name(name)
	{
		if (parent->contents[name])
			throw runtime_error("File already exists");
		
		/* Add to parent collection */
		parent->contents[name] = this;
	}

	MemoryFileNode::~MemoryFileNode()
	{
		/* Remove from parent collection */
		if (parent)
			parent->contents.erase(name);
	}

	std::filesystem::path MemoryFileNode::path() const
	{
		return parent ? parent->path() / name : name;
	}

	void MemoryFileNode::remove()
	{
		if (!parent)
			throw logic_error("Cannot delete root folder");

		delete this;
	}

	MemoryFileDirectory::MemoryFileDirectory() : MemoryFileNode() {}
	MemoryFileDirectory::MemoryFileDirectory(MemoryFileDirectory* parent, const std::string& name) : MemoryFileNode(parent, name) {}

	MemoryFileDirectory::~MemoryFileDirectory()
	{
		/* Clean-up children */
		/* NOTE: This doesn't remove children from collection, this is handled by base destructor */
		auto iter = contents.begin();
		while (iter != contents.end())
		{
			MemoryFileNode* child = (*iter).second;
			iter++;
			delete child;
		}
	}

	MemoryFileNode* MemoryFileDirectory::find(const std::filesystem::path& path) const
	{
		if (path.has_parent_path())
		{
			auto parent = path.parent_path();
			auto node = find(parent);
			return node && node->isDirectory() ? static_cast<MemoryFileDirectory*>(node)->find(std::filesystem::relative(path, parent)) : nullptr;
		}

		auto iter = contents.find(path.generic_string());
		return iter != contents.end() ? (*iter).second : nullptr;
	}

	MemoryFileDirectory* MemoryFileDirectory::make_directory(const std::filesystem::path& path)
	{
		if (path.has_parent_path())
		{
			auto parent = path.parent_path();
			return make_directory(parent)->make_directory(std::filesystem::relative(path, parent));
		}

		if (!path.is_relative())
			throw runtime_error("File path must be relative");

		MemoryFileNode* file = contents[path.generic_string()];
		if (file)
		{
			if (file->isDirectory())
				return static_cast<MemoryFileDirectory*>(file);
			else
				throw runtime_error("Folder cannot be created as file with same already exists");
		}

		return new MemoryFileDirectory(this, path.generic_string());
	}

	MemoryFile* MemoryFileDirectory::make(const std::filesystem::path& path)
	{
		if (path.has_parent_path())
		{
			auto parent = path.parent_path();
			return make_directory(parent)->make(std::filesystem::relative(path, parent));
		}

		if (!path.has_filename() || path != path.filename())
			throw runtime_error("Invalid filename");

		return new MemoryFile(this, path.generic_string());
	}

	std::map<std::string, MemoryFileNode*>::const_iterator MemoryFileDirectory::begin() const
	{
		return contents.begin();
	}

	std::map<std::string, MemoryFileNode*>::const_iterator MemoryFileDirectory::end() const
	{
		return contents.end();
	}
}