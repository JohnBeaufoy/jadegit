#pragma once
#include "Entity.h"
#include "ObjectValue.h"
#include "RootSchema/JadeHTMLDocumentMeta.h"

namespace JadeGit::Data
{
	class HTMLClass;
	class Schema;

	DECLARE_OBJECT_CAST(HTMLClass)

	class JadeHTMLDocument : public MajorEntity<JadeHTMLDocument, Entity>
	{
	public:
		using Parents = ObjectParents<Schema>;
		static const std::filesystem::path subFolder;

		JadeHTMLDocument(Schema* parent, const Class* dataClass, const char* name = nullptr);

		ObjectValue<HTMLClass*, &JadeHTMLDocumentMeta::htmlClass> htmlClass;
		ObjectValue<Schema* const, &JadeHTMLDocumentMeta::schema> schema;

		void Accept(EntityVisitor& v) override;
	};
}