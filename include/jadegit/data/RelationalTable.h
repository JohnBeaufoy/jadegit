#pragma once
#include "NamedObjectDict.h"
#include "RelationalAttribute.h"
#include "RootSchema/RelationalTableMeta.h"

namespace JadeGit::Data
{
	class ExternalRPSClassMap;
	class JadeOdbcView;

	class RelationalTable : public NamedObject
	{
	public:
		using Parents = ObjectParents<ExternalRPSClassMap, JadeOdbcView>;

		RelationalTable(JadeOdbcView& parent, const Class* dataClass, const char* name);
		RelationalTable(ExternalRPSClassMap& parent, const Class* dataClass, const char* name);

		NamedObjectDict<RelationalAttribute, &RelationalTableMeta::attributes> attributes;
		ObjectValue<Array<Class*>, &RelationalTableMeta::classes> classes;
		ObjectValue<ExternalRPSClassMap* const, &RelationalTableMeta::rpsClassMap> classMap;
		ObjectValue<JadeOdbcView* const, &RelationalTableMeta::rView> view;
	};

	extern template NamedObjectDict<RelationalAttribute, &RelationalTableMeta::attributes>;
	extern template ObjectValue<Array<Class*>, &RelationalTableMeta::classes>;
	extern template ObjectValue<ExternalRPSClassMap* const, &RelationalTableMeta::rpsClassMap>;
	extern template ObjectValue<JadeOdbcView* const, &RelationalTableMeta::rView>;
}