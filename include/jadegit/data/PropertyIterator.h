#pragma once
#include <jadegit/data/Class.h>

namespace JadeGit::Data
{
	class PropertyIterator
	{
	public:
		PropertyIterator() noexcept;
		PropertyIterator(const Class* cls);

		const Property& operator*() const;
		const Property* operator->() const;

		PropertyIterator& operator++();
		bool operator==(const PropertyIterator& iter) const;
		bool operator!=(const PropertyIterator& iter) const;

	private:
		const Class* cls = nullptr;
		EntityDict<Property, &ClassMeta::properties>::const_iterator iter;

		void Recurse();
	};

	// Range based support
	PropertyIterator begin(PropertyIterator iter) noexcept;
	PropertyIterator end(const PropertyIterator&) noexcept;
}