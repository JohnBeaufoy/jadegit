#pragma once
#include "ExternalAttributeMap.h"
#include "ExternalReferenceMap.h"
#include "RootSchema/ExternalClassMapMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;
	class ExternalTable;

	class ExternalClassMap : public ExternalSchemaMap
	{
	public:
		using Parents = ObjectParents<ExternalDatabase>;

		ExternalClassMap(ExternalDatabase& parent, const Class* dataClass, const char* name);

		NamedObjectDict<ExternalAttributeMap, &ExternalClassMapMeta::attributeMaps> attributeMaps;
		ObjectValue<ExternalDatabase* const, &ExternalClassMapMeta::database> database;
		NamedObjectDict<ExternalReferenceMap, &ExternalClassMapMeta::referenceMaps> referenceMaps;
		ObjectValue<Array<ExternalTable*>, &ExternalClassMapMeta::tables> tables;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}