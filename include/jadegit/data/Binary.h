#pragma once
#include <string>
#include <vector>

namespace JadeGit::Data
{
	using Byte = unsigned char;

	class Binary : public std::vector<Byte>
	{
	public:
		Binary() {}
		Binary(const Byte* source, size_t length) : vector(source, source + length) {}

		/* Decodes base64 string */
		Binary(const std::string& base64);

		/* Returns base64 encoded string */
		operator std::string() const;
	};
}