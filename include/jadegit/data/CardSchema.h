#pragma once
#include "MetaSchema.h"

namespace JadeGit::Data
{
	class RootSchema;

	class CardSchema : public MetaSchema
	{
	public:
		CardSchema(Assembly& parent, const RootSchema& superschema, Version version);
	};
}
