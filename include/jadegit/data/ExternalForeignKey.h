#pragma once
#include "ExternalSchemaEntity.h"
#include "RootSchema/ExternalForeignKeyMeta.h"

namespace JadeGit::Data
{
	class ExternalTable;

	class ExternalForeignKey : public ExternalSchemaEntity
	{
	public:
		ExternalForeignKey(ExternalTable& parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalTable* const, &ExternalForeignKeyMeta::table> table;
	};
}