#pragma once
#include <jadegit/data/NamedObjectFactory.h>
#include <jadegit/data/Entity.h>

namespace JadeGit::Data
{
	class EntityFactory : public NamedObjectFactory
	{
	public:
		static EntityFactory& Get();

		class Registration : public NamedObjectFactory::Registration
		{
		public:
			Entity* Create(Component* parent, const Class* dataClass, const char* name) const override = 0;
			Entity* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override = 0;
			virtual Entity* Resolve(const Assembly* assembly, const std::filesystem::path& path, bool expected, bool shallow) const = 0;
			virtual Entity* load(Assembly* assembly, const Class* dataClass, const std::filesystem::path& path) const = 0;
		};

		template<class TDerived>
		void Register(const char* key, const Registration* registrar)
		{
			ObjectFactory::Register<TDerived>(key, registrar);
		}

		template<class TDerived = Entity>
		TDerived* Create(const std::string& key, Component* origin, const char* name) const
		{
			return NamedObjectFactory::Create<TDerived>(key, origin, name);
		}

		template<class TDerived = Entity>
		TDerived* Create(const type_info& type, Component* origin, const char* name) const
		{
			return NamedObjectFactory::Create<TDerived>(type, origin, name);
		}

		using NamedObjectFactory::Resolve;

		template<class TDerived = Entity>
		TDerived* Resolve(const std::string& key, const Component* origin, const QualifiedName& name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return NamedObjectFactory::Resolve<TDerived>(key, origin, name, expected, shallow, inherit);
		}

		template<class TDerived = Entity>
		TDerived* Resolve(const std::string& key, const Component* origin, const QualifiedName* name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return NamedObjectFactory::Resolve<TDerived>(key, origin, name, expected, shallow, inherit);
		}

		template<class TDerived>
		TDerived* Resolve(const Assembly* assembly, const std::filesystem::path & path, bool expected = true, bool shallow = true) const
		{
			return static_cast<TDerived*>(Lookup(typeid(TDerived))->Resolve(assembly, path, expected, shallow));
		}

		template<>
		Assembly* Resolve(const Assembly* assembly, const std::filesystem::path& path, bool expected, bool shallow) const
		{
			return Resolve(assembly, path);
		}

		Entity* load(const std::string& key, Assembly* assembly, const std::filesystem::path& path) const;

		const Registration* Lookup(const std::string& key, const Component* origin, Class*& dataClass, bool required = true) const;
		const Registration* Lookup(const type_info& type) const;

	protected:
		EntityFactory() {}
		
		Assembly* Resolve(const Assembly* assembly, const std::filesystem::path& path) const;
	};
}