#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalIndexKeyMeta : public RootClass<>
	{
	public:
		static const ExternalIndexKeyMeta& get(const Object& object);
		
		ExternalIndexKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		ImplicitInverseRef* const column;
		PrimAttribute* const descending;
		ExplicitInverseRef* const index;
		PrimAttribute* const keySequence;
	};
};
