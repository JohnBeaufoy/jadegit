#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeEditMaskMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeEditMaskMeta& get(const Object& object);
		
		JadeEditMaskMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const autoSize;
		PrimAttribute* const autoTab;
		PrimAttribute* const insertMode;
		PrimAttribute* const languageId;
		PrimAttribute* const mask;
		PrimAttribute* const promptCharacter;
		PrimAttribute* const readOnly;
		PrimAttribute* const selectionStyle;
		PrimAttribute* const text;
	};
};
