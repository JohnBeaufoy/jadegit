#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ActiveXFeatureMeta : public RootClass<>
	{
	public:
		static const ActiveXFeatureMeta& get(const Object& object);
		
		ActiveXFeatureMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const feature;
		PrimAttribute* const memid;
	};
};
