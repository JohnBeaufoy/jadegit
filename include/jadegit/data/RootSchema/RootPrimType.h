#pragma once
#include "../MetaSchema/MetaType.h"
#include "../PrimType.h"

namespace JadeGit::Data
{
	class RootPrimType : public MetaType<PrimType>
	{
	public:
		RootPrimType(RootSchema& parent, const Character* name);
	};
}