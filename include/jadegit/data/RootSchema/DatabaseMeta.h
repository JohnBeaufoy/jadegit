#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class DatabaseMeta : public RootClass<>
	{
	public:
		static const DatabaseMeta& get(const Object& object);
		
		DatabaseMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const classMaps;
		ExplicitInverseRef* const dbFiles;
		ExplicitInverseRef* const defaultFile;
		PrimAttribute* const path;
		ExplicitInverseRef* const schema;
		PrimAttribute* const serverName;
	};
};
