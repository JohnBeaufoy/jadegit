#pragma once
#include "RoutineMeta.h"

namespace JadeGit::Data
{
	class FunctionMeta : public RootClass<>
	{
	public:
		static const FunctionMeta& get(const Object& object);
		
		FunctionMeta(RootSchema& parent, const RoutineMeta& superclass);
	
		ExplicitInverseRef* const schema;
	};
};
