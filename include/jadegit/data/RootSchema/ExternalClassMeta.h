#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMeta : public RootClass<>
	{
	public:
		static const ExternalClassMeta& get(const Object& object);
		
		ExternalClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		PrimAttribute* const columnAttributeInfoList;
		PrimAttribute* const extensionString;
		PrimAttribute* const extensionStringMap;
		ExplicitInverseRef* const externalDatabase;
		ExplicitInverseRef* const externalSchemaMap;
		PrimAttribute* const fromList;
		PrimAttribute* const orderByList;
		PrimAttribute* const rowidPredicate;
		PrimAttribute* const rowidPredicateInfo;
		PrimAttribute* const selectList;
		PrimAttribute* const updatable;
		PrimAttribute* const wherePredicate;
	};
};
