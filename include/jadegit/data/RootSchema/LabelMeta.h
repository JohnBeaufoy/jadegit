#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class LabelMeta : public RootClass<GUIClass>
	{
	public:
		static const LabelMeta& get(const Object& object);
		
		LabelMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignment;
		PrimAttribute* const autoSize;
		PrimAttribute* const caption;
		PrimAttribute* const formatOut;
		PrimAttribute* const hyperlink;
		PrimAttribute* const noPrefix;
		PrimAttribute* const transparent;
		PrimAttribute* const wordWrap;
	};
};
