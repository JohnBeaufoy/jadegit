#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class JadeImportedClassMeta : public RootClass<>
	{
	public:
		static const JadeImportedClassMeta& get(const Object& object);
		
		JadeImportedClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		ExplicitInverseRef* const exportedClass;
		ExplicitInverseRef* const importedConstants;
		ExplicitInverseRef* const importedMethods;
		ExplicitInverseRef* const importedProperties;
		PrimAttribute* const incomplete;
		ExplicitInverseRef* const package;
	};
};
