#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class WindowMeta : public RootClass<GUIClass>
	{
	public:
		static const WindowMeta& get(const Object& object);
		
		WindowMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const backBrushStyle;
		PrimAttribute* const backColor;
		PrimAttribute* const borderStyle;
		PrimAttribute* const bubbleHelp;
		PrimAttribute* const description;
		PrimAttribute* const disableReason;
		PrimAttribute* const dragCursor;
		PrimAttribute* const enabled;
		PrimAttribute* const height;
		PrimAttribute* const helpContextId;
		PrimAttribute* const helpKeyword;
		PrimAttribute* const ignoreSkin;
		PrimAttribute* const left;
		PrimAttribute* const mouseCursor;
		PrimAttribute* const mousePointer;
		PrimAttribute* const name;
		PrimAttribute* const securityLevelEnabled;
		PrimAttribute* const securityLevelVisible;
		PrimAttribute* const skinCategoryName;
		PrimAttribute* const tag;
		PrimAttribute* const top;
		PrimAttribute* const visible;
		PrimAttribute* const width;
	};
};
