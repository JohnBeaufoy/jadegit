#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalColumnMeta : public RootClass<>
	{
	public:
		static const ExternalColumnMeta& get(const Object& object);
		
		ExternalColumnMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const attributeMaps;
		PrimAttribute* const bufferLength;
		PrimAttribute* const columnSize;
		PrimAttribute* const dataType;
		PrimAttribute* const decimalDigits;
		PrimAttribute* const kind;
		ExplicitInverseRef* const leftReferenceMaps;
		PrimAttribute* const length;
		PrimAttribute* const nullability;
		PrimAttribute* const ordinalPosition;
		ExplicitInverseRef* const rightReferenceMaps;
		PrimAttribute* const scope;
		ExplicitInverseRef* const table;
	};
};
