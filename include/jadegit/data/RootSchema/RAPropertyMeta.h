#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RAPropertyMeta : public RootClass<>
	{
	public:
		static const RAPropertyMeta& get(const Object& object);
		
		RAPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	};
};
