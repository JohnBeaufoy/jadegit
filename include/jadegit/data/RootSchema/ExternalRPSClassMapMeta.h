#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalRPSClassMapMeta : public RootClass<>
	{
	public:
		static const ExternalRPSClassMapMeta& get(const Object& object);
		
		ExternalRPSClassMapMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const constraint;
		PrimAttribute* const excluded;
		PrimAttribute* const guid;
		PrimAttribute* const includeInCallback;
		PrimAttribute* const noDeletes;
		ExplicitInverseRef* const rpsClass;
		ExplicitInverseRef* const rpsDatabase;
		PrimAttribute* const tableMode;
		ExplicitInverseRef* const tables;
		PrimAttribute* const typeMapValue;
	};
};
