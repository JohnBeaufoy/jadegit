#pragma once
#include "CollClassMeta.h"

namespace JadeGit::Data
{
	class JadeUserCollClassMeta : public RootClass<>
	{
	public:
		static const JadeUserCollClassMeta& get(const Object& object);
		
		JadeUserCollClassMeta(RootSchema& parent, const CollClassMeta& superclass);
	};
};
