#pragma once
#include "TypeMeta.h"

namespace JadeGit::Data
{
	class PrimTypeMeta : public RootClass<>
	{
	public:
		static const PrimTypeMeta& get(const Object& object);
		
		PrimTypeMeta(RootSchema& parent, const TypeMeta& superclass);
	
		ExplicitInverseRef* const supertype;
	};
};
