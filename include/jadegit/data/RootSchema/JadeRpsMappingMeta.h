#pragma once
#include "RelationalViewMeta.h"

namespace JadeGit::Data
{
	class JadeRpsMappingMeta : public RootClass<>
	{
	public:
		static const JadeRpsMappingMeta& get(const Object& object);
		
		JadeRpsMappingMeta(RootSchema& parent, const RelationalViewMeta& superclass);
	};
};
