#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class SchemaViewMeta : public RootClass<>
	{
	public:
		SchemaViewMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	};
};
