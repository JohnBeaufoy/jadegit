#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class CheckBoxMeta : public RootClass<GUIClass>
	{
	public:
		static const CheckBoxMeta& get(const Object& object);
		
		CheckBoxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignment;
		PrimAttribute* const autoSize;
		PrimAttribute* const caption;
		PrimAttribute* const readOnly;
		PrimAttribute* const transparent;
		PrimAttribute* const value;
	};
};
