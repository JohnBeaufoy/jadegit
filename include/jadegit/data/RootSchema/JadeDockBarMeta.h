#pragma once
#include "JadeDockBaseMeta.h"

namespace JadeGit::Data
{
	class JadeDockBarMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeDockBarMeta& get(const Object& object);
		
		JadeDockBarMeta(RootSchema& parent, const JadeDockBaseMeta& superclass);
	
		PrimAttribute* const alignChildren;
		PrimAttribute* const autoSpacingX;
		PrimAttribute* const autoSpacingY;
	};
};
