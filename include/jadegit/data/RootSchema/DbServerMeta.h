#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class DbServerMeta : public RootClass<>
	{
	public:
		DbServerMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		PrimAttribute* const connectionPort;
		PrimAttribute* const connectionType;
		PrimAttribute* const dbLibrary;
		PrimAttribute* const locationType;
		PrimAttribute* const networkName;
		ExplicitInverseRef* const schema;
	};
};
