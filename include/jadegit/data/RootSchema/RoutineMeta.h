#pragma once
#include "ScriptMeta.h"

namespace JadeGit::Data
{
	class RoutineMeta : public RootClass<>
	{
	public:
		static const RoutineMeta& get(const Object& object);
		
		RoutineMeta(RootSchema& parent, const ScriptMeta& superclass);
	
		PrimAttribute* const entrypoint;
		PrimAttribute* const executionLocation;
		ExplicitInverseRef* const library;
		PrimAttribute* const options;
	};
};
