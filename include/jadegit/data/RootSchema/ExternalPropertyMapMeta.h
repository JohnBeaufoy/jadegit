#pragma once
#include "ExternalSchemaMapMeta.h"

namespace JadeGit::Data
{
	class ExternalPropertyMapMeta : public RootClass<>
	{
	public:
		static const ExternalPropertyMapMeta& get(const Object& object);
		
		ExternalPropertyMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass);
	};
};
