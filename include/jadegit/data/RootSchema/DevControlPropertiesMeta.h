#pragma once
#include "DevelopmentMeta.h"

namespace JadeGit::Data
{
	class DevControlPropertiesMeta : public RootClass<>
	{
	public:
		static const DevControlPropertiesMeta& get(const Object& object);
		
		DevControlPropertiesMeta(RootSchema& parent, const DevelopmentMeta& superclass);
	
		PrimAttribute* const cntrlType;
		PrimAttribute* const name;
		CompAttribute* const optionsList;
		ExplicitInverseRef* const parent;
	};
};
