#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeTextEditMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeTextEditMeta& get(const Object& object);
		
		JadeTextEditMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const edgeColor;
		PrimAttribute* const edgeColumn;
		PrimAttribute* const edgeMode;
		PrimAttribute* const endOfLineMode;
		PrimAttribute* const foldFlags;
		PrimAttribute* const foldSymbols;
		PrimAttribute* const folding;
		PrimAttribute* const indentGuides;
		PrimAttribute* const indentWidth;
		PrimAttribute* const markerMargin;
		PrimAttribute* const readOnly;
		PrimAttribute* const selBackColor;
		PrimAttribute* const tabWidth;
		PrimAttribute* const useTabs;
		PrimAttribute* const viewEndOfLine;
		PrimAttribute* const viewLinenumbers;
		PrimAttribute* const viewWhitespace;
		PrimAttribute* const wrapIndent;
		PrimAttribute* const wrapMode;
		PrimAttribute* const wrapVisualFlags;
	};
};
