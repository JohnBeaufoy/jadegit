#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RARelationOidMeta : public RootClass<>
	{
	public:
		static const RARelationOidMeta& get(const Object& object);
		
		RARelationOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	
		PrimAttribute* const fromFirstClass;
	};
};
