#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class CollectionMeta : public RootClass<CollClass>
	{
	public:
		static const CollectionMeta& get(const Object& object);
		
		CollectionMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};
