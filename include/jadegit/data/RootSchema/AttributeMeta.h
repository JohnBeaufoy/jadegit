#pragma once
#include "PropertyMeta.h"

namespace JadeGit::Data
{
	class AttributeMeta : public RootClass<>
	{
	public:
		static const AttributeMeta& get(const Object& object);
		
		AttributeMeta(RootSchema& parent, const PropertyMeta& superclass);
	
		PrimAttribute* const length;
	};
};
