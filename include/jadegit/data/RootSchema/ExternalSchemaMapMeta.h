#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalSchemaMapMeta : public RootClass<>
	{
	public:
		static const ExternalSchemaMapMeta& get(const Object& object);
		
		ExternalSchemaMapMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const schemaEntity;
	};
};
