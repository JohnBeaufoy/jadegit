#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class OptionButtonMeta : public RootClass<GUIClass>
	{
	public:
		static const OptionButtonMeta& get(const Object& object);
		
		OptionButtonMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignment;
		PrimAttribute* const autoSize;
		PrimAttribute* const caption;
		PrimAttribute* const transparent;
		PrimAttribute* const value;
	};
};
