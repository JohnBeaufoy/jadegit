#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalTableMeta : public RootClass<>
	{
	public:
		static const ExternalTableMeta& get(const Object& object);
		
		ExternalTableMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		PrimAttribute* const aliasName;
		PrimAttribute* const cardinality;
		PrimAttribute* const catalogName;
		ExplicitInverseRef* const classMaps;
		ExplicitInverseRef* const columns;
		ExplicitInverseRef* const database;
		PrimAttribute* const excluded;
		ExplicitInverseRef* const foreignKeys;
		ExplicitInverseRef* const indexes;
		ExplicitInverseRef* const primaryKey;
		PrimAttribute* const schemaName;
		ImplicitInverseRef* const specialColumns;
		PrimAttribute* const tableType;
		PrimAttribute* const uuid;
	};
};
