#pragma once
#include "LabelMeta.h"

namespace JadeGit::Data
{
	class ProgressBarMeta : public RootClass<GUIClass>
	{
	public:
		static const ProgressBarMeta& get(const Object& object);
		
		ProgressBarMeta(RootSchema& parent, const LabelMeta& superclass);
	
		PrimAttribute* const partsDone;
		PrimAttribute* const partsInJob;
		PrimAttribute* const showTaskBarProgress;
	};
};
