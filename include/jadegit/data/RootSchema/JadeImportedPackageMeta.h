#pragma once
#include "JadePackageMeta.h"

namespace JadeGit::Data
{
	class JadeImportedPackageMeta : public RootClass<>
	{
	public:
		static const JadeImportedPackageMeta& get(const Object& object);
		
		JadeImportedPackageMeta(RootSchema& parent, const JadePackageMeta& superclass);
	
		ExplicitInverseRef* const classes;
		ExplicitInverseRef* const exportedPackage;
		ExplicitInverseRef* const interfaces;
	};
};
