#pragma once
#include "MethodMeta.h"

namespace JadeGit::Data
{
	class JadeAnnotationContractMeta : public RootClass<>
	{
	public:
		static const JadeAnnotationContractMeta& get(const Object& object);
		
		JadeAnnotationContractMeta(RootSchema& parent, const MethodMeta& superclass);
	
		ExplicitInverseRef* const schema;
	};
};
