#pragma once
#include "TypeMeta.h"

namespace JadeGit::Data
{
	class JadeInterfaceMeta : public RootClass<>
	{
	public:
		static const JadeInterfaceMeta& get(const Object& object);
		
		JadeInterfaceMeta(RootSchema& parent, const TypeMeta& superclass);
	
		ExplicitInverseRef* const exportedInterfaceRefs;
		PrimAttribute* const genericArgumentEncoding;
		ExplicitInverseRef* const genericArguments;
		ExplicitInverseRef* const implementorClasses;
		PrimAttribute* const role;
		ExplicitInverseRef* const subinterfaces;
		ExplicitInverseRef* const superinterfaces;
		ExplicitInverseRef* const templateInterface;
	};
};
