#pragma once
#include "LocaleFormatMeta.h"

namespace JadeGit::Data
{
	class TimeFormatMeta : public RootClass<>
	{
	public:
		static const TimeFormatMeta& get(const Object& object);
		
		TimeFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass);
	
		PrimAttribute* const amText;
		PrimAttribute* const ampmIsSuffix;
		PrimAttribute* const is12HourFormat;
		PrimAttribute* const pmText;
		PrimAttribute* const separator;
		PrimAttribute* const showLeadingZeros;
		PrimAttribute* const showSeconds;
	};
};
