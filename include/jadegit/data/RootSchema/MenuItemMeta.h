#pragma once
#include "MenuItemDataMeta.h"

namespace JadeGit::Data
{
	class MenuItemMeta : public RootClass<GUIClass>
	{
	public:
		static const MenuItemMeta& get(const Object& object);
		
		MenuItemMeta(RootSchema& parent, const MenuItemDataMeta& superclass);
	
		PrimAttribute* const backColor;
		PrimAttribute* const caption;
		PrimAttribute* const checked;
		PrimAttribute* const commandId;
		PrimAttribute* const description;
		PrimAttribute* const disableReason;
		PrimAttribute* const enabled;
		PrimAttribute* const foreColor;
		ExplicitInverseRef* const form;
		PrimAttribute* const hasSubMenu;
		PrimAttribute* const helpContextId;
		PrimAttribute* const helpKeyword;
		PrimAttribute* const helpList;
		PrimAttribute* const level;
		PrimAttribute* const name;
		PrimAttribute* const picture;
		PrimAttribute* const securityLevelEnabled;
		PrimAttribute* const securityLevelVisible;
		PrimAttribute* const shortCutFlags;
		PrimAttribute* const shortCutKey;
		PrimAttribute* const visible;
		PrimAttribute* const webFileName;
		PrimAttribute* const windowList;
	};
};
