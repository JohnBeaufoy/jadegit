#pragma once
#include "KeyMeta.h"

namespace JadeGit::Data
{
	class ExternalKeyMeta : public RootClass<>
	{
	public:
		static const ExternalKeyMeta& get(const Object& object);
		
		ExternalKeyMeta(RootSchema& parent, const KeyMeta& superclass);
	
		PrimAttribute* const length;
		PrimAttribute* const name;
		PrimAttribute* const precision;
		PrimAttribute* const scaleFactor;
		ExplicitInverseRef* const type;
	};
};
