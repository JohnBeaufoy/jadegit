#pragma once
#include "NumberFormatMeta.h"

namespace JadeGit::Data
{
	class CurrencyFormatMeta : public RootClass<>
	{
	public:
		static const CurrencyFormatMeta& get(const Object& object);
		
		CurrencyFormatMeta(RootSchema& parent, const NumberFormatMeta& superclass);
	
		PrimAttribute* const positiveFormat;
		PrimAttribute* const symbol;
	};
};
