#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalParameterMeta : public RootClass<>
	{
	public:
		static const ExternalParameterMeta& get(const Object& object);
		
		ExternalParameterMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		PrimAttribute* const bufferLength;
		PrimAttribute* const columnSize;
		PrimAttribute* const dataType;
		PrimAttribute* const decimalDigits;
		PrimAttribute* const length;
		PrimAttribute* const nullability;
		PrimAttribute* const ordinalPosition;
		PrimAttribute* const type;
	};
};
