#pragma once
#include "ImplicitInverseRefMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicImplicitInverseRefMeta : public RootClass<>
	{
	public:
		static const JadeDynamicImplicitInverseRefMeta& get(const Object& object);
		
		JadeDynamicImplicitInverseRefMeta(RootSchema& parent, const ImplicitInverseRefMeta& superclass);
	
		ExplicitInverseRef* const dynamicPropertyCluster;
	};
};
