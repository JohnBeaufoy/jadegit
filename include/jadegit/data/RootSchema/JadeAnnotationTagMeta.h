#pragma once
#include "RoutineMeta.h"

namespace JadeGit::Data
{
	class JadeAnnotationTagMeta : public RootClass<>
	{
	public:
		static const JadeAnnotationTagMeta& get(const Object& object);
		
		JadeAnnotationTagMeta(RootSchema& parent, const RoutineMeta& superclass);
	
		ImplicitInverseRef* const annotationClass;
		ImplicitInverseRef* const annotationValue;
		ExplicitInverseRef* const target;
	};
};
