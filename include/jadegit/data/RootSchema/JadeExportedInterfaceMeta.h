#pragma once
#include "JadeExportedTypeMeta.h"

namespace JadeGit::Data
{
	class JadeExportedInterfaceMeta : public RootClass<>
	{
	public:
		static const JadeExportedInterfaceMeta& get(const Object& object);
		
		JadeExportedInterfaceMeta(RootSchema& parent, const JadeExportedTypeMeta& superclass);
	
		ExplicitInverseRef* const importedInterfaceRefs;
		ExplicitInverseRef* const originalInterface;
	};
};
