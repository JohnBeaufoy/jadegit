#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class KeyMeta : public RootClass<>
	{
	public:
		static const KeyMeta& get(const Object& object);
		
		KeyMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const caseInsensitive;
		PrimAttribute* const descending;
		PrimAttribute* const sortOrder;
	};
};
