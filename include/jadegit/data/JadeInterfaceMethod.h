#pragma once
#include "Method.h"

namespace JadeGit::Data
{
	class JadeInterface;

	class JadeInterfaceMethod : public Method
	{
	public:
		using Parents = ObjectParents<JadeInterface>;

		JadeInterfaceMethod(JadeInterface* parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor& v) final;

		const JadeInterfaceMethod& getOriginal() const final;
		bool isCopy() const final;
	};
}