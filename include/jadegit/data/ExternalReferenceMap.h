#pragma once
#include "ExternalPropertyMap.h"
#include "RootSchema/ExternalReferenceMapMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMap;
	class ExternalColumn;

	class ExternalReferenceMap : public ExternalPropertyMap
	{
	public:
		ExternalReferenceMap(ExternalClassMap& parent, const Class* dataClass, const char* name);

		const unsigned short id;

		ObjectValue<ExternalClassMap* const, &ExternalReferenceMapMeta::classMap> classMap;
		ObjectValue<Array<ExternalColumn*>, &ExternalReferenceMapMeta::leftColumns> leftColumns;
		Value<ExternalReferenceMap*> otherRefMap;
		ObjectValue<Array<ExternalColumn*>, &ExternalReferenceMapMeta::rightColumns> rightColumns;
	};
}