#pragma once
#include "Feature.h"
#include "RootSchema/JadeExposedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExposedClass;

	class JadeExposedFeature : public NamedObject
	{
	public:
		using Parents = ObjectParents<JadeExposedClass>;

		JadeExposedFeature(JadeExposedClass* parent, const Class* dataClass, const char* name = nullptr);

		ObjectValue<JadeExposedClass* const, &JadeExposedFeatureMeta::exposedClass> exposedClass;
		Value<std::string> exposedName;
		Value<std::string> exposedType;
		ObjectValue<Feature*, &JadeExposedFeatureMeta::relatedFeature> relatedFeature;

		const Feature& getOriginal() const;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<JadeExposedClass* const, &JadeExposedFeatureMeta::exposedClass>;	
	extern template ObjectValue<Feature*, &JadeExposedFeatureMeta::relatedFeature>;
}