#pragma once
#include "NamedObject.h"
#include "RootSchema/ExternalSchemaMapMeta.h"

namespace JadeGit::Data
{
	class ExternalSchemaMap : public NamedObject
	{
	public:
		ObjectValue<SchemaEntity*, &ExternalSchemaMapMeta::schemaEntity> schemaEntity;

	protected:
		using NamedObject::NamedObject;
	};

	extern template ObjectValue<SchemaEntity*, &ExternalSchemaMapMeta::schemaEntity>;
}