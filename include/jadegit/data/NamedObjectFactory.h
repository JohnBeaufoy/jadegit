#pragma once
#include <jadegit/data/ObjectFactory.h>
#include <jadegit/data/QualifiedName.h>

namespace JadeGit::Data
{
	class NamedObjectFactory : public ObjectFactory
	{
	public:
		static NamedObjectFactory& Get();

		class Registration : public ObjectFactory::Registration
		{
		public:
			virtual Object* Create(Component* parent, const Class* dataClass, const char* name) const = 0;
			virtual Object* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const = 0;
		};

		template<class TDerived>
		void Register(const char* key, const Registration* registrar)
		{
			ObjectFactory::Register<TDerived>(key, registrar);
		}

		template<class TDerived = Object>
		TDerived* Create(const std::string& key, Component* origin, const char* name) const
		{
			Class* dataClass = nullptr;
			auto result = Lookup(key, origin, dataClass)->Create(origin, dataClass, name);
			assert(!result || dynamic_cast<TDerived*>(result));
			return static_cast<TDerived*>(result);
		}

		template<class TDerived = Object>
		TDerived* Create(const type_info& type, Component* origin, const char* name) const
		{
			auto result = Lookup(type)->Create(origin, nullptr, name);
			assert(!result || dynamic_cast<TDerived*>(result));
			return static_cast<TDerived*>(result);
		}

		template<class TDerived = Object>
		TDerived* Resolve(const std::string& key, const Component* origin, const QualifiedName& name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return Resolve<TDerived>(key, origin, &name, expected, shallow, inherit);
		}

		template<class TDerived = Object>
		TDerived* Resolve(const std::string& key, const Component* origin, const QualifiedName* name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			Class* dataClass = nullptr;
			auto result = Lookup(key, origin, dataClass)->Resolve(origin, name, expected, shallow, inherit);
			assert(!result || dynamic_cast<TDerived*>(result));
			return static_cast<TDerived*>(result);
		}

		template<class TDerived>
		TDerived* Resolve(const Component* origin, const QualifiedName& name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return Resolve<TDerived>(origin, &name, expected, shallow, inherit);
		}

		template<class TDerived>
		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return static_cast<TDerived*>(Lookup(typeid(TDerived))->Resolve(origin, name, expected, shallow, inherit));
		}

		template<>
		Assembly* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const
		{
			assert(!name);
			return &origin->getAssembly();
		}

		const Registration* Lookup(const std::string& key, const Component* origin, Class*& dataClass, bool required = true) const;
		const Registration* Lookup(const type_info& type) const;

	protected:
		NamedObjectFactory() {}
	};
}