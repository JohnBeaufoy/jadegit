#pragma once
#include <memory>
#include <string>

namespace JadeGit
{
	class QualifiedName
	{
	public:
		std::unique_ptr<QualifiedName> parent;
		std::string name;

		QualifiedName(std::string path);
		QualifiedName(const char* path);

		operator std::string() const;
		bool operator!() const;

		inline const QualifiedName& first() const
		{
			const QualifiedName* parent = this;
			while (parent->parent)
				parent = parent->parent.get();

			return *parent;
		}

		inline const short parts() const
		{
			short result = 0;
			const QualifiedName* part = this;
			while (part)
			{
				result++;
				part = part->parent.get();
			}
			return result;
		}
	};
}