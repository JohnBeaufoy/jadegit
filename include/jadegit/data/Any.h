#pragma once
#include <memory>

namespace JadeGit::Data
{
	class Any;
	
	// Base abstraction for any value
	class AnyValue
	{
	public:
		virtual ~AnyValue() {}
		virtual AnyValue* Clone() const = 0;

		virtual bool empty() const = 0;
		virtual void reset() = 0;
		virtual void Set(Any const & v) = 0;
		virtual void Set(const std::string& v) = 0;

		virtual std::string ToString() const = 0;
		virtual std::string ToBasicString() const { return ToString(); }

		AnyValue & operator=(AnyValue const &) { return *this; }
		virtual bool operator==(const AnyValue* rhs) const = 0;

	protected:
		AnyValue() {}
	};

	// Type template which defines what value can be cast to
	template <typename T>
	class TypeValue
	{
	public:
		virtual operator T() const = 0;
	};

	class invalid_cast : public std::logic_error
	{
	public:
		invalid_cast(const std::type_info& from, const std::type_info& to);
	};

	template <typename T, typename Enable = void>
	class Value;

	// Define template for casting any value to another type
	template <typename T>
	struct AnyCast
	{
		static T Get(const AnyValue* v)
		{
			// Attempt direct cast
			if (const TypeValue<T>* value = dynamic_cast<const TypeValue<T>*>(v))
				return *value;

			// Attempt string cast
			if (const TypeValue<std::string>* string = dynamic_cast<const TypeValue<std::string>*>(v))
			{
				Value<T> value;
				value.Set(*string);
				return value;
			}
			
			throw invalid_cast(typeid(*v), typeid(T));
		}
	};

	// Basic value template which defines underlying type
	template <typename T>
	class BaseValue : public AnyValue, public TypeValue<T>
	{
	public:
		bool operator==(const AnyValue* rhs) const override
		{
			return static_cast<T>(*this) == AnyCast<T>::Get(rhs);
		}

	protected:
		BaseValue() : AnyValue() {}
		BaseValue(const BaseValue& rhs) : AnyValue(rhs) {}
		BaseValue& operator=(const BaseValue& rhs)
		{
			AnyValue::operator =(rhs);
			return *this;
		}
	};

	// Handle to any value
	class Any
	{
	public:
		Any(const Any& v);
		Any(const AnyValue& v);
		explicit Any(AnyValue* v = nullptr);

		bool operator==(const Any &rhs) const;
		bool operator==(const AnyValue& rhs) const;
		bool operator==(const AnyValue* rhs) const;

		Any& operator=(const Any &);
		
		inline bool empty() const
		{
			return !v || v->empty();
		}

		template <typename T>
		T Get() const
		{
			if (!v)
				return T();

			return AnyCast<T>::Get(v.get());
		}

		std::string ToString() const;
		std::string ToBasicString() const;

	private:
		std::unique_ptr<AnyValue> v;
	};
}