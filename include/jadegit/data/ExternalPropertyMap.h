#pragma once
#include "ExternalSchemaMap.h"

namespace JadeGit::Data
{
	class ExternalPropertyMap : public ExternalSchemaMap
	{
	public:
		using Parents = ObjectParents<ExternalClassMap>;

	protected:
		using ExternalSchemaMap::ExternalSchemaMap;

		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}