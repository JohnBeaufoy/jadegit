#pragma once
#include "NamedObject.h"
#include "MemberKeyDictionary.h"

namespace JadeGit::Data
{
	template<class TChild, auto prop> requires std::is_base_of_v<NamedObject, TChild>
	using NamedObjectDict = ObjectValue<MemberKeyDictionary<TChild, &TChild::name>, prop>;
}