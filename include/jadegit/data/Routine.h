#pragma once
#include "Script.h"
#include "RootSchema/RoutineMeta.h"

namespace JadeGit::Data
{
	class Library;
	class Parameter;
	class ReturnType;
	
	DECLARE_OBJECT_CAST(Library)

	class Routine : public Script
	{
	public:
		enum class ExecutionLocation : char
		{
			Any = 'A',
			Server = 'S',
			Client = 'C',
			ApplicationServer = 'a',
			PresentationClient = 'p'
		};

		Value<std::string> entrypoint;
		Value<ExecutionLocation> executionLocation = ExecutionLocation::Any;
		ObjectValue<Library*, &RoutineMeta::library> library;
		Value<int> options = 0;
		std::vector<Parameter*> parameters;
		ReturnType* returnType = nullptr;

	protected:
		Routine(Type* parent, const Class* dataClass, const char* name);
		Routine(Object* parent, const Class* dataClass, const char* name);
	};

	extern template Value<Routine::ExecutionLocation>;
	extern template ObjectValue<Library*, &RoutineMeta::library>;
	extern template std::map<Routine::ExecutionLocation, const char*> EnumStrings<Routine::ExecutionLocation>::data;
}