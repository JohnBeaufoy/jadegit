#pragma once
#include "Object.h"
#include "MetaSchema/MetaObject.h"
#include <jadegit/Version.h>

namespace JadeGit::Data
{
	class PrimType;
	class Schema;

	// MetaSchema is a Schema that is explicitly instantiated
	class MetaSchema : public Meta<Schema>
	{
	public:
		bool initialized = false;
		const Version version;

	protected:
		MetaSchema(Assembly& parent, const char* name, Version version, const RootSchema& rootSchema);
		MetaSchema(Assembly& parent, const char* name, Schema* superschema, Version version);

		template<class TMeta, typename... Args>
		TMeta* newMeta(Args&... args)
		{
			return new TMeta(args...);
		}

		template<class TMeta, typename... Args>
		TMeta* newMeta(const Version& minVersion, Args&... args)
		{
			if (!version.empty() && minVersion > version)
				return nullptr;

			return new TMeta(args...);
		}

	private:
		MetaSchema(Schema* schema, Version version);
	};

	template<class TType = Class> class MetaClass;
}
