#pragma once
#include <jadegit/arch.h>
#include <jadegit/Progress.h>
#include <memory>

namespace JadeGit::Build
{
	class Builder;
	class Source;

	class Director
	{
	public:
		// Setup director to build from source
		Director(const Source& source, Builder& builder, IProgress* progress = nullptr);
		~Director();

		// Build deployment
		bool build();

	private:
		class Impl;
		std::unique_ptr<Impl> impl;
	};
}