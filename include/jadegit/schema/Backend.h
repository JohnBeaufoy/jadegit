#pragma once

namespace JadeGit::Schema::Backend
{
	void Startup();
	void Shutdown();
}