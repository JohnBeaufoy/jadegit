#pragma once
#include "FileSystem.h"

namespace JadeGit
{
	class NativeFile;
	class NativeFileIterator;

	class NativeFileSystem : public FileSystem
	{
	public:
		NativeFileSystem(const std::filesystem::path& base, bool writable = false, const FileSignature* signature = nullptr);
		~NativeFileSystem();

		bool isReadOnly() const override;
		File open(const std::filesystem::path& path) const override;

	protected:
		friend NativeFile;
		friend NativeFileIterator;
		const std::filesystem::path base;
		const bool writable = false;
		const FileSignature* signature = nullptr;
	};
}