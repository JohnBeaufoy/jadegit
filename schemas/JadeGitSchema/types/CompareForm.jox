<GUIClass name="CompareForm" id="b1de774f-948b-4fc2-8830-ad4cb97e49d7">
    <superclass name="DiffForm"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="base" id="cd002b9b-74a9-45f5-8dd9-9cf5f18bb6ab">
        <embedded>true</embedded>
        <type name="CommitControl"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="compare" id="d0a74a89-0304-4723-9efc-0ca8be70e179">
        <embedded>true</embedded>
        <type name="CommitControl"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="header" id="79dbb330-cf5b-49f1-bb13-23c56040e48b">
        <embedded>true</embedded>
        <type name="Frame"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="btnMenu" id="6829936b-06f0-4cb1-a944-c0fee6e81f8b">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="switch" id="3d26cec9-0b09-45e1-a108-bf0c8e18547f">
        <embedded>true</embedded>
        <type name="Hyperlink"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="menu" id="e946dc02-687b-406d-b4f2-71c6523ebe15">
        <embedded>true</embedded>
        <type name="MenuItem"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="mnuExtractPatch" id="7d150cb9-0c29-4853-99c3-dbb84263a80a">
        <embedded>true</embedded>
        <type name="MenuItem"/>
    </ImplicitInverseRef>
    <JadeMethod name="populate" id="96cff86b-7c44-4c8c-a3da-c538be0e5777">
        <updating>true</updating>
        <source>populate(object: Object): Boolean updating, protected;

vars
	keyword : String;

begin
	assert("Unexpected object", object.isKindOf(ReferenceArray) or object.isKindOf(CommitArray));
	assert("Unexpected array size", object.Array.size() = 2);
	
	keyword := object.isKindOf(ReferenceArray).iif("Branches", "Commits").String;
	header.caption := "Comparing " &amp; keyword;
	switch.caption := "Switch " &amp; keyword;
	caption := header.caption &amp; " - " &amp; object.Array[1].getName() &amp; " with " &amp; object.Array[2].getName();
	
	// Repopulate base branch/commit
	base.repopulate(object.ObjectArray.first);
	
	// Repopulate compare branch/commit
	compare.repopulate(object.ObjectArray.last);
	
	// Adjust details height to fit base/compare details
	frmDetails.height := base.height + compare.height + 8;
	compare.setParentBottomOffset(4);
	
	// Display diff between commits resolved by base/compare controls
	return inheritMethod(Diff@tree_to_tree(base.commit.getRepository(), base.commit.tree(), compare.commit.tree()));
end;
</source>
        <access>protected</access>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="delete" id="b2f0a4c2-26b0-4d31-9e20-9e8151cc6a80">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>delete() updating, clientExecution;

begin
	// Cleanup array supplied when opening form
	if userObject.isKindOf(Array) then
		delete userObject;
	endif;
end;
</source>
    </JadeMethod>
    <JadeMethod name="switch_click" id="95401210-0e1b-41fc-a567-069fe85e1e07">
        <controlMethod name="Hyperlink::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>switch_click(label: Label input) updating;

begin
	userObject.Array.reverse();
	repopulate(userObject);
end;
</source>
        <Parameter name="label">
            <usage>input</usage>
            <type name="Label"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnMenu_click" id="762467e9-d837-4dad-94d7-e134a93eecf5">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnMenu_click(btn: Button input) updating;

vars
    x1 : Real;
    y1 : Real;
	
begin
	menu.repopulate(btn.userObject, true);
	
    y1 := btn.height;
    btn.windowToScreen(x1, y1);
    self.screenToWindow(x1, y1);
    popupMenu(menu, x1.Integer, y1.Integer);
end;
</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="mnuExtractPatch_click" id="b1690289-d7db-4406-983e-4c1a15abe6b3">
        <controlMethod name="MenuItem::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>mnuExtractPatch_click(menuItem: MenuItem input) updating;

vars
	commits : CommitArray;

begin
	create commits transient;
	commits.add(base.commit);
	commits.add(compare.commit);

	PatchBuilderDialog@open(commits).showModal();
	
epilog
	delete commits;
end;</source>
        <Parameter name="menuItem">
            <usage>input</usage>
            <type name="MenuItem"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="mnuExtractPatch_enable" id="5dc2a9c5-6536-468e-9f84-0e567e647548">
        <controlMethod name="MenuItem::enable"/>
        <methodInvocation>event</methodInvocation>
        <source>mnuExtractPatch_enable(menuItem: MenuItem input) protected;

begin
	// Extract cannot be performed when there's no changes to extract
	menuItem.enabled := not frmChanges.userObject.Diff.deltas.isEmpty();
end;
</source>
        <access>protected</access>
        <Parameter name="menuItem">
            <usage>input</usage>
            <type name="MenuItem"/>
        </Parameter>
    </JadeMethod>
</GUIClass>
