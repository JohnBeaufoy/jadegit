<Class name="DiffFormat" id="dbf38cf9-0ec7-472c-be77-24d6fe8f5323">
    <superclass name="Presentation"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Holds line by line formatting info for a diff </text>
    <Constant name="LineChanged" id="f9c656e1-b909-42da-9e5d-e4d37095f6a3">
        <source>'C'</source>
        <type name="Character"/>
    </Constant>
    <Constant name="LineNormal" id="a097b3be-2feb-4203-a9e2-218b2f422eda">
        <source>'N'</source>
        <type name="Character"/>
    </Constant>
    <Constant name="LineOld" id="8f1d0cfc-f6a5-4bdc-bed8-3cdd1ac629a4">
        <source>'O'</source>
        <type name="Character"/>
    </Constant>
    <Constant name="LinePad" id="29f92b74-886c-4f70-9f2d-8f2f1c964e61">
        <source>'P'</source>
        <type name="Character"/>
    </Constant>
    <CompAttribute name="lineFormat" id="4f2bc1ec-2a99-49b3-bfc8-63e142ab2a13">
        <type name="CharacterArray"/>
        <access>readonly</access>
    </CompAttribute>
    <CompAttribute name="lineNumber" id="19ac81b8-b561-40ba-a755-2cfe8eb30a8c">
        <type name="IntegerArray"/>
        <access>readonly</access>
    </CompAttribute>
    <CompAttribute name="lineText" id="a95cfddd-0561-49ea-9976-eba3c773df2e">
        <type name="StringArray"/>
        <access>readonly</access>
    </CompAttribute>
    <PrimAttribute name="text" id="9281509a-b9d0-4d05-9bd6-45ec9dcc2ad6">
        <virtual>true</virtual>
        <type name="String"/>
        <access>readonly</access>
    </PrimAttribute>
    <PrimAttribute name="replaceEscapes" id="01f7db21-0bc0-4985-a131-9f27b54530e7">
        <embedded>true</embedded>
        <type name="Boolean"/>
    </PrimAttribute>
    <JadeMethod name="changeCounts" id="192bab47-463e-46d5-a3c0-6bca51b32b51">
        <source>changeCounts( changes	 : Integer output;		// # of blocks that are different
              lineChanges : Integer output;		// # of lines that are different
			  linePads : Integer output );		// # of lines padded

// provide counts on the diff per the param vars
			  
vars
	inx		: Integer;
	fmt		: Character;
	zone	: Boolean;
	
begin
	foreach inx in 1 to lineFormat.size do
		fmt		:= lineFormat[inx];
		
		if fmt &lt;&gt; LineNormal then
			if fmt = LineChanged then
				lineChanges	+= 1;
			else
				linePads += 1;
			endif;
		
			if not zone then
				changes	+= 1;
				zone	:= true;
			endif;		
		else	
			zone	:= false;
		endif;
	endforeach;
end;
</source>
        <Parameter name="changes">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
        <Parameter name="lineChanges">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
        <Parameter name="linePads">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="changeLine" id="c4d69908-8f8b-4cf5-85ef-a36edf988b6a">
        <source>changeLine(changeNumber : Integer): Integer;
// return the first line number representing the block defined by the param

vars
	inx		: Integer;
	fmt		: Character;
	zone	: Boolean;
	count	: Integer;
	lastInx	: Integer;
	
begin
	foreach inx in 1 to lineFormat.size do
		fmt		:= lineFormat[inx];
		
		if fmt &lt;&gt; LineNormal then
			if not zone then
				count	+= 1;
				zone	:= true;
				lastInx	:= inx;
				if count = changeNumber then
					return inx;
				endif;
			endif;		
		else	
			zone	:= false;
		endif;
	endforeach;
	
	if changeNumber = DiffControl.NavigateTotLast then
		return lastInx;
	endif;
end;</source>
        <Parameter name="changeNumber">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="changeSection" id="8e36dca9-3383-4c37-91f8-44d4de867202">
        <source>changeSection( lineStart, lineEnd : Integer;		// current visible area
			   direction		  : Integer			// whether we want to go up or down
								 ): Integer;		// line position to change to
								 
// returns a line position by navigating to the next changed block 

vars
	inx		: Integer;
	fmt		: Character;
	zone	: Boolean;
	
begin
	if direction = DiffControl.NavigateNext then
		inx		:= lineEnd;
		if inx &lt; lineFormat.size then
			while inx &lt;= lineFormat.size do
				fmt		:= lineFormat[inx];
				if fmt &lt;&gt; LineNormal then
					return inx;
				endif;
				inx		+= 1;
			endwhile;		
		else
			// we were already at the bottom
		endif;
		
	elseif direction = DiffControl.NavigateBack then
		if lineStart = 1 then
			return 0;		// already at the top
		endif;
		inx		:= lineStart;
		while inx &gt; 0 do
			fmt		:= lineFormat[inx];
			if fmt &lt;&gt; LineNormal then
				zone	:= true;				
			elseif zone then
				return inx;
			endif;
			inx		-= 1;
		endwhile;
		if zone then
			return 1;
		endif;		
	endif;
	return 0;
end;
</source>
        <Parameter name="lineStart">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="lineEnd">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="direction">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getSections" id="006c9556-1a77-4d78-aa34-c75f54a3c3ae">
        <source>getSections( linesBefore, linesAfter : Integer ): PointArray;
// returns a list of lines that define the changed blocks

vars
	sections	: PointArray;
	section		: Point;
	index		: Integer;
	fmt			: Character;
	subStart	: Integer;
	subAfter	: Integer;
	
begin
	create sections transient;
	
	while index &lt; lineFormat.size do
		index	+= 1;
		fmt		:= lineFormat[index];
		
		if subStart = 0 then
			if fmt &lt;&gt; LineNormal then
				subStart	:= (index-linesBefore).max(1);
			endif;		
		
		elseif fmt = LineNormal then
			subAfter	+= 1;
			if subAfter = linesAfter then
				// record the entry
				section.set( subStart, index - subStart + 1 );			
				sections.add(section);
				
				subStart	:= 0;
				subAfter	:= 0;
			endif;
		
		else
			subAfter	:= 0;
		endif;
	endwhile;	
	
	if subStart &gt; 0 then
		section.set( subStart, index - subStart + 1 );			
		sections.add(section);	
	endif;
	return sections;
end;
</source>
        <Parameter name="linesBefore">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="linesAfter">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="PointArray"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="lineAppend" id="590282e8-96fa-4435-b18a-14a0006b18a0">
        <source>lineAppend( lineNo  : Integer;
			lineTxt	: String );
// adds a new element to our arrays, doing formatting on the text where necessary		

constants
	MaxLineChars = 2000;
			
vars
	text	: String;

begin
	if lineTxt.length &gt; MaxLineChars then
		text	:= lineTxt[1:MaxLineChars] &amp; "..";
	else
		text	:= lineTxt;
	endif;
	
	if replaceEscapes 
	and text.pos( "&amp;", 1 ) &gt; 0 then
		text	:= text.replace__( "&amp;lt;", "&lt;", false );
		text	:= text.replace__( "&amp;gt;", "&gt;", false );
		text	:= text.replace__( "&amp;amp;", "&amp;", false );
	endif;
	
	lineText.add( text );
	lineFormat.add( LineNormal );
	lineNumber.add( lineNo );
end;
</source>
        <Parameter name="lineNo">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="lineTxt">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="linePad" id="ae96697f-72f5-4a2b-9bd7-d463e32618af">
        <source>linePad( lineNum : Integer );
// pads an empty line (filler) into the formatting arrays

vars
	inx		: Integer;
	
begin
	inx		:= lineNum;
	if inx = 0 or inx &gt; lineFormat.size then
		lineFormat.add( LinePad );
		lineNumber.add( 0 );
		lineText.add( "" );
	else
		lineFormat.insert( inx, LinePad );
		lineNumber.insert( inx, 0 );
		lineText.insert( inx, "" );
	endif;
end;
</source>
        <Parameter name="lineNum">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="load" id="69b244b7-14f3-4e98-86b7-03b144d61b09">
        <updating>true</updating>
        <source>load( fullText : String ) updating;
// populates the formatting object with the text &amp; default formatting

vars
	inx		: Integer;
	length	: Integer;
	line	: String;
	lineNo	: Integer;
	
begin
	lineFormat.clear();
	lineNumber.clear();
	lineText.clear();
	
	inx	:= 1;
	length := fullText.length;
	
	while inx &lt;= length do		
		lineNo	+= 1;
		line	:= fullText.scanUntil( CrLf, inx);
		lineAppend( lineNo, line );
		
		if inx = 0 then
			break;
		else
			inx		+= 2;	
		endif;
	endwhile;
end;
</source>
        <Parameter name="fullText">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="text" id="2a4d644c-e586-410f-a595-8737f80b4d02">
        <methodInvocation>mapping</methodInvocation>
        <source>text(set: Boolean; _value: String io) mapping, protected;

vars
	text	: String;
	inx		: Integer;
	
begin
	if not set then
		foreach inx in 1 to lineNumber.size do
			text	&amp;= lineText[inx] &amp; CrLf;
		endforeach;
		_value	:= text;
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="set">
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="_value">
            <usage>io</usage>
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="create" id="257551bb-0636-4936-a722-bf4cb4f7f30d">
        <updating>true</updating>
        <source>create( replaceEsc : Boolean ) updating;

begin
	replaceEscapes	:= replaceEsc;
end;</source>
        <Parameter name="replaceEsc">
            <type name="Boolean"/>
        </Parameter>
    </JadeMethod>
</Class>
