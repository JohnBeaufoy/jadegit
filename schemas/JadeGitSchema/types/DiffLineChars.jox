<Class name="DiffLineChars" id="6f02200a-c7c9-43f1-aa8f-841c9264298a">
    <superclass name="Presentation"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Comparison of two sentences/lines.

With advanced diff views when a line is marked as changed we ideally show the chars
 that are different, which this class provides.</text>
    <Constant name="CharDifferent" id="7c361335-ff33-4c5f-88f8-c62605d8ab8e">
        <source>"@"</source>
        <type name="Character"/>
    </Constant>
    <Constant name="CharNotChecked" id="7e1ef6c8-0d3e-48b7-86e3-f5c5330864c0">
        <source>"?"</source>
        <type name="Character"/>
    </Constant>
    <Constant name="CharSame" id="924e76b5-c6dc-4c53-81a2-1ecd02a355df">
        <source>'='</source>
        <type name="Character"/>
    </Constant>
    <CompAttribute name="diffNew" id="12294220-6b7f-4482-a1b5-979b765e6c41">
        <type name="CharacterArray"/>
        <access>protected</access>
    </CompAttribute>
    <PrimAttribute name="diffNewString" id="7caee9fd-486e-49a0-b02b-8ca4d2797f37">
        <virtual>true</virtual>
        <type name="String"/>
        <access>readonly</access>
    </PrimAttribute>
    <CompAttribute name="diffOld" id="68ac491d-2953-4c0e-b21e-bc043f050166">
        <type name="CharacterArray"/>
        <access>protected</access>
    </CompAttribute>
    <PrimAttribute name="diffOldString" id="f92cd0f6-a542-42e1-9afc-2ead6be2bb2c">
        <virtual>true</virtual>
        <type name="String"/>
        <access>readonly</access>
    </PrimAttribute>
    <CompAttribute name="diffSections" id="e8c7fb56-470f-42e3-88d7-1bee0bc34e6c">
        <type name="PointArray"/>
        <access>protected</access>
    </CompAttribute>
    <JadeMethod name="compare" id="cccd8874-e290-41e1-8dcd-8182e2a5aa4a">
        <updating>true</updating>
        <source>compare( oldString, newString : String ) updating;
// compares the two params to list differences

vars
	oldWords, newWords	: StringArray;
	used				: Integer;
	
begin
	diffOld.clear();
	diffNew.clear();
	diffSections.clear();

	// put the line into words
	oldWords	:= parseWords(oldString);
	newWords	:= parseWords(newString);
	padSides( oldWords, newWords );

	// compare the old with the new
	if oldString.length &gt; 0 then
		diffOld[oldString.length]	:= null;
		diffUpdate(diffOld, 1, oldString.length, CharNotChecked );
		used	:= compareBackwards( oldWords, oldString.length, newWords, diffOld );
		compareForwards( oldWords, newWords, used, diffOld );
	endif;

	// compare the new with the old
	if newString.length &gt; 0 then
		diffNew[newString.length]	:= null;
		diffUpdate(diffNew, 1, newString.length, CharNotChecked );
		used	:= compareBackwards( newWords, newString.length, oldWords, diffNew );
		compareForwards( newWords, oldWords, used, diffNew );
	endif;
	
epilog
	delete oldWords;
	delete newWords;
end;
</source>
        <Parameter name="oldString">
            <type name="String"/>
        </Parameter>
        <Parameter name="newString">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="compareBackwards" id="7162172c-7c28-49d1-91f5-b8fc8c3306bc">
        <source>compareBackwards( compare 	: StringArray;
				  compareL	: Integer;
				  with		: StringArray;
				  diff		: CharacterArray input )
				            : Integer protected;

// works backwards through the words until it finds a difference
// it wont mark the difference, that will come out in the forward check which follows
// return how many of the other words we've used, so we dont double use them
				  
vars
	inx		: Integer;
	withInx	: Integer;
	word	: String;
	withWord: String;
	posEnd	: Integer;
	posStart: Integer;
	withUsed: Integer;
	
begin
	inx		:= compare.size;
	withInx	:= with.size;
	posEnd	:= compareL;
	while inx &gt; 0 do
		word	:= compare[inx];
		if withInx &gt; 0 then
			withWord	:= with[withInx];
		else
			withWord	:= "";
		endif;
		
		posStart := posEnd - word.length + 1;
		if word = withWord then
			diffUpdate( diff, posStart, posEnd, CharSame );
			withUsed	+= 1;
		else	
			break;
		endif;		
		
		posEnd	:= posStart - 1;
		inx		:= inx - 1;
		withInx	:= withInx - 1;
	endwhile;	
	return withUsed;
end;
</source>
        <access>protected</access>
        <Parameter name="compare">
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="compareL">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="with">
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="diff">
            <usage>input</usage>
            <type name="CharacterArray"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="compareForwards" id="bdedfdf9-8209-453a-8b26-d18040edf862">
        <source>compareForwards(compare 	: StringArray;
				with		: StringArray;
				withUsed	: Integer;
				diff		: CharacterArray input ) protected;

// looks forwards through all the words, marking differences and stopping at the end 
// or when it reaches what the reverse compare found
				
vars
	inx		: Integer;
	withInx	: Integer;
	word	: String;
	withWord: String;
	posEnd	: Integer;
	posStart: Integer;
	
begin
	posStart	:= 1;
	inx			:= 1;
	withInx		:= 1;
	while inx &lt;= compare.size do
		word	:= compare[inx];
		posEnd	:= posStart+word.length-1;
		if withInx &lt;= with.size - withUsed then
			withWord	:= with[withInx];
		else
			withWord	:= "";
		endif;
		
		if posStart &lt;= diff.size 
		and diff[posStart] &lt;&gt; CharNotChecked then
			// we've already checked so we're done
			break;
		endif;

		// if words are the same we can mark as such
		// if they're different drop into a char by char compare
		if word = withWord then
			diffUpdate( diff, posStart, posEnd, CharSame );
			
		else
			compareWord( word, withWord, diff, posStart );
		endif;		
		
		posStart:= posEnd + 1;
		inx		:= inx + 1;
		withInx	:= withInx + 1;
	endwhile;	
end;
</source>
        <access>protected</access>
        <Parameter name="compare">
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="with">
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="withUsed">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="diff">
            <usage>input</usage>
            <type name="CharacterArray"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="compareWord" id="ff60ddb0-be01-48da-a3e8-85c3836db053">
        <source>compareWord(word		: String;
			wordTo		: String;
			diff		: CharacterArray input;
			diffStart 	: Integer) protected;
					   
// char by char compare of a word
// returns true if we've ne

vars
	inx		: Integer;
	diffInx	: Integer;
	char	: Character;
	charTo	: Character;
	inxTo	: Integer;
	diffFound : Integer;
		
begin
	inx		:= word.length;
	inxTo	:= wordTo.length;
	
	if inxTo = 0 then
		diffUpdate( diff, diffStart, diffStart + word.length - 1, CharDifferent );
		return;
	endif;
	
	// work from the end backwards first
	while inx &gt; 0 do
		char	:= word[inx];
		if inxTo &gt; 0 then
			charTo	:= wordTo[inxTo];
			if char = charTo then
				diffInx	:= diffStart + inx - 1;
				diff[diffInx]	:= CharSame;
				diffFound		+= 1;
			else
				break;
			endif;
		else
			break;
		endif;	
		inx	  -= 1;
		inxTo -= 1;
	endwhile;
		
	// now from the start
	inxTo	:= 0;
	foreach inx in 1 to word.length do
		diffInx	:= diffStart + inx - 1;
		if diff[diffInx] &lt;&gt; CharNotChecked then
			break;
		endif;
		inxTo	+= 1;
				
		char	:= word[inx];
		if inxTo &lt;= wordTo.length - diffFound then
			charTo	:= wordTo[inxTo];
			if char = charTo then
				diff[diffInx]	:= CharSame;
			else
				diff[diffInx]	:= CharDifferent;
			endif;
		else
			diff[diffInx]	:= CharDifferent;
		endif;		
	endforeach;	
	
	// the above is an accurate assessment of the differences
	// but just because two chars are equal in the same position
	// doesnt mean its part of the same word or meaning
	if not compareWordScore( diff, diffStart, diffStart + word.length - 1 ) then
		diffUpdate( diff, diffStart, diffStart + word.length - 1, CharDifferent );
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="word">
            <type name="String"/>
        </Parameter>
        <Parameter name="wordTo">
            <type name="String"/>
        </Parameter>
        <Parameter name="diff">
            <usage>input</usage>
            <type name="CharacterArray"/>
        </Parameter>
        <Parameter name="diffStart">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="compareWordScore" id="50d837ce-a22f-42e5-956a-6b23b2151473">
        <source>compareWordScore( diffList	: CharacterArray;
				  diffStart	: Integer;
				  diffStop 	: Integer )
							: Boolean protected;

vars
	diffLast	: Character;
	diff		: Character;
	index		: Integer;
	count		: Integer;
	total		: Integer;
	size		: Integer;
	
begin
	index	:= diffStart;
	while index &lt;= diffStop do
		diff		:= diffList[index]; 
		if diff = CharSame then
			count	+= 1;
			total	+= 1;
		else
			if count &gt; 4 then
				return true;
			endif;
			count	:= 0;
		endif;
		diffLast	:= diff;
		index		+= 1;
	endwhile;
	
	size	:= (diffStop-diffStart+1);
	if size &gt; 0 and total/size &gt;= 0.7 then
		return true;
	endif;
	return false;
end;
</source>
        <access>protected</access>
        <Parameter name="diffList">
            <type name="CharacterArray"/>
        </Parameter>
        <Parameter name="diffStart">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="diffStop">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="diffNewString" id="45998900-6536-4e99-9962-d2ab6403a691">
        <methodInvocation>mapping</methodInvocation>
        <source>diffNewString(set: Boolean; _value: String io) mapping, protected;

vars

begin
	if set then
		// set code here
	else
		// get code here
		_value	:= diffString( diffNew );
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="set">
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="_value">
            <usage>io</usage>
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="diffOldString" id="5dac0637-e806-40a2-839d-ff8b042c94ba">
        <methodInvocation>mapping</methodInvocation>
        <source>diffOldString(set: Boolean; _value: String io) mapping, protected;

vars

begin
	if set then
		// set code here
	else
		// get code here
		_value	:= diffString( diffOld );
	endif;
end;

</source>
        <access>protected</access>
        <Parameter name="set">
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="_value">
            <usage>io</usage>
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="getChangedSections" id="622d78fa-fd2b-4cf6-b54e-fdf27dbdbaaf">
        <source>getChangedSections( new : Boolean )
						: PointArray;
// provides an array list of offsets where the text is different to the other side

vars
	lineSection	: Point;
	diffs		: CharacterArray;
	start		: Integer;
	index		: Integer;
	count		: Integer;
	
begin
	diffSections.clear();
	
	if new then
		diffs	:= diffNew;
	else	
		diffs	:= diffOld;
	endif;

	foreach index in 1 to diffs.size do
		if diffs[index] = CharDifferent then
			if count = 0 then
				start	:= index;
			endif;
			count += 1;
						
		elseif count &gt; 0 then
			lineSection.set( start, count );		
			diffSections.add( lineSection );		
			count	:= 0;
		endif;
	endforeach;
	if count &gt; 0 then
		lineSection.set( start, count );		
		diffSections.add( lineSection );		
	endif;
	
	return diffSections;
end;
</source>
        <Parameter name="new">
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="PointArray"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="diffString" id="e8805587-91bc-463c-8d3e-c5b127579bd0">
        <source>diffString( diffs : CharacterArray ): String protected;

vars
	length		: Integer;
	inx			: Integer;
	text		: String;
	
begin
	length	:= diffs.size;
	foreach inx in 1 to length do
		text	&amp;= diffs[inx];
	endforeach;
	return text;
end;
</source>
        <access>protected</access>
        <Parameter name="diffs">
            <type name="CharacterArray"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="diffUpdate" id="5570bbeb-db62-4095-ab83-030a508ec984">
        <source>diffUpdate( diff	: CharacterArray input;
			fromPos	: Integer;
			toPos	: Integer;
			value	: Character ) protected;

vars
	inx		: Integer;
	
begin
	foreach inx in fromPos to toPos do
		diff[inx]	:= value;
	endforeach;
end;
</source>
        <access>protected</access>
        <Parameter name="diff">
            <usage>input</usage>
            <type name="CharacterArray"/>
        </Parameter>
        <Parameter name="fromPos">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="toPos">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="value">
            <type name="Character"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="padSides" id="38207d6e-ca32-45f9-89cb-63889870e0b7">
        <source>padSides(oldWords, newWords : StringArray input) protected;
// if the old word has blank space beforehand and the new doesnt, all our words will be out of whack
// so put a spacer in

vars
	oldSize	: Integer;
	newSize	: Integer;
	old		: Boolean;
	new		: Boolean;
	
begin
	oldSize	:= oldWords.size;
	newSize	:= newWords.size;

	if oldSize &lt;= 1 or newSize &lt;= 1 then
		return;
	endif;
	
	// start check
	old	:= wordIsWhitespace( oldWords[1] );
	new	:= wordIsWhitespace( newWords[1] );
	if old &lt;&gt; new then
		if old then
			newWords.insert( 1, null );
		else
			oldWords.insert( 1, null );
		endif;
	endif;
	
	// end check
	old	:= wordIsWhitespace( oldWords[oldSize] );
	new	:= wordIsWhitespace( newWords[newSize] );
	if old &lt;&gt; new then
		if old then
			newWords.add( null );
		else
			oldWords.add( null );
		endif;
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="oldWords">
            <usage>input</usage>
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="newWords">
            <usage>input</usage>
            <type name="StringArray"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="parseWords" id="51187238-bf2e-4515-a065-652c2d9a87b7">
        <source>parseWords( text : String ): StringArray protected;

vars
	inx			: Integer;
	word		: String;
	words		: StringArray;
	lastWhite	: Boolean;
	
begin
	// convert to words
	create words transient;
	while inx &lt; text.length do
		inx		+= 1;
		word	:= text.scanUntil( '"' &amp; "'&lt; =&gt;.;(){}," &amp; Tab, inx );
		if word &lt;&gt; null then
			words.add(word);
		endif;
		if inx = 0 then
			break;
		endif;
		words.add(text[inx]);
	endwhile;

	// condense all the whitespace
	inx		:= words.size;
	while inx &gt; 0 do
		word	:= words[inx];
		if wordIsWhitespace(word) then
			if lastWhite then
				words[inx]	:= word &amp; words[inx+1];
				words.removeAt(inx+1);
			endif;
			lastWhite	:= true;
		else
			lastWhite	:= false;
		endif;
		inx		:= inx - 1;
	endwhile;
	return words;
end;
</source>
        <access>protected</access>
        <Parameter name="text">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="StringArray"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="wordIsWhitespace" id="dfb62069-85f8-4b9f-9cc0-84e0d370a53c">
        <source>wordIsWhitespace(word : String): Boolean protected;

vars
	char : Character;
	inx	 : Integer;
	
begin
	foreach inx in 1 to word.length do
		char	:= word[inx];
		if not char.isOneOf( Tab, " ", Cr, Lf ) then
			return false;
		endif;
	endforeach;
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="word">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
</Class>
