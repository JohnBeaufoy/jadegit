<Class name="GraphModelRoot" id="c43c44c0-d5b5-4bb5-877b-de5110cf9bc2">
    <superclass name="Presentation"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Singleton for the graph information (series, points and lines)</text>
    <PrimAttribute name="columnsUsed" id="107732e9-d134-4673-96d9-4c9ff22e72c8">
        <embedded>true</embedded>
        <type name="Integer"/>
        <text>total number of columns needed to display the points</text>
    </PrimAttribute>
    <ImplicitInverseRef name="commits" id="d25c4425-5c28-4ae0-9d89-d4a3cb73a886">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="CommitArray"/>
        <access>readonly</access>
        <text>Copy of the table commits list.
May be capped, and may have extras to represent the parents of the original list commits.</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="gridContents" id="3442754c-806e-44e6-9526-b17947a9eebd">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="ObjectArray"/>
        <access>protected</access>
        <text>Array of CharacterArrays paired with 'gridSeries'
Each CA represents a column and is the size of the number of commits.
We mark each position where a point or line will be, so we can adjust for clashes.

</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="gridSeries" id="4894fc85-6575-49ed-923b-0e87effd687a">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="ObjectArray"/>
        <access>protected</access>
        <text>Array of ObjectArrays paired with 'gridContents'
Each OA has a list of all series which will be drawn in the column</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lines" id="b80c4cc6-1ee1-4c3b-8dbd-a1dd527d0aaf">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="GraphModelLineDict"/>
        <access>readonly</access>
        <text>Ordered list of all lines</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="points" id="84aaa5ff-43f5-47e2-b546-db0a2821fa8e">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="GraphModelPointArray"/>
        <access>readonly</access>
        <text>List of all points (commits)</text>
    </ImplicitInverseRef>
    <PrimAttribute name="rows" id="8b42f95e-433c-4ab5-b830-a7e1bd3637fd">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
        <text>Number of rows built (could be less than supplied commits due to cap)</text>
    </PrimAttribute>
    <ImplicitInverseRef name="serieses" id="1cd6ca06-f2e9-4bd2-9fa8-8c04fd1703b2">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="GraphModelSeriesArray"/>
        <access>protected</access>
        <text>List of all the series</text>
    </ImplicitInverseRef>
    <JadeMethod name="addLine" id="1a01e680-d656-46e5-be85-68f65ffa2312">
        <updating>true</updating>
        <source>addLine( p1, p2 : GraphModelPoint;
		 series : GraphModelSeries input ) updating, protected;

// sets up a line between two points and puts in a series
		 
vars
	line	: GraphModelLine;
	
begin
	create line transient;
	if p1.row &lt; p2.row then
		line.pointLow	:= p1;
		line.pointHigh	:= p2;
	else
		line.pointLow	:= p2;
		line.pointHigh	:= p1;
	endif;		
	lines.add( line );
	
	series.lines.add( line );
end;
</source>
        <access>protected</access>
        <Parameter name="p1">
            <type name="GraphModelPoint"/>
        </Parameter>
        <Parameter name="p2">
            <type name="GraphModelPoint"/>
        </Parameter>
        <Parameter name="series">
            <usage>input</usage>
            <type name="GraphModelSeries"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="addLineJoins" id="755b85eb-37a9-40cd-beb4-56ae1c4495f1">
        <updating>true</updating>
        <source>addLineJoins() updating, protected;

// the lines have been recorded for the series points, but not yet for the links between them (reflecting merges etc)

vars
	commit			: Commit;
	commitParent	: Commit;
	parentIndex		: Integer;
	count			: Integer;
	point			: GraphModelPoint;
	pointParent		: GraphModelPoint;
	
begin
	foreach commit in commits do
		parentIndex	:= commit.parents.size;
		point		:= getPoint( commit );
		
		while parentIndex &gt; 0 do
			commitParent	:= commit.parents[parentIndex];
			pointParent		:= getPoint(commitParent);
			if pointParent &lt;&gt; null then
				if parentIndex &gt; 1 then
					addLine( point, pointParent, pointParent.series );
				elseif pointParent.series &lt;&gt; point.series then
					addLine( point, pointParent, point.series );
				endif;
			endif;
			
			parentIndex	+= -1;
		endwhile;
	endforeach;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="addPoint" id="2c92d0e8-7f5d-4324-a2b2-0a8ebab676af">
        <source>addPoint( commit : Commit ) protected;

// adds a point to our structure reflecting a commit

vars
	point	: GraphModelPoint;
		
begin
	create point transient;
	point.commit	:= commit;
	point.row		:= points.size + 1;
	points.add(point);
	
	commits.add( commit );
end;
</source>
        <access>protected</access>
        <Parameter name="commit">
            <type name="Commit"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="addPoints" id="08484001-6c74-4c06-aeae-b209747d85b0">
        <updating>true</updating>
        <source>addPoints(tableCommits : CommitArray;
		  maxCommits   : Integer ) updating, protected;

// builds our list of points
		   
vars
	commit		: Commit;
	commitParent: Commit;
	parentIndex : Integer;
	count		: Integer;
	extraCommits: CommitArray;
	
begin
	rows	:= maxCommits.min( tableCommits.size );

	create extraCommits transient;

	foreach commit in tableCommits do
		addPoint( commit );
	
		// make sure parents are included in our range
		foreach commitParent in commit.parents do
			parentIndex		:= tableCommits.indexOf( commitParent );
			if (parentIndex = 0 or parentIndex &gt; rows)
			and (extraCommits.includes( commitParent ) = false) then
				extraCommits.add( commitParent );
			endif;
		endforeach;
	
		// size check
		count += 1;
		if count = rows then
			break;
		endif;
	endforeach;
	
	// add in the extras, so the lines run off the bottom of the control correctly
	foreach commit in extraCommits do
		addPoint( commit );
	endforeach;
		
epilog
	delete extraCommits;
end;
</source>
        <access>protected</access>
        <Parameter name="tableCommits">
            <type name="CommitArray"/>
        </Parameter>
        <Parameter name="maxCommits">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="addSeries" id="344a49fb-4737-4789-ad64-5d8ca1db45ff">
        <updating>true</updating>
        <source>addSeries() updating, protected;

// sets up all the series

vars
	commit				: Commit;
	commitsUnprocessed 	: CommitArray;
	point		   	    : GraphModelPoint;
	pointLow			: GraphModelPoint;
	series				: GraphModelSeries;
	index  				: Integer;
	
begin
	create commitsUnprocessed transient;
	commits.copy( commitsUnprocessed );
	
	while commitsUnprocessed.size &gt; 0 do
		commit	:= commitsUnprocessed.removeAt(1);
		point   := getPoint(commit);
				
		while point.series = null do
			
			if series = null then
				create series transient;
				serieses.add(series);
			endif;
			series.points.add(point);
			
			commit	:= commit.parents.first();
			if commit = null then
				break;
			endif;
			index	:= commitsUnprocessed.indexOf(commit);
			if index = 0 then
				break;
			endif;
			commitsUnprocessed.removeAt(index);
			
			pointLow	:= point;
			point 		:= getPoint(commit);
			addLine( pointLow, point, series );
		endwhile;
		
		series	:= null;
	endwhile;
	
epilog
	delete commitsUnprocessed;
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="build" id="f56174f2-816f-4637-b8f3-b12d5065c259">
        <updating>true</updating>
        <source>build(	tableCommits : CommitArray;
		maxCommits   : Integer;
		maxColumns	 : Integer;
		colours		 : StringArray ) updating;

// build the transient graph structure of points, lines and series
		
begin
	addPoints(tableCommits, maxCommits);
	addSeries();
	addLineJoins();
	dethatch(maxColumns);
	colouring(colours);
end;
</source>
        <Parameter name="tableCommits">
            <type name="CommitArray"/>
        </Parameter>
        <Parameter name="maxCommits">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="maxColumns">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="colours">
            <type name="StringArray"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="colouring" id="2941b707-19dc-416d-a546-70524ef87109">
        <source>colouring(colours : StringArray) protected;

// assigns a colour to all points and lines in the structure
// tries to reuse colours once series have finished (which is non-trivial to work out)

vars
	seriesActive	: GraphModelSeriesArray;
	seriesByStart	: DynaDictionary;
	series			: GraphModelSeries;
	seriesFinished	: GraphModelSeries;
	colour			: String;
	coloursUsed		: StringArray;
	rowCurrent		: Integer;
	index			: Integer;
	line			: GraphModelLine;
	
begin
	create seriesByStart transient;
	seriesByStart.setMembership( GraphModelSeries );
	seriesByStart.addExternalKey( Integer, 0, false, false );
	seriesByStart.endKeys( true );
	foreach series in serieses do
		seriesByStart.putAtKey( series.rowLow, series );
	endforeach;
	
	create seriesActive transient;
	create coloursUsed transient;
	
	foreach series in seriesByStart as GraphModelSeries do
	
		// remove any series that have now finished, so we get our colours back
		rowCurrent	:= series.rowLow;
		index		:= seriesActive.size;
		while index &gt; 0 do
			if seriesActive[index].rowHigh &lt; rowCurrent then
				seriesActive.removeAt(index);
				coloursUsed.removeAt(index);
			endif;
			index += -1;
		endwhile;
		
		// assign a colour
		if series.colour = "" then
			foreach colour in colours do
				if not coloursUsed.includes( colour ) then
					break;
				endif;
			endforeach;
		
			series.colour	:= colour;
			coloursUsed.add( colour );
			seriesActive.add( series );
						
			foreach line in series.lines do
				line.colour	:= colour;
			endforeach;
		endif;
	endforeach;
	
epilog
	delete seriesActive;
	delete seriesByStart;
	delete coloursUsed;
end;
</source>
        <access>protected</access>
        <Parameter name="colours">
            <type name="StringArray"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="delete" id="212ea5cc-0690-4f7e-b4dd-08a981f3a5de">
        <updating>true</updating>
        <source>delete() updating;

vars

begin
	points.purge();
	serieses.purge();
	lines.purge();
	
	gridContents.purge();
	gridSeries.purge();
end;
</source>
    </JadeMethod>
    <JadeMethod name="dethatch" id="976ab571-2ba5-429c-8888-36c4d10a727d">
        <updating>true</updating>
        <source>dethatch(maxColumns : Integer) updating, protected;

// assigns columns to each of the series, trying to avoid line collisions as much as possible

vars
	seriesBySpan	: DynaDictionary;
	series			: GraphModelSeries;
	seriesBackbone	: GraphModelSeries;
	seriesArray		: GraphModelSeriesArray;
	count			: Integer;
	index			: Integer;
	span			: Integer;
	
begin
	create seriesBySpan transient;
	seriesBySpan.setMembership( GraphModelSeries );
	seriesBySpan.addExternalKey( Integer, 0, true, false );
	seriesBySpan.endKeys( true );

	foreach series in serieses do
		span	:= series.span;
		if span &gt; 0 then
			seriesBySpan.putAtKey( span, series );
		endif;
	endforeach;	
		
	// put the first entry last in the list, meaning its processed first as we count down
	// this means the main branch is often on the left of screen which is generally cleaner
	create seriesArray transient;
	seriesBySpan.copy( seriesArray );
	if not seriesArray.isEmpty then
		series	:= seriesArray.removeAt(1);
		seriesArray.add( series );
	endif;

	// loop until all series have been inserted
	// may require several passes as parents need assigning before children
	while seriesArray.isEmpty = false do
		
		index	:= seriesArray.size;
		while index &gt; 0 do
			series	:= seriesArray[index];
			if gridProcessSeries( series, maxColumns ) then
				seriesArray.removeAt(index);
			endif;
			index   := index - 1;
		endwhile;
	endwhile;
	
	// save the column positions
	foreach series in seriesBySpan as GraphModelSeries do
		count	:= gridSeriesIndex( series );
		series.column	:= count;
		columnsUsed		:= columnsUsed.max(series.column);
	endforeach;	
	
	// protect for edge cases (most likely scenario is a single commit)
	foreach series in serieses do
		if series.column = 0 then
			series.column	:= 1;
		endif;
	endforeach;
		
epilog
	delete seriesArray;
	delete seriesBySpan;
end;
</source>
        <access>protected</access>
        <Parameter name="maxColumns">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="getPoint" id="691a350e-c666-48d1-a9a8-e2dac3d5789f">
        <source>getPoint( commit : Commit ): GraphModelPoint protected;

// passes back the point reflecting the commit

vars
	row		: Integer;
	
begin
	row		:= commits.indexOf( commit );
	if row &gt; 0 then
		return points[row];
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="commit">
            <type name="Commit"/>
        </Parameter>
        <ReturnType>
            <type name="GraphModelPoint"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="gridPointsAdd" id="859eecb2-4ae9-4541-ab37-16128319e470">
        <source>gridPointsAdd(series  : GraphModelSeries;
			  column  : Integer;
			  update  : Boolean 
					 ): Boolean protected;	// true if no collision as detected

// mark the points in use for the series in the column so we minimise collisions with other points (if update set)
					 
constants
	UsedPosition = 'X';
					   
vars
	content		: Character;
	contents	: CharacterArray;
	rowLow		: Integer;
	rowHigh		: Integer;
	row			: Integer;
	
begin
	contents	:= gridContents[column].CharacterArray;

	rowLow		:= series.rowLow;
	rowHigh		:= series.rowHigh;

	if series.parent &lt;&gt; null then
		rowLow	+= 1;
		rowHigh += -1;
	endif;	

	foreach row in rowLow to rowHigh do
		if contents[row] = UsedPosition then
			return false;		// position already in use
		endif;
	endforeach;	
	
	if update then
		// go ahead and mark the column as in use
		foreach row in rowLow to rowHigh do
			contents[row]	:= UsedPosition;
		endforeach;	
	endif;
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="series">
            <type name="GraphModelSeries"/>
        </Parameter>
        <Parameter name="column">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="update">
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="gridProcessSeries" id="7ce38105-f064-41e7-b57c-d3cce279a187">
        <updating>true</updating>
        <source>gridProcessSeries( series 	  : GraphModelSeries;
				   maxColumns : Integer 
							) : Boolean updating, protected;
						
// attempts to find a slot in the columns to place the series of points
// this can only be achieved if the parent series exists

vars
	column			: Integer;
	contentsArray	: CharacterArray;
	seriesArray		: GraphModelSeriesArray;
	parentSeries	: GraphModelSeries;
		
begin
	// ideally we can find an existing column with a gap in it
	
	if gridSeries.size &gt; 0 then
		parentSeries	:= series.parent();
		if parentSeries &lt;&gt; null then
			column	:= gridSeriesIndex( parentSeries );
			if column = 0 then
				return false;	// parent needs doing first
			endif;
			
			// check all columns to the right of the parent
			while column &lt; gridContents.size do
				column	+= 1;
				if gridPointsAdd( series, column, true ) then
					// record that we have the series in this column
					seriesArray		:= gridSeries[column].GraphModelSeriesArray;
					seriesArray.add( series );
					return true;
				else
					// clash detected
				endif;					
			endwhile;
		endif;
	endif;

	// clashes detected on existing columns, try and add a new one
	column	:= (gridSeries.size + 1).min(maxColumns);
	if column &gt; gridSeries.size then
		// add a new column on the end
		create contentsArray transient;
		contentsArray[commits.size]		:= null;
		gridContents.add( contentsArray );
		
		create seriesArray transient;
		gridSeries.add( seriesArray );
		
	else
		// have run out of allowed columns, jam into the last one
		contentsArray	:= gridContents[column].CharacterArray;
		seriesArray		:= gridSeries[column].GraphModelSeriesArray;
	endif;
	
	// record the series in the new column
	seriesArray.add( series );
	gridPointsAdd( series, column, true );		
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="series">
            <type name="GraphModelSeries"/>
        </Parameter>
        <Parameter name="maxColumns">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="gridSeriesIndex" id="636963d1-6276-41b2-b346-c3d4de3f038b">
        <source>gridSeriesIndex( series : GraphModelSeries 
					   ): Integer protected;

// return the column which the series is currently in
							 
constants
	NotFound = 0;

vars
	gridSeriesColumn	: GraphModelSeriesArray;
	column				: Integer;
	
begin
	foreach gridSeriesColumn in gridSeries as GraphModelSeriesArray do
		column	+= 1;
		if gridSeriesColumn.includes( series ) then
			return column;
		endif;
	endforeach;
	return NotFound;
end;
</source>
        <access>protected</access>
        <Parameter name="series">
            <type name="GraphModelSeries"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
</Class>
