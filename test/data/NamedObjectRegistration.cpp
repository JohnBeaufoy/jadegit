#include <catch2/catch_test_macros.hpp>
#include <data/NamedObjectRegistration.h>

using namespace JadeGit::Data;

TEST_CASE("NamedObjectRegistration.Alias", "[data]") 
{
	CHECK(defaultNamedObjectAlias("Class") == "class");
	CHECK(defaultNamedObjectAlias("ExternalMethod") == "external method");
	CHECK(defaultNamedObjectAlias("HTMLClass") == "HTML class");
	CHECK(defaultNamedObjectAlias("JadeMethod") == "jade method");
}