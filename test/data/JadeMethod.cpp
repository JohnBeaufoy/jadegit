#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Method.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/MetaSchema/MetaType.h>
#include <jadegit/data/ScriptElement.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("JadeMethod.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");

	{
		JadeMethod method(&klass, nullptr, "testMethod");

		// Check name has been set
		CHECK("testMethod" == method.name);
		CHECK("testMethod" == method.GetValue("name").Get<string>());

		// Check method has been added to collection
		CHECK(&method == klass.methods.Get("testMethod"));
	}

	// Check method has been removed from collection
	CHECK(nullptr == klass.methods.Get("testMethod"));
}

TEST_CASE("JadeMethod.Rename", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/JadeMethod/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema");

	// Load existing class
	auto& klass = Entity::resolve<Class&>(schema, "TestClass");

	// Load existing method
	Method* method = Entity::resolve<Method>(klass, "testMethod", false);

	// Verify method is in collections with current name
	CHECK(method == klass.methods.Get("testMethod"));
	
	// Rename method
	method->Rename("testMethodRenamed");

	// Check name change has been applied
	CHECK("testMethodRenamed" == method->name);
	CHECK(nullptr == klass.methods.Get("testMethod"));
	CHECK(method == klass.methods.Get("testMethodRenamed"));

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("JadeMethod.Write", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Class/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load class
	Class* klass = Entity::resolve<Class>(schema, "TestClass", false);
	REQUIRE(klass);

	JadeMethod method(klass, nullptr, "testMethod");
	method.Created("47183823-2574-4bfd-b411-99ed177d3e43");

	Parameter param(&method, nullptr, "param1");
	param.Modified();
	param.type = assembly.GetRootSchema().string;

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}