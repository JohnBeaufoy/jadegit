#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Database.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("DbFile.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	Database database(&schema, nullptr, "TestDatabase");

	{
		DbFile file(&database, nullptr, "TestDbFile");

		// Check name has been set as expected
		CHECK("TestDbFile" == file.name);
		CHECK("TestDbFile" == file.GetValue("name").Get<string>());

		// Check file has been added to collection
		CHECK(&file == database.files.Get("TestDbFile"));
	}

	// Check implicit deletion has removed from collection
	CHECK(nullptr == database.files.Get("TestDbFile"));
}

TEST_CASE("DbFile.ClassMapsInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Database database(&schema, nullptr, "TestDatabase");

	Class klass(&schema, nullptr, "TestClass");
	DbClassMap classMap(&klass, nullptr);

	{
		DbFile file(&database, nullptr, "TestDbFile");

		// Adding class map to file should set inverse
		file.classMapRefs.Add(&classMap);
		CHECK(&file == classMap.diskFile);
	}

	// Check class map file has been reset implicitly when file is deleted
	CHECK(nullptr == static_cast<DbFile*>(classMap.diskFile));
}