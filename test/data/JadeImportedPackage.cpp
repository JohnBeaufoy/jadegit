#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("JadeImportedPackage.Resolution", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "BaseSchema");
	JadeImportedPackage package(&schema, nullptr, "ImportedPackage");

	// Check local package can be resolved irrespective of inherit option
	CHECK(Entity::resolve<JadeImportedPackage>(schema, "ImportedPackage", false, false) == &package);
	CHECK(Entity::resolve<JadeImportedPackage>(schema, "ImportedPackage", false, true) == &package);

	Schema subschema(assembly, nullptr, "SubSchema");
	subschema.superschema = &schema;

	// Check inherited package can be resolved when inherit option is used
	CHECK(Entity::resolve<JadeImportedPackage>(subschema, "ImportedPackage", false, false) == nullptr);
	CHECK(Entity::resolve<JadeImportedPackage>(subschema, "ImportedPackage", false, true) == &package);
}