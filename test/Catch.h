#pragma once
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <Exception.h>

namespace Catch
{
	class TestInvokerAsStdFunction : public Catch::ITestInvoker
	{
	public:
		TestInvokerAsStdFunction(std::function<void()> f) : f(f) {}

		void invoke() const final { f(); }

	public:
		std::function<void()> f;
	};

	inline Detail::unique_ptr<ITestInvoker> makeTestInvoker(std::function<void()> f) noexcept
	{
		return Detail::make_unique<TestInvokerAsStdFunction>(f);
	}
}

CATCH_TRANSLATE_EXCEPTION(std::exception const& ex) {
	std::stringstream ss;
	JadeGit::print_exception_with_nested(ss, ex);
	return ss.str();
}