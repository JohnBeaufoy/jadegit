#include "TestDbContext.h"
#include <filesystem>
#include <fstream>

using namespace std;

namespace JadeGit
{
	filesystem::path resetTestDatabase()
	{
		// Derive test system path from current path (build directory)
		auto path = filesystem::current_path() / "test" / "system";

		// Purge prior
		filesystem::remove_all(path);

		// Clone empty system
		filesystem::copy(filesystem::current_path() / "deps" / "jade" / "empty", path, std::filesystem::copy_options::recursive);

		// Make INI file
		ofstream ini(path / "jade.ini");
		ini << "[JadeLog]" << endl;
		ini << "LogDirectory=" << (filesystem::current_path() / "test" / "logs").string() << endl;

		return path;
	}

	TestDbContext::TestDbContext(const std::filesystem::path& path) : Jade::DbContext(path.string(), (path / "jade.ini").string())
	{
	}

	TestDbContext::TestDbContext() : TestDbContext(resetTestDatabase())
	{
	}
}