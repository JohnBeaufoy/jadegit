#include "TestRepository.h"
#include <jadegit/vfs/RecursiveFileIterator.h>
#include <vfs/GitFileSystem.h>

using namespace std;

namespace JadeGit
{
	class Main
	{
	public:
		Main()
		{
			git_libgit2_init();
		}

		~Main()
		{
			git_libgit2_shutdown();
		}
	};
	static Main main;

	TestRepository::TestRepository() : fs("repo")
	{
		// Initialize bare repository
		git_throw(git_repository_init(git_ptr(repo), fs.path().string().c_str(), true));

		// Open config
		unique_ptr<git_config> config;
		git_throw(git_repository_config(git_ptr(config), repo.get()));

		// Set default/required configuration
		git_throw(git_config_set_bool(config.get(), "core.autocrlf", false));
	}

	TestRepository::TestRepository(const FileSystem& src) : TestRepository()
	{
		unique_ptr<git_index> index;
		git_throw(git_repository_index(git_ptr(index), repo.get()));

		GitFileSystem dst(index.get());

		// Populate repository using files from source supplied
		for (auto& file : RecursiveFileIterator(src.open("")))
		{
			if (!file.isDirectory())
			{
				assert(!file.path().empty());

				file.copy(dst.open(file.path()));
			}
		}

		// Write index to tree
		git_oid id;
		git_throw(git_index_write_tree(&id, index.get()));

		// Lookup tree
		unique_ptr<git_tree> tree;
		git_throw(git_tree_lookup(git_ptr(tree), repo.get(), &id));

		// Create signature
		unique_ptr<git_signature> signature;
		git_throw(git_signature_now(git_ptr(signature), "Test", "test@test.com"));

		// Create commit
		git_throw(git_commit_create(&id, repo.get(), "HEAD", signature.get(), signature.get(), nullptr, "Initial commit", tree.get(), 0, nullptr));
	}

	TestRepository::operator git_repository* () const
	{
		return repo.get();
	}

	TestRepository::operator git_commit* () const
	{
		if (!commit)
		{
			unique_ptr<git_reference> head;
			auto error = git_repository_head(git_ptr(head), repo.get());
			if (error == GIT_EUNBORNBRANCH)
				return nullptr;
			git_throw(error);

			git_throw(git_commit_lookup(git_ptr(commit), repo.get(), git_reference_target(head.get())));
		}

		return commit.get();
	}

	TestRepository::operator git_index* () const
	{
		if (!index)
			git_throw(git_repository_index(git_ptr(index), repo.get()));

		return index.get();
	}
}