#include <ApprovalTests.hpp>
#include <data/MockEntityIDAllocator.h>
#include <extract/Assembly.h>
#include <extract/SchemaIterator.h>
#include <jade/Loader.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <source_location>
#include "Catch.h"
#include "TestAppContext.h"

using namespace std;
using namespace Jade;
using namespace JadeGit;
using namespace JadeGit::Extract;

namespace fs = std::filesystem;

// Handle loading test schemas
void load(fs::path path)
{
	fs::path commands = path / "Commands.jcf";
	if (fs::exists(commands))
	{
		Loader().loadCommandFile(commands, false);
	}

	fs::path mul = path / "All.mul";
	if (fs::exists(mul))
	{
		Loader().loadSchemaFile(mul, fs::path(), false);
	}
	else
	{
		fs::path scm(path / "TestSchema.scm");
		fs::path ddb(path / "TestSchema.ddx");

		Loader().loadSchemaFile(fs::exists(scm) ? scm : fs::path(), fs::exists(ddb) ? ddb : fs::path(), false);
	}
}

// Handle performing full extract & compare with expected
void extract(MemoryFileSystem& fs, fs::path path)
{
	// Extract all schemas (implicitly saves changes to filesystem)
	Extract::Assembly(fs).extract();

	// Compare with expected
	auto namer = ApprovalTests::TemplatedCustomNamer::create((path / "extract").generic_string() + ".{ApprovedOrReceived}");
	ApprovalTests::Approvals::verify(fs, ApprovalTests::Options().withNamer(namer));
}

void extract(fs::path scenario)
{
	// Setup mock entity id allocator
	Data::MockEntityIDAllocator allocator;

	// Setup test filesystem
	MemoryFileSystem fs;

	// Setup test database
	TestDbContext db;
	TestAppContext app(db);

	fs::path last;

	// Handle testing loading/extracting sequence of snapshots
	for (const auto& snapshot : fs::directory_iterator(scenario))
	{
		if (!snapshot.is_directory())
			continue;

		// Load test schemas
		INFO((scenario.stem() / snapshot.path().stem()).generic_string() + ": Loading");
		load(snapshot.path());

		// Extract & compare with expected
		INFO((scenario.stem() / snapshot.path().stem()).generic_string() + ": Extracting");
		extract(fs, scenario / snapshot.path().stem());

		// Store last snapshot for re-extract
		last = scenario / snapshot.path().stem();
	}

	// Handle testing single load/extract if sequence of snapshots wasn't found above
	if (last.empty())
	{
		// Load test schemas
		INFO(scenario.stem().generic_string() + ": Loading");
		load(scenario);

		// Extract & compare with expected
		INFO(scenario.stem().generic_string() + ": Extracting");
		extract(fs, scenario);

		last = scenario;
	}

	// Extract & compare with expected again to verify what's stored initially is found/reused as expected)
	INFO(scenario.stem().generic_string() + ": Extracting (again)");
	extract(fs, last);
}

class ExtractScenariosRegistrar
{
public:
	ExtractScenariosRegistrar()
	{
		for (const auto& scenario : fs::directory_iterator(filesystem::path(source_location::current().file_name()).parent_path() / "scenarios"))
		{
			filesystem::path path = scenario.path();

			REGISTER_TEST_CASE([path](){ extract(path); }, std::format("{}", path.filename().string()), "[extract]");
		}
	}
};
static ExtractScenariosRegistrar registrar;