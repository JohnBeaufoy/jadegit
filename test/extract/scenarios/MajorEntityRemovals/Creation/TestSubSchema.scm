jadeVersionNumber "20.0.02";
schemaDefinition
TestSubSchema subschemaOf TestSuperSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	2057 "English (United Kingdom)";
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.089;
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.089;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.089;
libraryDefinitions
typeHeaders
	TestSubSchema subclassOf TestSuperSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2195;
	DeprecatedSubClass subclassOf DeprecatedClass highestOrdinal = 1, number = 2200;
	GTestSubSchema subclassOf GTestSuperSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2196;
	TestServiceProvider subclassOf JadeWebServiceProvider transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2203;
	TestClass subclassOf Object transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, highestOrdinal = 2, number = 2201;
	STestSubSchema subclassOf STestSuperSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2197;
 
interfaceDefs
	DeprecatedSubInterface number = 1290
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:08:23.724;
	)
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	TestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
	)
	DeprecatedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:57.519;
	)
	DeprecatedSubClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:06:27.868;
	attributeDefinitions
		number:                        Integer readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:12:17.281;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	GTestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
	)
	JadeWebService completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 260208 2008:03:04:13:10:57.584;
	webServicesClassProperties
	(
		wsdl = ``;
	)
	)
	JadeWebServiceProvider completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 130307 2007:03:15:14:50:43.084;
	webServicesClassProperties
	(
		additionalInfo = ``;
		wsdl = ``;
		secureService = default;
	)
	)
	TestServiceProvider completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:09:35.491;
	webServicesClassProperties
	(
		additionalInfo = ``;
		wsdl = ``;
		secureService = default;
	)
 
	webServicesMethodDefinitions
		deprecatedServiceMethod(): DeprecatedSubClass webService, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:10:17.437;
		webServicesMethodProperties
		(
			inputEncodingStyle = "";
			inputNamespace = "";
			inputUsesEncodedFormat = false;
			outputEncodingStyle = "";
			outputNamespace = "";
			outputUsesEncodedFormat = false;
			soapAction = "";
			useBareStyle = false;
			useSoap12 = false;
			usesRPC = default;
			wsdlName = "";
			soapHeaders = null;
		)
		testServiceMethod(): TestClass webService, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:10:37.861;
		webServicesMethodProperties
		(
			inputEncodingStyle = "";
			inputNamespace = "";
			inputUsesEncodedFormat = false;
			outputEncodingStyle = "";
			outputNamespace = "";
			outputUsesEncodedFormat = false;
			soapAction = "";
			useBareStyle = false;
			useSoap12 = false;
			usesRPC = default;
			wsdlName = "";
			soapHeaders = null;
		)
	)
	TestClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:09:47.743;
	attributeDefinitions
		name:                          String[31] readonly, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:10:51.616;
	referenceDefinitions
		deprecatedProperty:            DeprecatedClass  readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:06:53.527;
 
	jadeMethodDefinitions
		deprecatedMethod(): DeprecatedClass number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:09:03.301;
	implementInterfaces
		DeprecatedInterface
		(
		deprecatedMethod is deprecatedMethod;
		)
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	STestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
	)
 
inverseDefinitions
databaseDefinitions
TestSubSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
	databaseFileDefinitions
		"testsubschema" number = 59;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:41.101;
	defaultFileDefinition "testsubschema";
	classMapDefinitions
		STestSubSchema in "_environ";
		TestSubSchema in "_usergui";
		GTestSubSchema in "testsubschema";
		DeprecatedSubClass in "testsuperschema";
	)
schemaViewDefinitions
_exposedListDefinitions
DeprecatedExposure version=1, priorVersion=0, registryId="_WebServices_Provider"
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:12:23.783;
(
	DeprecatedClass
	(
	_exposedPropertyDefinitions
		name;
	)
	DeprecatedServiceProvider defaultStyle=99
	(
	)
	Object
	(
	)
)
TestExposure version=1, priorVersion=0, registryId="_WebServices_Provider"
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:12:32.271;
(
	DeprecatedClass
	(
	_exposedPropertyDefinitions
		name;
	)
	DeprecatedSubClass
	(
	_exposedPropertyDefinitions
		number;
	)
	Object
	(
	)
	TestClass
	(
	_exposedPropertyDefinitions
		deprecatedProperty;
		name;
	)
	TestServiceProvider defaultStyle=99
	(
	)
)
exportedPackageDefinitions
typeSources
	TestServiceProvider (
	webServicesMethodSources
deprecatedServiceMethod
{
deprecatedServiceMethod(): DeprecatedSubClass webService;

begin
	return null;
end;

}

testServiceMethod
{
testServiceMethod(): TestClass webService;

begin
	return null;
end;

}

	)
	TestClass (
	jadeMethodSources
deprecatedMethod
{
deprecatedMethod(): DeprecatedClass;

begin
	return null;
end;

}

	)
