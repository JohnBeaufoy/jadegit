jadeVersionNumber "20.0.02";
schemaDefinition
TestSuperSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	2057 "English (United Kingdom)";
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
libraryDefinitions
typeHeaders
	TestSuperSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2192;
	DeprecatedClass subclassOf Object highestOrdinal = 1, number = 2198;
	GTestSuperSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2193;
	DeprecatedServiceProvider subclassOf JadeWebServiceProvider transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2202;
	STestSuperSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2194;
 
interfaceDefs
	DeprecatedInterface number = 1288
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:07:05.965;
 
	jadeMethodDefinitions
		deprecatedMethod(): DeprecatedClass number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:08:40.061;
	)
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	DeprecatedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:57.519;
	attributeDefinitions
		name:                          String[31] readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:12:09.566;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	JadeWebService completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 260208 2008:03:04:13:10:57.584;
	webServicesClassProperties
	(
		wsdl = ``;
	)
	)
	JadeWebServiceProvider completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 130307 2007:03:15:14:50:43.084;
	webServicesClassProperties
	(
		additionalInfo = ``;
		wsdl = ``;
		secureService = default;
	)
	)
	DeprecatedServiceProvider completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:08:55.535;
	webServicesClassProperties
	(
		additionalInfo = ``;
		wsdl = ``;
		secureService = default;
	)
 
	webServicesMethodDefinitions
		deprecatedServiceMethod(): DeprecatedClass webService, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:20:10:09:18.485;
		webServicesMethodProperties
		(
			inputEncodingStyle = "";
			inputNamespace = "";
			inputUsesEncodedFormat = false;
			outputEncodingStyle = "";
			outputNamespace = "";
			outputUsesEncodedFormat = false;
			soapAction = "";
			useBareStyle = false;
			useSoap12 = false;
			usesRPC = default;
			wsdlName = "";
			soapHeaders = null;
		)
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
 
inverseDefinitions
databaseDefinitions
TestSuperSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.664;
	databaseFileDefinitions
		"testsuperschema" number = 58;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.664;
	defaultFileDefinition "testsuperschema";
	classMapDefinitions
		STestSuperSchema in "_environ";
		TestSuperSchema in "_usergui";
		GTestSuperSchema in "testsuperschema";
		DeprecatedClass in "testsuperschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	DeprecatedServiceProvider (
	webServicesMethodSources
deprecatedServiceMethod
{
deprecatedServiceMethod(): DeprecatedClass webService;

begin
	return null;
end;

}

	)
	DeprecatedInterface (
	jadeMethodSources
deprecatedMethod
{
deprecatedMethod(): DeprecatedClass;
}

	)
