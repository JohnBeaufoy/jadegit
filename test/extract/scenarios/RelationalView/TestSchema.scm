jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:38.995;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:38.995;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:38.995;
libraryDefinitions
typeHeaders
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2048;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2049;
	TestObject subclassOf Object highestOrdinal = 1, number = 2054;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2050;
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	TestObject completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:58.406;
	attributeDefinitions
		name:                          String[31] number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:45:06.995;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Set completeDefinition
	(
	)
	ObjectSet completeDefinition
	(
	)
	List completeDefinition
	(
	)
	Array completeDefinition
	(
	)
	ObjectArray completeDefinition
	(
	)
 
inverseDefinitions
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	databaseFileDefinitions
		"testschema" number = 51;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	defaultFileDefinition "testschema";
	classMapDefinitions
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
		TestObject in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
