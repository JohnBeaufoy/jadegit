jadeVersionNumber "20.0.02";
schemaDefinition
TestExportSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.438;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.389;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.389;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.389;
libraryDefinitions
typeHeaders
	TestExportSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2188;
	GTestExportSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2189;
	PackagedClass subclassOf Object highestOrdinal = 1, number = 2194;
	STestExportSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2190;
 
interfaceDefs
	IPackagedInterface number = 1287
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:04:45.594;
 
	jadeMethodDefinitions
		packagedInterfaceMethod(): PackagedClass number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:12:20.469;
	)
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.436;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.437;
	)
	PackagedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:16:32.354;
	constantDefinitions
		PackagedConstant:              String = "Test" number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:07:01.297;
	attributeDefinitions
		name:                          String[31] readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:05:55.894;
 
	jadeMethodDefinitions
		packagedInterfaceMethod(): PackagedClass protected, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:16:46.305;
		packagedMethod(): Boolean number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:06:43.812;
	implementInterfaces
		IPackagedInterface
		(
		packagedInterfaceMethod is packagedInterfaceMethod;
		)
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.438;
	)
 
inverseDefinitions
databaseDefinitions
TestExportSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.439;
	databaseFileDefinitions
		"testexportschema" number = 58;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:41.440;
	defaultFileDefinition "testexportschema";
	classMapDefinitions
		STestExportSchema in "_environ";
		TestExportSchema in "_usergui";
		GTestExportSchema in "testexportschema";
		PackagedClass in "testexportschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
	ExportedPackage
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:09:11.193;
	exportedClassDefinitions
	PackagedClass
		(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:05:32.636;
		exportedConstantDefinitions
			PackagedConstant;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:07:48.394;
		exportedPropertyDefinitions
			name readonly;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:07:43.534;
		exportedMethodDefinitions
			packagedMethod;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:07:45.702;
		)
	exportedInterfaceDefinitions
		IPackagedInterface
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:05:32.640;
	)
typeSources
	PackagedClass (
	jadeMethodSources
packagedInterfaceMethod
{
packagedInterfaceMethod(): PackagedClass protected;

begin
	return null;
end;

}

packagedMethod
{
packagedMethod(): Boolean;

begin
	return false;
end;

}

	)
	IPackagedInterface (
	jadeMethodSources
packagedInterfaceMethod
{
packagedInterfaceMethod(): PackagedClass;
}

	)
