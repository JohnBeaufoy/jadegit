jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:42:14.087;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:42:14.090;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:42:14.089;
libraryDefinitions
typeHeaders
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2187;
	TestDb subclassOf ExternalDatabase transient, final, subschemaFinal, number = 2280;
	Catalog_TableOfContents subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 2, number = 2190;
	Companies subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 15, number = 2191;
	CompanyTypes subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 6, number = 2192;
	Contacts subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 13, number = 2193;
	EmployeePrivileges subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 7, number = 2194;
	Employees subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 16, number = 2195;
	MRU subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 5, number = 2196;
	NorthwindFeatures subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 7, number = 2197;
	OrderDetailStatus subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 7, number = 2199;
	OrderDetails subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 11, number = 2198;
	OrderStatus subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 8, number = 2201;
	Orders subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 18, number = 2200;
	Privileges subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 6, number = 2202;
	ProductCategories subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 9, number = 2203;
	ProductVendors subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 7, number = 2205;
	Products subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 16, number = 2204;
	PurchaseOrderDetails subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 10, number = 2206;
	PurchaseOrderStatus subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 7, number = 2208;
	PurchaseOrders subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 18, number = 2207;
	States subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 2, number = 2209;
	StockTake subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 9, number = 2210;
	Strings subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 6, number = 2211;
	SystemSettings subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 4, number = 2212;
	TaxStatus subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 6, number = 2213;
	Titles subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 5, number = 2214;
	USysRibbons subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 3, number = 2216;
	UserSettings subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 4, number = 2215;
	Welcome subclassOf ExternalObject transient, final, subschemaFinal, highestOrdinal = 4, number = 2217;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2188;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2189;
	Catalog_TableOfContentsByTocPageDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2218;
	Catalog_TableOfContentsByTocTitleDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2219;
	CompaniesByCompanyIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2221;
	CompaniesByCompanyNameCompanyTypeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2222;
	CompaniesByCompanyTypeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2223;
	CompaniesByStandardTaxStatusIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2224;
	CompaniesByStateAbbrevDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2225;
	CompanyTypesByCompanyTypeDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2227;
	CompanyTypesByCompanyTypeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2228;
	ContactsByCompanyIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2230;
	ContactsByCompanyIDFirstNameLastNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2231;
	ContactsByCompanyIDLastNameFirstNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2232;
	ContactsByContactIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2233;
	EmployeePrivilegesByEmployeeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2235;
	EmployeePrivilegesByEmployeePrivilegeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2236;
	EmployeePrivilegesByEmployeePrivilegeIDPrivilegeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2237;
	EmployeePrivilegesByPrivilegeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2238;
	EmployeesByEmployeeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2240;
	EmployeesByFirstNameLastNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2241;
	EmployeesByLastNameFirstNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2242;
	EmployeesBySupervisorIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2243;
	EmployeesByTitleDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2244;
	EmployeesByWindowsUserNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2245;
	MRUByEmployeeIDDateAddedDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2247;
	MRUByEmployeeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2248;
	MRUByEmployeeIDTableNamePKValueDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2249;
	MRUByMRU_IDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2250;
	NorthwindFeaturesByNorthwindFeaturesIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2252;
	OrderDetailStatusByOrderDetailStatusIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2254;
	OrderDetailStatusByOrderDetailStatusNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2255;
	OrderDetailStatusBySortOrderDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2256;
	OrderDetailsByOrderDetailIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2258;
	OrderDetailsByOrderDetailStatusIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2259;
	OrderDetailsByOrderIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2260;
	OrderDetailsByOrderIDProductIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2261;
	OrderDetailsByProductIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2262;
	OrderStatusByOrderStatusNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2264;
	OrderStatusBySortOrderDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2265;
	OrdersByCustomerIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2267;
	OrdersByEmployeeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2268;
	OrdersByOrderDateDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2269;
	OrdersByOrderIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2270;
	OrdersByOrderStatusIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2271;
	OrdersByShipperIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2272;
	OrdersByTaxStatusIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2273;
	PrivilegesByPrivilegeIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2275;
	PrivilegesByPrivilegeNameDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2276;
	ProductCategoriesByProductCategoryCodeDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2278;
	ProductCategoriesByProductCategoryIDDict subclassOf ExternalDictionary transient, final, subschemaFinal, number = 2279;
	Catalog_TableOfContentsSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2220;
	CompaniesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2226;
	CompanyTypesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2229;
	ContactsSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2234;
	EmployeePrivilegesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2239;
	EmployeesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2246;
	MRUSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2251;
	NorthwindFeaturesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2253;
	OrderDetailStatusSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2257;
	OrderDetailsSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2263;
	OrderStatusSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2266;
	OrdersSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2274;
	PrivilegesSet subclassOf ExternalSet transient, final, subschemaFinal, number = 2277;
 
membershipDefinitions
	Catalog_TableOfContentsByTocPageDict of Catalog_TableOfContents ;
	Catalog_TableOfContentsByTocTitleDict of Catalog_TableOfContents ;
	CompaniesByCompanyIDDict of Companies ;
	CompaniesByCompanyNameCompanyTypeIDDict of Companies ;
	CompaniesByCompanyTypeIDDict of Companies ;
	CompaniesByStandardTaxStatusIDDict of Companies ;
	CompaniesByStateAbbrevDict of Companies ;
	CompanyTypesByCompanyTypeDict of CompanyTypes ;
	CompanyTypesByCompanyTypeIDDict of CompanyTypes ;
	ContactsByCompanyIDDict of Contacts ;
	ContactsByCompanyIDFirstNameLastNameDict of Contacts ;
	ContactsByCompanyIDLastNameFirstNameDict of Contacts ;
	ContactsByContactIDDict of Contacts ;
	EmployeePrivilegesByEmployeeIDDict of EmployeePrivileges ;
	EmployeePrivilegesByEmployeePrivilegeIDDict of EmployeePrivileges ;
	EmployeePrivilegesByEmployeePrivilegeIDPrivilegeIDDict of EmployeePrivileges ;
	EmployeePrivilegesByPrivilegeIDDict of EmployeePrivileges ;
	EmployeesByEmployeeIDDict of Employees ;
	EmployeesByFirstNameLastNameDict of Employees ;
	EmployeesByLastNameFirstNameDict of Employees ;
	EmployeesBySupervisorIDDict of Employees ;
	EmployeesByTitleDict of Employees ;
	EmployeesByWindowsUserNameDict of Employees ;
	MRUByEmployeeIDDateAddedDict of MRU ;
	MRUByEmployeeIDDict of MRU ;
	MRUByEmployeeIDTableNamePKValueDict of MRU ;
	MRUByMRU_IDDict of MRU ;
	NorthwindFeaturesByNorthwindFeaturesIDDict of NorthwindFeatures ;
	OrderDetailStatusByOrderDetailStatusIDDict of OrderDetailStatus ;
	OrderDetailStatusByOrderDetailStatusNameDict of OrderDetailStatus ;
	OrderDetailStatusBySortOrderDict of OrderDetailStatus ;
	OrderDetailsByOrderDetailIDDict of OrderDetails ;
	OrderDetailsByOrderDetailStatusIDDict of OrderDetails ;
	OrderDetailsByOrderIDDict of OrderDetails ;
	OrderDetailsByOrderIDProductIDDict of OrderDetails ;
	OrderDetailsByProductIDDict of OrderDetails ;
	OrderStatusByOrderStatusNameDict of OrderStatus ;
	OrderStatusBySortOrderDict of OrderStatus ;
	OrdersByCustomerIDDict of Orders ;
	OrdersByEmployeeIDDict of Orders ;
	OrdersByOrderDateDict of Orders ;
	OrdersByOrderIDDict of Orders ;
	OrdersByOrderStatusIDDict of Orders ;
	OrdersByShipperIDDict of Orders ;
	OrdersByTaxStatusIDDict of Orders ;
	PrivilegesByPrivilegeIDDict of Privileges ;
	PrivilegesByPrivilegeNameDict of Privileges ;
	ProductCategoriesByProductCategoryCodeDict of ProductCategories ;
	ProductCategoriesByProductCategoryIDDict of ProductCategories ;
	Catalog_TableOfContentsSet of Catalog_TableOfContents ;
	CompaniesSet of Companies ;
	CompanyTypesSet of CompanyTypes ;
	ContactsSet of Contacts ;
	EmployeePrivilegesSet of EmployeePrivileges ;
	EmployeesSet of Employees ;
	MRUSet of MRU ;
	NorthwindFeaturesSet of NorthwindFeatures ;
	OrderDetailStatusSet of OrderDetailStatus ;
	OrderDetailsSet of OrderDetails ;
	OrderStatusSet of OrderStatus ;
	OrdersSet of Orders ;
	PrivilegesSet of Privileges ;
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:35:40.206;
	)
	ExternalDatabase completeDefinition
	(
	)
	TestDb completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ExternalObject completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "" 2002:02:08:16:19:19;
	)
	Catalog_TableOfContents completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		tocPage:                       Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		tocTitle:                      String[256] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Companies completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		address:                       String[256] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		businessPhone:                 String[21] protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		city:                          String[256] protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyID:                     Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyName:                   String[51] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyTypeID:                 Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes:                         String protected, subId = 1, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		standardTaxStatusID:           Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stateAbbrev:                   String[3] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		website:                       String protected, subId = 2, number = 14, ordinal = 14;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		zip:                           String[11] protected, number = 15, ordinal = 15;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		notes(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		website(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompanyTypes completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyType:                   String[51] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyTypeID:                 Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Contacts completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		companyID:                     Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		contactID:                     Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		emailAddress:                  String[256] protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		firstName:                     String[21] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		jobTitle:                      String[51] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		lastName:                      String[31] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes:                         String protected, subId = 1, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		primaryPhone:                  String[21] protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		secondaryPhone:                String[21] protected, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		notes(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivileges completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		employeeID:                    Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		employeePrivilegeID:           Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		privilegeID:                   Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Employees completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		attachments:                   String protected, subId = 1, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		emailAddress:                  String[256] protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		employeeID:                    Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		firstName:                     String[21] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		jobTitle:                      String[51] protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		lastName:                      String[31] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes:                         String protected, subId = 2, number = 14, ordinal = 14;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		primaryPhone:                  String[21] protected, number = 15, ordinal = 15;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		secondaryPhone:                String[21] protected, number = 16, ordinal = 16;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		supervisorID:                  Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		title:                         String[21] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		windowsUserName:               String[51] protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		attachments(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRU completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		dateAdded:                     TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		employeeID:                    Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		mRU_ID:                        Integer protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		pKValue:                       Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		tableName:                     String[51] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	NorthwindFeatures completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		description:                   String[256] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		helpKeywords:                  String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		itemName:                      String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		learnMore:                     String protected, subId = 1, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		navigation:                    String[256] protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		northwindFeaturesID:           Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		openMethod:                    Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		learnMore(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailStatus completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderDetailStatusID:           Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderDetailStatusName:         String[51] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		sortOrder:                     Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetails completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		discount:                      Real protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderDetailID:                 Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderDetailStatusID:           Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderID:                       Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productID:                     Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		quantity:                      Integer protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		unitPrice:                     Decimal[19,4] protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderStatus completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderStatusCode:               String[6] protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderStatusID:                 Integer protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderStatusName:               String[51] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		sortOrder:                     Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Orders completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		customerID:                    Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		employeeID:                    Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		invoiceDate:                   TimeStamp protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes:                         String protected, subId = 1, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderDate:                     TimeStamp protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderID:                       Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		orderStatusID:                 Integer protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		paidDate:                      TimeStamp protected, number = 14, ordinal = 14;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		paymentMethod:                 String[51] protected, number = 15, ordinal = 15;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		shippedDate:                   TimeStamp protected, number = 16, ordinal = 16;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		shipperID:                     Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		shippingFee:                   Decimal[19,4] protected, number = 17, ordinal = 17;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		taxRate:                       Real protected, number = 18, ordinal = 18;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		taxStatusID:                   Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		notes(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Privileges completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		privilegeID:                   Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		privilegeName:                 String[51] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ProductCategories completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryCode:           String[4] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryDesc:           String[256] protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryID:             Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryImage:          String protected, subId = 1, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryName:           String[256] protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		productCategoryImage(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ProductVendors completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productID:                     Integer protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productVendorID:               Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		vendorID:                      Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Products completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		discontinued:                  Boolean protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		minimumReorderQuantity:        Integer protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCategoryID:             Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productCode:                   String[21] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productDescription:            String protected, subId = 1, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productID:                     Integer protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productName:                   String[51] protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		quantityPerUnit:               String[51] protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		reorderLevel:                  Integer protected, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		standardUnitCost:              Decimal[19,4] protected, number = 14, ordinal = 14;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		targetLevel:                   Integer protected, number = 15, ordinal = 15;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		unitPrice:                     Decimal[19,4] protected, number = 16, ordinal = 16;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		productDescription(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PurchaseOrderDetails completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productID:                     Integer protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		purchaseOrderDetailID:         Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		purchaseOrderID:               Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		quantity:                      Integer protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		receivedDate:                  TimeStamp protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		unitCost:                      Decimal[19,4] protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PurchaseOrderStatus completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		sortOrder:                     Integer protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		statusID:                      Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		statusName:                    String[51] protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PurchaseOrders completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		approvedByID:                  Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		approvedDate:                  TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		notes:                         String protected, subId = 1, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		paymentAmount:                 Decimal[19,4] protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		paymentDate:                   TimeStamp protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		paymentMethod:                 String[51] protected, number = 10, ordinal = 10;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		purchaseOrderID:               Integer protected, number = 11, ordinal = 11;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		receivedDate:                  TimeStamp protected, number = 12, ordinal = 12;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		shippingFee:                   Decimal[19,4] protected, number = 13, ordinal = 13;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		statusID:                      Integer protected, number = 14, ordinal = 14;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		submittedByID:                 Integer protected, number = 15, ordinal = 15;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		submittedDate:                 TimeStamp protected, number = 16, ordinal = 16;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		taxAmount:                     Decimal[19,4] protected, number = 17, ordinal = 17;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		vendorID:                      Integer protected, number = 18, ordinal = 18;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		notes(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	States completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		stateAbbrev:                   String[3] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stateName:                     String[51] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	StockTake completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		expectedQuantity:              Integer protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		productID:                     Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		quantityOnHand:                Integer protected, number = 7, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stockTakeDate:                 TimeStamp protected, number = 8, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stockTakeID:                   Integer protected, number = 9, ordinal = 9;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Strings completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stringData:                    String protected, subId = 1, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		stringID:                      Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		stringData(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	SystemSettings completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		notes:                         String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingID:                     Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingName:                   String[51] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingValue:                  String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	TaxStatus completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		taxStatus:                     String[51] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		taxStatusID:                   Integer protected, number = 6, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Titles completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		addedBy:                       String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		addedOn:                       TimeStamp protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedBy:                    String[256] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		modifiedOn:                    TimeStamp protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		title:                         String[21] protected, number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	USysRibbons completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		iD:                            Integer protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		ribbonName:                    String[256] protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		ribbonXML:                     String protected, subId = 1, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		ribbonXML(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	UserSettings completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		notes:                         String[256] protected, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingID:                     Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingName:                   String[51] protected, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		settingValue:                  String[256] protected, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Welcome completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	attributeDefinitions
		dataMacro:                     String protected, subId = 1, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		iD:                            Integer protected, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		learn:                         String protected, subId = 2, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		welcome:                       String protected, subId = 3, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
 
	jadeMethodDefinitions
		dataMacro(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		learn(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
		welcome(
			set: Boolean; 
			value: String io) updating, mapping, subschemaHidden, number = 1003;
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:35:40.207;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:35:40.207;
	)
	Collection completeDefinition
	(
	)
	ExternalCollection completeDefinition
	(
		setModifiedTimeStamp "cnwnhs1" "99.0.00" 250918 2018:10:01:15:19:08.610;
	)
	ExternalDictionary completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "" 2002:02:08:16:19:19;
	)
	Catalog_TableOfContentsByTocPageDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	Catalog_TableOfContentsByTocTitleDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesByCompanyIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesByCompanyNameCompanyTypeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesByCompanyTypeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesByStandardTaxStatusIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesByStateAbbrevDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompanyTypesByCompanyTypeDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompanyTypesByCompanyTypeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ContactsByCompanyIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ContactsByCompanyIDFirstNameLastNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ContactsByCompanyIDLastNameFirstNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ContactsByContactIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivilegesByEmployeeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivilegesByEmployeePrivilegeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivilegesByEmployeePrivilegeIDPrivilegeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivilegesByPrivilegeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesByEmployeeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesByFirstNameLastNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesByLastNameFirstNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesBySupervisorIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesByTitleDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesByWindowsUserNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRUByEmployeeIDDateAddedDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRUByEmployeeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRUByEmployeeIDTableNamePKValueDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRUByMRU_IDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	NorthwindFeaturesByNorthwindFeaturesIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailStatusByOrderDetailStatusIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailStatusByOrderDetailStatusNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailStatusBySortOrderDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsByOrderDetailIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsByOrderDetailStatusIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsByOrderIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsByOrderIDProductIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsByProductIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderStatusByOrderStatusNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderStatusBySortOrderDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByCustomerIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByEmployeeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByOrderDateDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByOrderIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByOrderStatusIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByShipperIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersByTaxStatusIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PrivilegesByPrivilegeIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PrivilegesByPrivilegeNameDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ProductCategoriesByProductCategoryCodeDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ProductCategoriesByProductCategoryIDDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ExternalSet completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "" 2002:02:08:16:19:19;
	)
	Catalog_TableOfContentsSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompaniesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	CompanyTypesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	ContactsSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeePrivilegesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	EmployeesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	MRUSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	NorthwindFeaturesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailStatusSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderDetailsSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrderStatusSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	OrdersSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
	PrivilegesSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2020:06:10:09:17:00;
	)
 
memberKeyDefinitions
	Catalog_TableOfContentsByTocPageDict completeDefinition
	(
		tocPage;
	)
	Catalog_TableOfContentsByTocTitleDict completeDefinition
	(
		tocTitle;
	)
	CompaniesByCompanyIDDict completeDefinition
	(
		companyID;
	)
	CompaniesByCompanyNameCompanyTypeIDDict completeDefinition
	(
		companyName;
		companyTypeID;
	)
	CompaniesByCompanyTypeIDDict completeDefinition
	(
		companyTypeID;
	)
	CompaniesByStandardTaxStatusIDDict completeDefinition
	(
		standardTaxStatusID;
	)
	CompaniesByStateAbbrevDict completeDefinition
	(
		stateAbbrev;
	)
	CompanyTypesByCompanyTypeDict completeDefinition
	(
		companyType;
	)
	CompanyTypesByCompanyTypeIDDict completeDefinition
	(
		companyTypeID;
	)
	ContactsByCompanyIDDict completeDefinition
	(
		companyID;
	)
	ContactsByCompanyIDFirstNameLastNameDict completeDefinition
	(
		companyID;
		firstName;
		lastName;
	)
	ContactsByCompanyIDLastNameFirstNameDict completeDefinition
	(
		companyID;
		lastName;
		firstName;
	)
	ContactsByContactIDDict completeDefinition
	(
		contactID;
	)
	EmployeePrivilegesByEmployeeIDDict completeDefinition
	(
		employeeID;
	)
	EmployeePrivilegesByEmployeePrivilegeIDDict completeDefinition
	(
		employeePrivilegeID;
	)
	EmployeePrivilegesByEmployeePrivilegeIDPrivilegeIDDict completeDefinition
	(
		employeePrivilegeID;
		privilegeID;
	)
	EmployeePrivilegesByPrivilegeIDDict completeDefinition
	(
		privilegeID;
	)
	EmployeesByEmployeeIDDict completeDefinition
	(
		employeeID;
	)
	EmployeesByFirstNameLastNameDict completeDefinition
	(
		firstName;
		lastName;
	)
	EmployeesByLastNameFirstNameDict completeDefinition
	(
		lastName;
		firstName;
	)
	EmployeesBySupervisorIDDict completeDefinition
	(
		supervisorID;
	)
	EmployeesByTitleDict completeDefinition
	(
		title;
	)
	EmployeesByWindowsUserNameDict completeDefinition
	(
		windowsUserName;
	)
	MRUByEmployeeIDDateAddedDict completeDefinition
	(
		employeeID;
		dateAdded;
	)
	MRUByEmployeeIDDict completeDefinition
	(
		employeeID;
	)
	MRUByEmployeeIDTableNamePKValueDict completeDefinition
	(
		employeeID;
		tableName;
		pKValue;
	)
	MRUByMRU_IDDict completeDefinition
	(
		mRU_ID;
	)
	NorthwindFeaturesByNorthwindFeaturesIDDict completeDefinition
	(
		northwindFeaturesID;
	)
	OrderDetailStatusByOrderDetailStatusIDDict completeDefinition
	(
		orderDetailStatusID;
	)
	OrderDetailStatusByOrderDetailStatusNameDict completeDefinition
	(
		orderDetailStatusName;
	)
	OrderDetailStatusBySortOrderDict completeDefinition
	(
		sortOrder;
	)
	OrderDetailsByOrderDetailIDDict completeDefinition
	(
		orderDetailID;
	)
	OrderDetailsByOrderDetailStatusIDDict completeDefinition
	(
		orderDetailStatusID;
	)
	OrderDetailsByOrderIDDict completeDefinition
	(
		orderID;
	)
	OrderDetailsByOrderIDProductIDDict completeDefinition
	(
		orderID;
		productID;
	)
	OrderDetailsByProductIDDict completeDefinition
	(
		productID;
	)
	OrderStatusByOrderStatusNameDict completeDefinition
	(
		orderStatusName;
	)
	OrderStatusBySortOrderDict completeDefinition
	(
		sortOrder;
	)
	OrdersByCustomerIDDict completeDefinition
	(
		customerID;
	)
	OrdersByEmployeeIDDict completeDefinition
	(
		employeeID;
	)
	OrdersByOrderDateDict completeDefinition
	(
		orderDate;
	)
	OrdersByOrderIDDict completeDefinition
	(
		orderID;
	)
	OrdersByOrderStatusIDDict completeDefinition
	(
		orderStatusID;
	)
	OrdersByShipperIDDict completeDefinition
	(
		shipperID;
	)
	OrdersByTaxStatusIDDict completeDefinition
	(
		taxStatusID;
	)
	PrivilegesByPrivilegeIDDict completeDefinition
	(
		privilegeID;
	)
	PrivilegesByPrivilegeNameDict completeDefinition
	(
		privilegeName;
	)
	ProductCategoriesByProductCategoryCodeDict completeDefinition
	(
		productCategoryCode;
	)
	ProductCategoriesByProductCategoryIDDict completeDefinition
	(
		productCategoryID;
	)
 
inverseDefinitions
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:35:40.207;
	databaseFileDefinitions
		"testschema" number = 57;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:04:13:35:40.207;
	defaultFileDefinition "testschema";
	classMapDefinitions
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	Companies (
	jadeMethodSources
notes
{
notes(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Notes",14, value );
	else
		value := _getVirtualString( "Notes",14);
	endif;
end;
}

website
{
website(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Website",15, value );
	else
		value := _getVirtualString( "Website",15);
	endif;
end;
}

	)
	Contacts (
	jadeMethodSources
notes
{
notes(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Notes",13, value );
	else
		value := _getVirtualString( "Notes",13);
	endif;
end;
}

	)
	Employees (
	jadeMethodSources
attachments
{
attachments(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Attachments",15, value );
	else
		value := _getVirtualString( "Attachments",15);
	endif;
end;
}

notes
{
notes(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Notes",16, value );
	else
		value := _getVirtualString( "Notes",16);
	endif;
end;
}

	)
	NorthwindFeatures (
	jadeMethodSources
learnMore
{
learnMore(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "LearnMore",7, value );
	else
		value := _getVirtualString( "LearnMore",7);
	endif;
end;
}

	)
	Orders (
	jadeMethodSources
notes
{
notes(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Notes",18, value );
	else
		value := _getVirtualString( "Notes",18);
	endif;
end;
}

	)
	ProductCategories (
	jadeMethodSources
productCategoryImage
{
productCategoryImage(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "ProductCategoryImage",9, value );
	else
		value := _getVirtualString( "ProductCategoryImage",9);
	endif;
end;
}

	)
	Products (
	jadeMethodSources
productDescription
{
productDescription(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "ProductDescription",16, value );
	else
		value := _getVirtualString( "ProductDescription",16);
	endif;
end;
}

	)
	PurchaseOrders (
	jadeMethodSources
notes
{
notes(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Notes",18, value );
	else
		value := _getVirtualString( "Notes",18);
	endif;
end;
}

	)
	Strings (
	jadeMethodSources
stringData
{
stringData(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "StringData",6, value );
	else
		value := _getVirtualString( "StringData",6);
	endif;
end;
}

	)
	USysRibbons (
	jadeMethodSources
ribbonXML
{
ribbonXML(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "RibbonXML",3, value );
	else
		value := _getVirtualString( "RibbonXML",3);
	endif;
end;
}

	)
	Welcome (
	jadeMethodSources
dataMacro
{
dataMacro(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "DataMacro",2, value );
	else
		value := _getVirtualString( "DataMacro",2);
	endif;
end;
}

learn
{
learn(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Learn",3, value );
	else
		value := _getVirtualString( "Learn",3);
	endif;
end;
}

welcome
{
welcome(set:Boolean; value:String io) mapping, updating, subschemaHidden;
begin
	if set then
		_setVirtualString( "Welcome",4, value );
	else
		value := _getVirtualString( "Welcome",4);
	endif;
end;
}

	)
