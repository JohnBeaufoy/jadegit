target_link_libraries(jadegit_test PRIVATE flatbuffers jadegit_registry)

target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Commit.cpp
	${CMAKE_CURRENT_LIST_DIR}/Printer.cpp
	${CMAKE_CURRENT_LIST_DIR}/Repository.cpp
	${CMAKE_CURRENT_LIST_DIR}/Root.cpp
	${CMAKE_CURRENT_LIST_DIR}/Storage.cpp
	${CMAKE_CURRENT_LIST_DIR}/Validator.cpp
)

catch_discover_tests(jadegit_test
					 TEST_SPEC "[registry]"
					 TEST_PREFIX "jadegit.registry."
					 WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})