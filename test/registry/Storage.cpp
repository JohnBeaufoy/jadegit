#include <catch2/catch_test_macros.hpp>
#include <registry/Commit.h>
#include <registry/Root.h>
#include <registry/Storage.h>

#if USE_JADE
#include <TestAppContext.h>
#endif

using namespace JadeGit;
using namespace JadeGit::Registry;

TEST_CASE("Storage", "[registry]")
{
	// Make prototype registry
	Root prototype;
	{
		RepositoryT repo;
		repo.name = "test_repo";
		repo.origin = "http://gitmock.com/mockrepo.git";

		repo.latest.push_back(make_commit("4a202b346bb0fb0db7eff3cffeb3c70babbd2045"));

		SchemaT schema;
		schema.name = "test_schema";
		repo.schemas.push_back(schema);

		prototype.add(repo);
	}

	SECTION("File")
	{
		FileStorage storage("test.jgr");

		// Save
		{
			prototype.save(storage);
		}

		// Load
		{
			Root registry(storage);

			CHECK(registry == prototype);
		}
	}

#if USE_JADE
	SECTION("Database")
	{
		TestDbContext db;
		TestAppContext app(db);

		DatabaseStorage storage;

		// Save
		{
			prototype.save(storage);
		}

		// Load
		{
			Root registry(storage);

			CHECK(registry == prototype);
		}
	}
#endif
}
