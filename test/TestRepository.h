#pragma once
#include <jadegit/git2.h>
#include "TestFileSystem.h"

namespace JadeGit
{
	class TestRepository
	{
	public:
		TestRepository();
		TestRepository(const FileSystem& src);

		operator git_repository* () const;
		explicit operator git_commit* () const;
		explicit operator git_index* () const;

	private:
		TestFileSystem fs;
		std::unique_ptr<git_repository> repo;
		mutable std::unique_ptr<git_commit> commit;
		mutable std::unique_ptr<git_index> index;
	};
}