#include "FileSystem.h"
#include "TestRepository.h"
#include <vfs/GitFileSystem.h>

using namespace std;
using namespace JadeGit;

template <bool Writable>
class GitFileSystemFactory : public FileSystemFactory
{
public:
	FileSystem& empty() final
	{
		repo = make_unique<TestRepository>();

		if constexpr (Writable)
		{
			fs = make_unique<GitFileSystem>(static_cast<git_index*>(*repo));
		}
		else
		{
			fs = make_unique<GitFileSystem>(*repo);
		}
		return *fs;
	}

	FileSystem& clone(const SourceFileSystem& src) final
	{
		repo = make_unique<TestRepository>(src);

		if constexpr (Writable)
		{
			fs = make_unique<GitFileSystem>(static_cast<git_index*>(*repo));
		}
		else
		{
			fs = make_unique<GitFileSystem>(static_cast<git_commit*>(*repo));
		}
		return *fs;
	}

private:
	unique_ptr<TestRepository> repo;
	unique_ptr<GitFileSystem> fs;
};

TEST_CASE("GitFileSystem", "[vfs]")
{
	SECTION("WithIndex")
	{
		GitFileSystemFactory<true> factory;

		iterator_test_scenarios(factory);
		writable_test_scenarios(factory);
	}
	SECTION("WithTree")
	{
		GitFileSystemFactory<false> factory;

		iterator_test_scenarios(factory);
		readonly_test_scenarios(factory);
	}
}